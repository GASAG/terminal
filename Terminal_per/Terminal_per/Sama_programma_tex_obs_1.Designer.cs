﻿namespace Terminal_per
{
    partial class Sama_programma_tex_obs_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sama_programma_tex_obs_1));
            System.Windows.Forms.Label NadbavkaLabel;
            System.Windows.Forms.Label Indikator_2Label;
            System.Windows.Forms.Label Indikator_1Label;
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Panel5 = new System.Windows.Forms.Panel();
            this.Label31 = new System.Windows.Forms.Label();
            this.TextBox25 = new System.Windows.Forms.TextBox();
            this.TextBox26 = new System.Windows.Forms.TextBox();
            this.Label32 = new System.Windows.Forms.Label();
            this.Label33 = new System.Windows.Forms.Label();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.MenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ВыберитеПользователяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ПриёмщикаВагоновToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.МастераЦТОToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox3 = new System.Windows.Forms.ToolStripTextBox();
            this.СлесарейToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.МастераТО1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox4 = new System.Windows.Forms.ToolStripTextBox();
            this.МастераТО2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox6 = new System.Windows.Forms.ToolStripTextBox();
            this.МастераСРToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox5 = new System.Windows.Forms.ToolStripTextBox();
            this.ОтделенияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.МалярногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox7 = new System.Windows.Forms.ToolStripTextBox();
            this.СлесарноКузовногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox8 = new System.Windows.Forms.ToolStripTextBox();
            this.РедукторногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox9 = new System.Windows.Forms.ToolStripTextBox();
            this.МеханическогоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox11 = new System.Windows.Forms.ToolStripTextBox();
            this.КолесотокарногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox12 = new System.Windows.Forms.ToolStripTextBox();
            this.МастерскиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.РадиотехническойToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox13 = new System.Windows.Forms.ToolStripTextBox();
            this.АккумуляторнойToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox14 = new System.Windows.Forms.ToolStripTextBox();
            this.УчасткаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.РемонтаТокоприемниковToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox15 = new System.Windows.Forms.ToolStripTextBox();
            this.ЭлектроаппаратногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox16 = new System.Windows.Forms.ToolStripTextBox();
            this.РемонтаДвигателейToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox17 = new System.Windows.Forms.ToolStripTextBox();
            this.ЦехаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ЭлектросварочногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox18 = new System.Windows.Forms.ToolStripTextBox();
            this.ТележечногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox19 = new System.Windows.Forms.ToolStripTextBox();
            this.АдминистрацииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.ОтчетыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ПриёмщикаВагоновToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ПрохождениеЕОToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ВыходToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ВыходToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ЗамечанияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.СлесарямToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.GroupBox4 = new System.Windows.Forms.GroupBox();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ssdate = new System.Windows.Forms.ToolStripStatusLabel();
            this.sstime = new System.Windows.Forms.ToolStripStatusLabel();
            this.TabControl2 = new System.Windows.Forms.TabControl();
            this.TabPage9 = new System.Windows.Forms.TabPage();
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            this.TabPage10 = new System.Windows.Forms.TabPage();
            this.Predmet_ystLabel1 = new System.Windows.Forms.Label();
            this.NameLabel1 = new System.Windows.Forms.Label();
            this.Time_2Label1 = new System.Windows.Forms.Label();
            this.Data_2Label1 = new System.Windows.Forms.Label();
            this.Label30 = new System.Windows.Forms.Label();
            this.Label29 = new System.Windows.Forms.Label();
            this.Label28 = new System.Windows.Forms.Label();
            this.Label27 = new System.Windows.Forms.Label();
            this.DataGridView2 = new System.Windows.Forms.DataGridView();
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.Button1 = new System.Windows.Forms.Button();
            this.DateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.DateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.MenuStrip2 = new System.Windows.Forms.MenuStrip();
            this.ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.ЗапросОУстраненииНеисправностейToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.TabControl3 = new System.Windows.Forms.TabControl();
            this.TabPage3 = new System.Windows.Forms.TabPage();
            this.MenuStrip3 = new System.Windows.Forms.MenuStrip();
            this.ToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.BindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.Vagon_spisokBindingNavigatorSaveItem21 = new System.Windows.Forms.ToolStripButton();
            this.Label14 = new System.Windows.Forms.Label();
            this.TextBox6 = new System.Windows.Forms.TextBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.CheckBox4 = new System.Windows.Forms.CheckBox();
            this.TabPage4 = new System.Windows.Forms.TabPage();
            this.BindingNavigator2 = new System.Windows.Forms.BindingNavigator(this.components);
            this.ToolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.CheckBox3 = new System.Windows.Forms.CheckBox();
            this.Label13 = new System.Windows.Forms.Label();
            this.TabPage6 = new System.Windows.Forms.TabPage();
            this.BindingNavigator3 = new System.Windows.Forms.BindingNavigator(this.components);
            this.ToolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.CheckBox5 = new System.Windows.Forms.CheckBox();
            this.Label18 = new System.Windows.Forms.Label();
            this.TabPage5 = new System.Windows.Forms.TabPage();
            this.MenuStrip6 = new System.Windows.Forms.MenuStrip();
            this.ToolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.CheckBox6 = new System.Windows.Forms.CheckBox();
            this.Label23 = new System.Windows.Forms.Label();
            this.TabPage7 = new System.Windows.Forms.TabPage();
            this.CheckBox9 = new System.Windows.Forms.CheckBox();
            this.CheckBox8 = new System.Windows.Forms.CheckBox();
            this.CheckBox7 = new System.Windows.Forms.CheckBox();
            this.TabPage2 = new System.Windows.Forms.TabPage();
            this.CheckBox14 = new System.Windows.Forms.CheckBox();
            this.CheckBox13 = new System.Windows.Forms.CheckBox();
            this.CheckBox12 = new System.Windows.Forms.CheckBox();
            this.CheckBox11 = new System.Windows.Forms.CheckBox();
            this.CheckBox2 = new System.Windows.Forms.CheckBox();
            this.CheckBox1 = new System.Windows.Forms.CheckBox();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.Panel6 = new System.Windows.Forms.Panel();
            this.NadbavkaTextBox = new System.Windows.Forms.TextBox();
            this.Indikator_2TextBox = new System.Windows.Forms.TextBox();
            this.Indikator_1TextBox = new System.Windows.Forms.TextBox();
            this.TextBox4 = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.Label36 = new System.Windows.Forms.Label();
            this.Label37 = new System.Windows.Forms.Label();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.TextBox1 = new System.Windows.Forms.TextBox();
            this.TextBox8 = new System.Windows.Forms.TextBox();
            this.TextBox9 = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.TextBox2 = new System.Windows.Forms.TextBox();
            this.TextBox3 = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.PictureBox2 = new System.Windows.Forms.PictureBox();
            this.GroupBox5 = new System.Windows.Forms.GroupBox();
            this.Label35 = new System.Windows.Forms.Label();
            this.Label34 = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.Label26 = new System.Windows.Forms.Label();
            this.Label25 = new System.Windows.Forms.Label();
            this.Label24 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.menuStrip4 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip8 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.menuStrip5 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.bindingNavigator4 = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.menuStrip7 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.bindingNavigator5 = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.menuStrip9 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.bindingNavigator6 = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            NadbavkaLabel = new System.Windows.Forms.Label();
            Indikator_2Label = new System.Windows.Forms.Label();
            Indikator_1Label = new System.Windows.Forms.Label();
            this.TableLayoutPanel1.SuspendLayout();
            this.Panel5.SuspendLayout();
            this.Panel1.SuspendLayout();
            this.MenuStrip1.SuspendLayout();
            this.GroupBox4.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            this.TabControl2.SuspendLayout();
            this.TabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            this.TabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView2)).BeginInit();
            this.GroupBox3.SuspendLayout();
            this.MenuStrip2.SuspendLayout();
            this.Panel2.SuspendLayout();
            this.TabControl3.SuspendLayout();
            this.TabPage3.SuspendLayout();
            this.MenuStrip3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator1)).BeginInit();
            this.BindingNavigator1.SuspendLayout();
            this.TabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator2)).BeginInit();
            this.BindingNavigator2.SuspendLayout();
            this.TabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator3)).BeginInit();
            this.BindingNavigator3.SuspendLayout();
            this.TabPage5.SuspendLayout();
            this.MenuStrip6.SuspendLayout();
            this.TabPage7.SuspendLayout();
            this.TabPage2.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.Panel6.SuspendLayout();
            this.Panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.Panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).BeginInit();
            this.GroupBox5.SuspendLayout();
            this.menuStrip4.SuspendLayout();
            this.menuStrip8.SuspendLayout();
            this.menuStrip5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator4)).BeginInit();
            this.bindingNavigator4.SuspendLayout();
            this.menuStrip7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator5)).BeginInit();
            this.bindingNavigator5.SuspendLayout();
            this.menuStrip9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator6)).BeginInit();
            this.bindingNavigator6.SuspendLayout();
            this.SuspendLayout();
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.BackColor = System.Drawing.SystemColors.Window;
            this.TableLayoutPanel1.ColumnCount = 3;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.32558F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.65804F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.01637F));
            this.TableLayoutPanel1.Controls.Add(this.Panel5, 0, 1);
            this.TableLayoutPanel1.Controls.Add(this.Panel1, 1, 0);
            this.TableLayoutPanel1.Controls.Add(this.GroupBox1, 0, 0);
            this.TableLayoutPanel1.Controls.Add(this.Panel3, 2, 0);
            this.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 2;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 83.10011F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.89989F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TableLayoutPanel1.Size = new System.Drawing.Size(1284, 778);
            this.TableLayoutPanel1.TabIndex = 5;
            // 
            // Panel5
            // 
            this.Panel5.Controls.Add(this.Label31);
            this.Panel5.Controls.Add(this.TextBox25);
            this.Panel5.Controls.Add(this.TextBox26);
            this.Panel5.Controls.Add(this.Label32);
            this.Panel5.Controls.Add(this.Label33);
            this.Panel5.Location = new System.Drawing.Point(3, 649);
            this.Panel5.Name = "Panel5";
            this.Panel5.Size = new System.Drawing.Size(216, 95);
            this.Panel5.TabIndex = 21;
            // 
            // Label31
            // 
            this.Label31.AutoSize = true;
            this.Label31.Location = new System.Drawing.Point(17, 14);
            this.Label31.Name = "Label31";
            this.Label31.Size = new System.Drawing.Size(211, 22);
            this.Label31.TabIndex = 19;
            this.Label31.Text = "Скрытая подпрограмма";
            // 
            // TextBox25
            // 
            this.TextBox25.Location = new System.Drawing.Point(20, 37);
            this.TextBox25.Name = "TextBox25";
            this.TextBox25.ReadOnly = true;
            this.TextBox25.Size = new System.Drawing.Size(100, 30);
            this.TextBox25.TabIndex = 10;
            this.TextBox25.Text = ".jpg";
            // 
            // TextBox26
            // 
            this.TextBox26.Location = new System.Drawing.Point(19, 63);
            this.TextBox26.Name = "TextBox26";
            this.TextBox26.ReadOnly = true;
            this.TextBox26.Size = new System.Drawing.Size(100, 30);
            this.TextBox26.TabIndex = 4;
            // 
            // Label32
            // 
            this.Label32.AutoSize = true;
            this.Label32.Location = new System.Drawing.Point(145, 66);
            this.Label32.Name = "Label32";
            this.Label32.Size = new System.Drawing.Size(75, 22);
            this.Label32.TabIndex = 17;
            this.Label32.Text = "Label32";
            // 
            // Label33
            // 
            this.Label33.AutoSize = true;
            this.Label33.Location = new System.Drawing.Point(136, 37);
            this.Label33.Name = "Label33";
            this.Label33.Size = new System.Drawing.Size(75, 22);
            this.Label33.TabIndex = 18;
            this.Label33.Text = "Label33";
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.SystemColors.Window;
            this.Panel1.Controls.Add(this.MenuStrip1);
            this.Panel1.Controls.Add(this.GroupBox4);
            this.Panel1.Controls.Add(this.GroupBox3);
            this.Panel1.Controls.Add(this.Panel2);
            this.Panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel1.Location = new System.Drawing.Point(315, 3);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(772, 640);
            this.Panel1.TabIndex = 0;
            this.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel1_Paint);
            // 
            // MenuStrip1
            // 
            this.MenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ВыберитеПользователяToolStripMenuItem,
            this.ОтчетыToolStripMenuItem,
            this.ВыходToolStripMenuItem1,
            this.ЗамечанияToolStripMenuItem});
            this.MenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip1.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.MenuStrip1.Name = "MenuStrip1";
            this.MenuStrip1.Size = new System.Drawing.Size(772, 31);
            this.MenuStrip1.TabIndex = 10;
            this.MenuStrip1.Text = "MenuStrip1";
            // 
            // ВыберитеПользователяToolStripMenuItem
            // 
            this.ВыберитеПользователяToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ПриёмщикаВагоновToolStripMenuItem,
            this.МастераЦТОToolStripMenuItem,
            this.СлесарейToolStripMenuItem,
            this.МастераТО1ToolStripMenuItem,
            this.МастераТО2ToolStripMenuItem,
            this.МастераСРToolStripMenuItem,
            this.ОтделенияToolStripMenuItem,
            this.МастерскиеToolStripMenuItem,
            this.УчасткаToolStripMenuItem,
            this.ЦехаToolStripMenuItem,
            this.АдминистрацииToolStripMenuItem});
            this.ВыберитеПользователяToolStripMenuItem.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ВыберитеПользователяToolStripMenuItem.Name = "ВыберитеПользователяToolStripMenuItem";
            this.ВыберитеПользователяToolStripMenuItem.Size = new System.Drawing.Size(260, 27);
            this.ВыберитеПользователяToolStripMenuItem.Text = "В&ыберите учётную запись";
            // 
            // ПриёмщикаВагоновToolStripMenuItem
            // 
            this.ПриёмщикаВагоновToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox1});
            this.ПриёмщикаВагоновToolStripMenuItem.Name = "ПриёмщикаВагоновToolStripMenuItem";
            this.ПриёмщикаВагоновToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.ПриёмщикаВагоновToolStripMenuItem.Text = "Приёмщика вагонов";
            // 
            // ToolStripTextBox1
            // 
            this.ToolStripTextBox1.Name = "ToolStripTextBox1";
            this.ToolStripTextBox1.ReadOnly = true;
            this.ToolStripTextBox1.Size = new System.Drawing.Size(100, 27);
            this.ToolStripTextBox1.Text = "100";
            // 
            // МастераЦТОToolStripMenuItem
            // 
            this.МастераЦТОToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox3});
            this.МастераЦТОToolStripMenuItem.Name = "МастераЦТОToolStripMenuItem";
            this.МастераЦТОToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.МастераЦТОToolStripMenuItem.Text = "Мастера ЕО";
            // 
            // ToolStripTextBox3
            // 
            this.ToolStripTextBox3.Name = "ToolStripTextBox3";
            this.ToolStripTextBox3.Size = new System.Drawing.Size(100, 27);
            // 
            // СлесарейToolStripMenuItem
            // 
            this.СлесарейToolStripMenuItem.Name = "СлесарейToolStripMenuItem";
            this.СлесарейToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.СлесарейToolStripMenuItem.Text = "Слесарей";
            // 
            // МастераТО1ToolStripMenuItem
            // 
            this.МастераТО1ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox4});
            this.МастераТО1ToolStripMenuItem.Name = "МастераТО1ToolStripMenuItem";
            this.МастераТО1ToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.МастераТО1ToolStripMenuItem.Text = "Мастера ТО1";
            // 
            // ToolStripTextBox4
            // 
            this.ToolStripTextBox4.Name = "ToolStripTextBox4";
            this.ToolStripTextBox4.Size = new System.Drawing.Size(100, 27);
            // 
            // МастераТО2ToolStripMenuItem
            // 
            this.МастераТО2ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox6});
            this.МастераТО2ToolStripMenuItem.Name = "МастераТО2ToolStripMenuItem";
            this.МастераТО2ToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.МастераТО2ToolStripMenuItem.Text = "Мастера ТО2";
            // 
            // ToolStripTextBox6
            // 
            this.ToolStripTextBox6.Name = "ToolStripTextBox6";
            this.ToolStripTextBox6.Size = new System.Drawing.Size(100, 27);
            // 
            // МастераСРToolStripMenuItem
            // 
            this.МастераСРToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox5});
            this.МастераСРToolStripMenuItem.Name = "МастераСРToolStripMenuItem";
            this.МастераСРToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.МастераСРToolStripMenuItem.Text = "Мастера СР";
            // 
            // ToolStripTextBox5
            // 
            this.ToolStripTextBox5.Name = "ToolStripTextBox5";
            this.ToolStripTextBox5.Size = new System.Drawing.Size(100, 27);
            // 
            // ОтделенияToolStripMenuItem
            // 
            this.ОтделенияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.МалярногоToolStripMenuItem,
            this.СлесарноКузовногоToolStripMenuItem,
            this.РедукторногоToolStripMenuItem,
            this.МеханическогоToolStripMenuItem,
            this.КолесотокарногоToolStripMenuItem});
            this.ОтделенияToolStripMenuItem.Name = "ОтделенияToolStripMenuItem";
            this.ОтделенияToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.ОтделенияToolStripMenuItem.Text = "Отделения";
            // 
            // МалярногоToolStripMenuItem
            // 
            this.МалярногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox7});
            this.МалярногоToolStripMenuItem.Name = "МалярногоToolStripMenuItem";
            this.МалярногоToolStripMenuItem.Size = new System.Drawing.Size(268, 28);
            this.МалярногоToolStripMenuItem.Text = "Малярного";
            // 
            // ToolStripTextBox7
            // 
            this.ToolStripTextBox7.Name = "ToolStripTextBox7";
            this.ToolStripTextBox7.Size = new System.Drawing.Size(100, 27);
            // 
            // СлесарноКузовногоToolStripMenuItem
            // 
            this.СлесарноКузовногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox8});
            this.СлесарноКузовногоToolStripMenuItem.Name = "СлесарноКузовногоToolStripMenuItem";
            this.СлесарноКузовногоToolStripMenuItem.Size = new System.Drawing.Size(268, 28);
            this.СлесарноКузовногоToolStripMenuItem.Text = "Слесарно-Кузовного";
            // 
            // ToolStripTextBox8
            // 
            this.ToolStripTextBox8.Name = "ToolStripTextBox8";
            this.ToolStripTextBox8.Size = new System.Drawing.Size(100, 27);
            // 
            // РедукторногоToolStripMenuItem
            // 
            this.РедукторногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox9});
            this.РедукторногоToolStripMenuItem.Name = "РедукторногоToolStripMenuItem";
            this.РедукторногоToolStripMenuItem.Size = new System.Drawing.Size(268, 28);
            this.РедукторногоToolStripMenuItem.Text = "Редукторного";
            // 
            // ToolStripTextBox9
            // 
            this.ToolStripTextBox9.Name = "ToolStripTextBox9";
            this.ToolStripTextBox9.Size = new System.Drawing.Size(100, 27);
            // 
            // МеханическогоToolStripMenuItem
            // 
            this.МеханическогоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox11});
            this.МеханическогоToolStripMenuItem.Name = "МеханическогоToolStripMenuItem";
            this.МеханическогоToolStripMenuItem.Size = new System.Drawing.Size(268, 28);
            this.МеханическогоToolStripMenuItem.Text = "Механического";
            // 
            // ToolStripTextBox11
            // 
            this.ToolStripTextBox11.Name = "ToolStripTextBox11";
            this.ToolStripTextBox11.Size = new System.Drawing.Size(100, 27);
            // 
            // КолесотокарногоToolStripMenuItem
            // 
            this.КолесотокарногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox12});
            this.КолесотокарногоToolStripMenuItem.Name = "КолесотокарногоToolStripMenuItem";
            this.КолесотокарногоToolStripMenuItem.Size = new System.Drawing.Size(268, 28);
            this.КолесотокарногоToolStripMenuItem.Text = "Колесотокарного";
            // 
            // ToolStripTextBox12
            // 
            this.ToolStripTextBox12.Name = "ToolStripTextBox12";
            this.ToolStripTextBox12.Size = new System.Drawing.Size(100, 27);
            // 
            // МастерскиеToolStripMenuItem
            // 
            this.МастерскиеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.РадиотехническойToolStripMenuItem,
            this.АккумуляторнойToolStripMenuItem});
            this.МастерскиеToolStripMenuItem.Name = "МастерскиеToolStripMenuItem";
            this.МастерскиеToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.МастерскиеToolStripMenuItem.Text = "Мастерской";
            // 
            // РадиотехническойToolStripMenuItem
            // 
            this.РадиотехническойToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox13});
            this.РадиотехническойToolStripMenuItem.Name = "РадиотехническойToolStripMenuItem";
            this.РадиотехническойToolStripMenuItem.Size = new System.Drawing.Size(246, 28);
            this.РадиотехническойToolStripMenuItem.Text = "Радиотехнической";
            // 
            // ToolStripTextBox13
            // 
            this.ToolStripTextBox13.Name = "ToolStripTextBox13";
            this.ToolStripTextBox13.Size = new System.Drawing.Size(100, 27);
            // 
            // АккумуляторнойToolStripMenuItem
            // 
            this.АккумуляторнойToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox14});
            this.АккумуляторнойToolStripMenuItem.Name = "АккумуляторнойToolStripMenuItem";
            this.АккумуляторнойToolStripMenuItem.Size = new System.Drawing.Size(246, 28);
            this.АккумуляторнойToolStripMenuItem.Text = "Аккумуляторной";
            // 
            // ToolStripTextBox14
            // 
            this.ToolStripTextBox14.Name = "ToolStripTextBox14";
            this.ToolStripTextBox14.Size = new System.Drawing.Size(100, 27);
            // 
            // УчасткаToolStripMenuItem
            // 
            this.УчасткаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.РемонтаТокоприемниковToolStripMenuItem,
            this.ЭлектроаппаратногоToolStripMenuItem,
            this.РемонтаДвигателейToolStripMenuItem});
            this.УчасткаToolStripMenuItem.Name = "УчасткаToolStripMenuItem";
            this.УчасткаToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.УчасткаToolStripMenuItem.Text = "Участка";
            // 
            // РемонтаТокоприемниковToolStripMenuItem
            // 
            this.РемонтаТокоприемниковToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox15});
            this.РемонтаТокоприемниковToolStripMenuItem.Name = "РемонтаТокоприемниковToolStripMenuItem";
            this.РемонтаТокоприемниковToolStripMenuItem.Size = new System.Drawing.Size(309, 28);
            this.РемонтаТокоприемниковToolStripMenuItem.Text = "Ремонта токоприемников";
            // 
            // ToolStripTextBox15
            // 
            this.ToolStripTextBox15.Name = "ToolStripTextBox15";
            this.ToolStripTextBox15.Size = new System.Drawing.Size(100, 27);
            // 
            // ЭлектроаппаратногоToolStripMenuItem
            // 
            this.ЭлектроаппаратногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox16});
            this.ЭлектроаппаратногоToolStripMenuItem.Name = "ЭлектроаппаратногоToolStripMenuItem";
            this.ЭлектроаппаратногоToolStripMenuItem.Size = new System.Drawing.Size(309, 28);
            this.ЭлектроаппаратногоToolStripMenuItem.Text = "Электроаппаратного";
            // 
            // ToolStripTextBox16
            // 
            this.ToolStripTextBox16.Name = "ToolStripTextBox16";
            this.ToolStripTextBox16.Size = new System.Drawing.Size(100, 27);
            // 
            // РемонтаДвигателейToolStripMenuItem
            // 
            this.РемонтаДвигателейToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox17});
            this.РемонтаДвигателейToolStripMenuItem.Name = "РемонтаДвигателейToolStripMenuItem";
            this.РемонтаДвигателейToolStripMenuItem.Size = new System.Drawing.Size(309, 28);
            this.РемонтаДвигателейToolStripMenuItem.Text = "Ремонта двигателей";
            // 
            // ToolStripTextBox17
            // 
            this.ToolStripTextBox17.Name = "ToolStripTextBox17";
            this.ToolStripTextBox17.Size = new System.Drawing.Size(100, 27);
            // 
            // ЦехаToolStripMenuItem
            // 
            this.ЦехаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ЭлектросварочногоToolStripMenuItem,
            this.ТележечногоToolStripMenuItem});
            this.ЦехаToolStripMenuItem.Name = "ЦехаToolStripMenuItem";
            this.ЦехаToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.ЦехаToolStripMenuItem.Text = "Цеха";
            // 
            // ЭлектросварочногоToolStripMenuItem
            // 
            this.ЭлектросварочногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox18});
            this.ЭлектросварочногоToolStripMenuItem.Name = "ЭлектросварочногоToolStripMenuItem";
            this.ЭлектросварочногоToolStripMenuItem.Size = new System.Drawing.Size(258, 28);
            this.ЭлектросварочногоToolStripMenuItem.Text = "Электросварочного";
            // 
            // ToolStripTextBox18
            // 
            this.ToolStripTextBox18.Name = "ToolStripTextBox18";
            this.ToolStripTextBox18.Size = new System.Drawing.Size(100, 27);
            // 
            // ТележечногоToolStripMenuItem
            // 
            this.ТележечногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox19});
            this.ТележечногоToolStripMenuItem.Name = "ТележечногоToolStripMenuItem";
            this.ТележечногоToolStripMenuItem.Size = new System.Drawing.Size(258, 28);
            this.ТележечногоToolStripMenuItem.Text = "Тележечного";
            // 
            // ToolStripTextBox19
            // 
            this.ToolStripTextBox19.Name = "ToolStripTextBox19";
            this.ToolStripTextBox19.Size = new System.Drawing.Size(100, 27);
            // 
            // АдминистрацииToolStripMenuItem
            // 
            this.АдминистрацииToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox2});
            this.АдминистрацииToolStripMenuItem.Name = "АдминистрацииToolStripMenuItem";
            this.АдминистрацииToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.АдминистрацииToolStripMenuItem.Text = "Администрации";
            // 
            // ToolStripTextBox2
            // 
            this.ToolStripTextBox2.Name = "ToolStripTextBox2";
            this.ToolStripTextBox2.ReadOnly = true;
            this.ToolStripTextBox2.Size = new System.Drawing.Size(100, 27);
            this.ToolStripTextBox2.Text = "100";
            // 
            // ОтчетыToolStripMenuItem
            // 
            this.ОтчетыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ПриёмщикаВагоновToolStripMenuItem1,
            this.ПрохождениеЕОToolStripMenuItem});
            this.ОтчетыToolStripMenuItem.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ОтчетыToolStripMenuItem.Name = "ОтчетыToolStripMenuItem";
            this.ОтчетыToolStripMenuItem.Size = new System.Drawing.Size(90, 27);
            this.ОтчетыToolStripMenuItem.Text = "От&четы";
            this.ОтчетыToolStripMenuItem.ToolTipText = "Пока не все работает))Требуется для работы Excel 2003";
            // 
            // ПриёмщикаВагоновToolStripMenuItem1
            // 
            this.ПриёмщикаВагоновToolStripMenuItem1.Name = "ПриёмщикаВагоновToolStripMenuItem1";
            this.ПриёмщикаВагоновToolStripMenuItem1.Size = new System.Drawing.Size(262, 28);
            this.ПриёмщикаВагоновToolStripMenuItem1.Text = "Приёмщика вагонов";
            // 
            // ПрохождениеЕОToolStripMenuItem
            // 
            this.ПрохождениеЕОToolStripMenuItem.Name = "ПрохождениеЕОToolStripMenuItem";
            this.ПрохождениеЕОToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.ПрохождениеЕОToolStripMenuItem.Text = "Прохождение ЕО";
            // 
            // ВыходToolStripMenuItem1
            // 
            this.ВыходToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem2,
            this.ВыходToolStripMenuItem2});
            this.ВыходToolStripMenuItem1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ВыходToolStripMenuItem1.Name = "ВыходToolStripMenuItem1";
            this.ВыходToolStripMenuItem1.Size = new System.Drawing.Size(81, 27);
            this.ВыходToolStripMenuItem1.Text = "В&ыход";
            // 
            // ToolStripMenuItem2
            // 
            this.ToolStripMenuItem2.Name = "ToolStripMenuItem2";
            this.ToolStripMenuItem2.Size = new System.Drawing.Size(247, 28);
            this.ToolStripMenuItem2.Text = "Окончание сеанса";
            // 
            // ВыходToolStripMenuItem2
            // 
            this.ВыходToolStripMenuItem2.Name = "ВыходToolStripMenuItem2";
            this.ВыходToolStripMenuItem2.Size = new System.Drawing.Size(247, 28);
            this.ВыходToolStripMenuItem2.Text = "Выход";
            // 
            // ЗамечанияToolStripMenuItem
            // 
            this.ЗамечанияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.СлесарямToolStripMenuItem1});
            this.ЗамечанияToolStripMenuItem.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ЗамечанияToolStripMenuItem.Name = "ЗамечанияToolStripMenuItem";
            this.ЗамечанияToolStripMenuItem.Size = new System.Drawing.Size(120, 27);
            this.ЗамечанияToolStripMenuItem.Text = "Замечание";
            // 
            // СлесарямToolStripMenuItem1
            // 
            this.СлесарямToolStripMenuItem1.Name = "СлесарямToolStripMenuItem1";
            this.СлесарямToolStripMenuItem1.Size = new System.Drawing.Size(172, 28);
            this.СлесарямToolStripMenuItem1.Text = "Слесарям";
            // 
            // GroupBox4
            // 
            this.GroupBox4.Controls.Add(this.StatusStrip1);
            this.GroupBox4.Controls.Add(this.TabControl2);
            this.GroupBox4.Location = new System.Drawing.Point(21, 29);
            this.GroupBox4.Name = "GroupBox4";
            this.GroupBox4.Size = new System.Drawing.Size(728, 232);
            this.GroupBox4.TabIndex = 9;
            this.GroupBox4.TabStop = false;
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.BackColor = System.Drawing.SystemColors.Window;
            this.StatusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.StatusStrip1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel1,
            this.ssdate,
            this.sstime});
            this.StatusStrip1.Location = new System.Drawing.Point(310, 9);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StatusStrip1.Size = new System.Drawing.Size(507, 28);
            this.StatusStrip1.TabIndex = 39;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // ToolStripStatusLabel1
            // 
            this.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1";
            this.ToolStripStatusLabel1.Size = new System.Drawing.Size(86, 23);
            this.ToolStripStatusLabel1.Text = "Сегодня";
            // 
            // ssdate
            // 
            this.ssdate.Name = "ssdate";
            this.ssdate.Size = new System.Drawing.Size(202, 23);
            this.ssdate.Text = "ToolStripStatusLabel2";
            // 
            // sstime
            // 
            this.sstime.Name = "sstime";
            this.sstime.Size = new System.Drawing.Size(202, 23);
            this.sstime.Text = "ToolStripStatusLabel3";
            // 
            // TabControl2
            // 
            this.TabControl2.Controls.Add(this.TabPage9);
            this.TabControl2.Controls.Add(this.TabPage10);
            this.TabControl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.TabControl2.Location = new System.Drawing.Point(3, 13);
            this.TabControl2.Name = "TabControl2";
            this.TabControl2.SelectedIndex = 0;
            this.TabControl2.Size = new System.Drawing.Size(721, 213);
            this.TabControl2.TabIndex = 0;
            // 
            // TabPage9
            // 
            this.TabPage9.Controls.Add(this.DataGridView1);
            this.TabPage9.Location = new System.Drawing.Point(4, 25);
            this.TabPage9.Name = "TabPage9";
            this.TabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage9.Size = new System.Drawing.Size(713, 184);
            this.TabPage9.TabIndex = 0;
            this.TabPage9.Text = "Неисправность";
            this.TabPage9.UseVisualStyleBackColor = true;
            // 
            // DataGridView1
            // 
            this.DataGridView1.AllowUserToAddRows = false;
            this.DataGridView1.AllowUserToDeleteRows = false;
            this.DataGridView1.AllowUserToOrderColumns = true;
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Location = new System.Drawing.Point(6, 7);
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.ReadOnly = true;
            this.DataGridView1.Size = new System.Drawing.Size(701, 171);
            this.DataGridView1.TabIndex = 3;
            // 
            // TabPage10
            // 
            this.TabPage10.AutoScroll = true;
            this.TabPage10.Controls.Add(this.Predmet_ystLabel1);
            this.TabPage10.Controls.Add(this.NameLabel1);
            this.TabPage10.Controls.Add(this.Time_2Label1);
            this.TabPage10.Controls.Add(this.Data_2Label1);
            this.TabPage10.Controls.Add(this.Label30);
            this.TabPage10.Controls.Add(this.Label29);
            this.TabPage10.Controls.Add(this.Label28);
            this.TabPage10.Controls.Add(this.Label27);
            this.TabPage10.Controls.Add(this.DataGridView2);
            this.TabPage10.Location = new System.Drawing.Point(4, 25);
            this.TabPage10.Name = "TabPage10";
            this.TabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage10.Size = new System.Drawing.Size(720, 159);
            this.TabPage10.TabIndex = 1;
            this.TabPage10.Text = "Проверка исполнения";
            this.TabPage10.UseVisualStyleBackColor = true;
            // 
            // Predmet_ystLabel1
            // 
            this.Predmet_ystLabel1.Location = new System.Drawing.Point(561, 9);
            this.Predmet_ystLabel1.Name = "Predmet_ystLabel1";
            this.Predmet_ystLabel1.Size = new System.Drawing.Size(130, 23);
            this.Predmet_ystLabel1.TabIndex = 13;
            this.Predmet_ystLabel1.Text = "Нет данных";
            // 
            // NameLabel1
            // 
            this.NameLabel1.Location = new System.Drawing.Point(561, 40);
            this.NameLabel1.Name = "NameLabel1";
            this.NameLabel1.Size = new System.Drawing.Size(100, 23);
            this.NameLabel1.TabIndex = 15;
            this.NameLabel1.Text = "Нет данных";
            // 
            // Time_2Label1
            // 
            this.Time_2Label1.Location = new System.Drawing.Point(561, 73);
            this.Time_2Label1.Name = "Time_2Label1";
            this.Time_2Label1.Size = new System.Drawing.Size(100, 23);
            this.Time_2Label1.TabIndex = 17;
            this.Time_2Label1.Text = "Нет данных";
            // 
            // Data_2Label1
            // 
            this.Data_2Label1.Location = new System.Drawing.Point(561, 103);
            this.Data_2Label1.Name = "Data_2Label1";
            this.Data_2Label1.Size = new System.Drawing.Size(100, 23);
            this.Data_2Label1.TabIndex = 19;
            this.Data_2Label1.Text = "Нет данных";
            // 
            // Label30
            // 
            this.Label30.AutoSize = true;
            this.Label30.Location = new System.Drawing.Point(380, 9);
            this.Label30.Name = "Label30";
            this.Label30.Size = new System.Drawing.Size(148, 17);
            this.Label30.TabIndex = 8;
            this.Label30.Text = "Что было  устранено";
            // 
            // Label29
            // 
            this.Label29.AutoSize = true;
            this.Label29.Location = new System.Drawing.Point(380, 78);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(135, 17);
            this.Label29.TabIndex = 7;
            this.Label29.Text = "Время  устранения";
            // 
            // Label28
            // 
            this.Label28.AutoSize = true;
            this.Label28.Location = new System.Drawing.Point(380, 108);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(123, 17);
            this.Label28.TabIndex = 6;
            this.Label28.Text = "Дата устранения";
            // 
            // Label27
            // 
            this.Label27.AutoSize = true;
            this.Label27.Location = new System.Drawing.Point(380, 45);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(107, 17);
            this.Label27.TabIndex = 5;
            this.Label27.Text = "Кем устранено";
            // 
            // DataGridView2
            // 
            this.DataGridView2.AllowUserToAddRows = false;
            this.DataGridView2.AllowUserToDeleteRows = false;
            this.DataGridView2.AllowUserToOrderColumns = true;
            this.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView2.Location = new System.Drawing.Point(9, 3);
            this.DataGridView2.Name = "DataGridView2";
            this.DataGridView2.ReadOnly = true;
            this.DataGridView2.Size = new System.Drawing.Size(356, 150);
            this.DataGridView2.TabIndex = 4;
            // 
            // GroupBox3
            // 
            this.GroupBox3.Controls.Add(this.Button1);
            this.GroupBox3.Controls.Add(this.DateTimePicker2);
            this.GroupBox3.Controls.Add(this.DateTimePicker1);
            this.GroupBox3.Controls.Add(this.MenuStrip2);
            this.GroupBox3.Location = new System.Drawing.Point(21, 257);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(731, 64);
            this.GroupBox3.TabIndex = 8;
            this.GroupBox3.TabStop = false;
            // 
            // Button1
            // 
            this.Button1.Location = new System.Drawing.Point(499, 22);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(219, 33);
            this.Button1.TabIndex = 13;
            this.Button1.Text = "Сформировать запрос";
            this.Button1.UseVisualStyleBackColor = true;
            // 
            // DateTimePicker2
            // 
            this.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTimePicker2.Location = new System.Drawing.Point(310, 25);
            this.DateTimePicker2.Name = "DateTimePicker2";
            this.DateTimePicker2.Size = new System.Drawing.Size(112, 30);
            this.DateTimePicker2.TabIndex = 12;
            // 
            // DateTimePicker1
            // 
            this.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTimePicker1.Location = new System.Drawing.Point(169, 26);
            this.DateTimePicker1.Name = "DateTimePicker1";
            this.DateTimePicker1.Size = new System.Drawing.Size(118, 30);
            this.DateTimePicker1.TabIndex = 11;
            // 
            // MenuStrip2
            // 
            this.MenuStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.MenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem1});
            this.MenuStrip2.Location = new System.Drawing.Point(13, 25);
            this.MenuStrip2.Name = "MenuStrip2";
            this.MenuStrip2.Size = new System.Drawing.Size(153, 31);
            this.MenuStrip2.TabIndex = 3;
            this.MenuStrip2.Text = "MenuStrip2";
            // 
            // ToolStripMenuItem1
            // 
            this.ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem4,
            this.ЗапросОУстраненииНеисправностейToolStripMenuItem});
            this.ToolStripMenuItem1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ToolStripMenuItem1.Name = "ToolStripMenuItem1";
            this.ToolStripMenuItem1.Size = new System.Drawing.Size(145, 27);
            this.ToolStripMenuItem1.Text = "Список задач";
            // 
            // ToolStripMenuItem4
            // 
            this.ToolStripMenuItem4.Name = "ToolStripMenuItem4";
            this.ToolStripMenuItem4.Size = new System.Drawing.Size(438, 28);
            this.ToolStripMenuItem4.Text = "Подготовка акта о осмотре";
            // 
            // ЗапросОУстраненииНеисправностейToolStripMenuItem
            // 
            this.ЗапросОУстраненииНеисправностейToolStripMenuItem.Name = "ЗапросОУстраненииНеисправностейToolStripMenuItem";
            this.ЗапросОУстраненииНеисправностейToolStripMenuItem.Size = new System.Drawing.Size(438, 28);
            this.ЗапросОУстраненииНеисправностейToolStripMenuItem.Text = "Запрос об устранении  неисправностей";
            // 
            // Panel2
            // 
            this.Panel2.Controls.Add(this.TabControl3);
            this.Panel2.Controls.Add(this.CheckBox2);
            this.Panel2.Controls.Add(this.CheckBox1);
            this.Panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.Panel2.Location = new System.Drawing.Point(21, 330);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(731, 298);
            this.Panel2.TabIndex = 5;
            // 
            // TabControl3
            // 
            this.TabControl3.Controls.Add(this.TabPage3);
            this.TabControl3.Controls.Add(this.TabPage4);
            this.TabControl3.Controls.Add(this.TabPage6);
            this.TabControl3.Controls.Add(this.TabPage5);
            this.TabControl3.Controls.Add(this.TabPage7);
            this.TabControl3.Controls.Add(this.TabPage2);
            this.TabControl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.TabControl3.Location = new System.Drawing.Point(10, 44);
            this.TabControl3.Name = "TabControl3";
            this.TabControl3.SelectedIndex = 0;
            this.TabControl3.Size = new System.Drawing.Size(714, 251);
            this.TabControl3.TabIndex = 0;
            // 
            // TabPage3
            // 
            this.TabPage3.Controls.Add(this.MenuStrip3);
            this.TabPage3.Controls.Add(this.BindingNavigator1);
            this.TabPage3.Controls.Add(this.Label14);
            this.TabPage3.Controls.Add(this.TextBox6);
            this.TabPage3.Controls.Add(this.Label5);
            this.TabPage3.Controls.Add(this.CheckBox4);
            this.TabPage3.Location = new System.Drawing.Point(4, 25);
            this.TabPage3.Name = "TabPage3";
            this.TabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage3.Size = new System.Drawing.Size(706, 211);
            this.TabPage3.TabIndex = 0;
            this.TabPage3.Text = "Песочница";
            this.TabPage3.UseVisualStyleBackColor = true;
            // 
            // MenuStrip3
            // 
            this.MenuStrip3.Dock = System.Windows.Forms.DockStyle.None;
            this.MenuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem3});
            this.MenuStrip3.Location = new System.Drawing.Point(498, 93);
            this.MenuStrip3.Name = "MenuStrip3";
            this.MenuStrip3.Size = new System.Drawing.Size(190, 31);
            this.MenuStrip3.TabIndex = 23;
            this.MenuStrip3.Text = "MenuStrip3";
            // 
            // ToolStripMenuItem3
            // 
            this.ToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem10});
            this.ToolStripMenuItem3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ToolStripMenuItem3.Name = "ToolStripMenuItem3";
            this.ToolStripMenuItem3.Size = new System.Drawing.Size(182, 27);
            this.ToolStripMenuItem3.Text = "Отправка данных";
            // 
            // ToolStripMenuItem10
            // 
            this.ToolStripMenuItem10.Name = "ToolStripMenuItem10";
            this.ToolStripMenuItem10.Size = new System.Drawing.Size(172, 28);
            this.ToolStripMenuItem10.Text = "Слесарям";
            // 
            // BindingNavigator1
            // 
            this.BindingNavigator1.AddNewItem = null;
            this.BindingNavigator1.AutoSize = false;
            this.BindingNavigator1.CountItem = null;
            this.BindingNavigator1.DeleteItem = null;
            this.BindingNavigator1.Dock = System.Windows.Forms.DockStyle.None;
            this.BindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Vagon_spisokBindingNavigatorSaveItem21});
            this.BindingNavigator1.Location = new System.Drawing.Point(638, 159);
            this.BindingNavigator1.MoveFirstItem = null;
            this.BindingNavigator1.MoveLastItem = null;
            this.BindingNavigator1.MoveNextItem = null;
            this.BindingNavigator1.MovePreviousItem = null;
            this.BindingNavigator1.Name = "BindingNavigator1";
            this.BindingNavigator1.PositionItem = null;
            this.BindingNavigator1.Size = new System.Drawing.Size(35, 25);
            this.BindingNavigator1.TabIndex = 21;
            this.BindingNavigator1.Text = "BindingNavigator1";
            // 
            // Vagon_spisokBindingNavigatorSaveItem21
            // 
            this.Vagon_spisokBindingNavigatorSaveItem21.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Vagon_spisokBindingNavigatorSaveItem21.Image = ((System.Drawing.Image)(resources.GetObject("Vagon_spisokBindingNavigatorSaveItem21.Image")));
            this.Vagon_spisokBindingNavigatorSaveItem21.Name = "Vagon_spisokBindingNavigatorSaveItem21";
            this.Vagon_spisokBindingNavigatorSaveItem21.Size = new System.Drawing.Size(23, 22);
            this.Vagon_spisokBindingNavigatorSaveItem21.Text = "Сохранить данные";
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(29, 71);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(82, 17);
            this.Label14.TabIndex = 20;
            this.Label14.Text = "Замечание";
            // 
            // TextBox6
            // 
            this.TextBox6.Location = new System.Drawing.Point(118, 68);
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.Size = new System.Drawing.Size(554, 22);
            this.TextBox6.TabIndex = 19;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(30, 15);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(81, 17);
            this.Label5.TabIndex = 2;
            this.Label5.Text = "Песочница";
            // 
            // CheckBox4
            // 
            this.CheckBox4.AutoSize = true;
            this.CheckBox4.Location = new System.Drawing.Point(118, 16);
            this.CheckBox4.Name = "CheckBox4";
            this.CheckBox4.Size = new System.Drawing.Size(18, 17);
            this.CheckBox4.TabIndex = 1;
            this.CheckBox4.ThreeState = true;
            this.CheckBox4.UseVisualStyleBackColor = true;
            // 
            // TabPage4
            // 
            this.TabPage4.Controls.Add(this.menuStrip4);
            this.TabPage4.Controls.Add(this.label7);
            this.TabPage4.Controls.Add(this.textBox5);
            this.TabPage4.Controls.Add(this.BindingNavigator2);
            this.TabPage4.Controls.Add(this.CheckBox3);
            this.TabPage4.Controls.Add(this.Label13);
            this.TabPage4.Location = new System.Drawing.Point(4, 25);
            this.TabPage4.Name = "TabPage4";
            this.TabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage4.Size = new System.Drawing.Size(706, 211);
            this.TabPage4.TabIndex = 1;
            this.TabPage4.Text = "Группы освещения";
            this.TabPage4.UseVisualStyleBackColor = true;
            // 
            // BindingNavigator2
            // 
            this.BindingNavigator2.AddNewItem = null;
            this.BindingNavigator2.CountItem = null;
            this.BindingNavigator2.DeleteItem = null;
            this.BindingNavigator2.Dock = System.Windows.Forms.DockStyle.None;
            this.BindingNavigator2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButton1});
            this.BindingNavigator2.Location = new System.Drawing.Point(638, 159);
            this.BindingNavigator2.MoveFirstItem = null;
            this.BindingNavigator2.MoveLastItem = null;
            this.BindingNavigator2.MoveNextItem = null;
            this.BindingNavigator2.MovePreviousItem = null;
            this.BindingNavigator2.Name = "BindingNavigator2";
            this.BindingNavigator2.PositionItem = null;
            this.BindingNavigator2.Size = new System.Drawing.Size(35, 25);
            this.BindingNavigator2.TabIndex = 21;
            this.BindingNavigator2.Text = "BindingNavigator2";
            // 
            // ToolStripButton1
            // 
            this.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripButton1.Image")));
            this.ToolStripButton1.Name = "ToolStripButton1";
            this.ToolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButton1.Text = "Сохранить данные";
            // 
            // CheckBox3
            // 
            this.CheckBox3.AutoSize = true;
            this.CheckBox3.Location = new System.Drawing.Point(170, 16);
            this.CheckBox3.Name = "CheckBox3";
            this.CheckBox3.Size = new System.Drawing.Size(18, 17);
            this.CheckBox3.TabIndex = 4;
            this.CheckBox3.ThreeState = true;
            this.CheckBox3.UseVisualStyleBackColor = true;
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(30, 15);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(134, 17);
            this.Label13.TabIndex = 3;
            this.Label13.Text = "Группы освещения";
            // 
            // TabPage6
            // 
            this.TabPage6.Controls.Add(this.menuStrip8);
            this.TabPage6.Controls.Add(this.label4);
            this.TabPage6.Controls.Add(this.textBox7);
            this.TabPage6.Controls.Add(this.BindingNavigator3);
            this.TabPage6.Controls.Add(this.CheckBox5);
            this.TabPage6.Controls.Add(this.Label18);
            this.TabPage6.Location = new System.Drawing.Point(4, 25);
            this.TabPage6.Name = "TabPage6";
            this.TabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage6.Size = new System.Drawing.Size(706, 211);
            this.TabPage6.TabIndex = 3;
            this.TabPage6.Text = "Группы отопителей";
            this.TabPage6.UseVisualStyleBackColor = true;
            // 
            // BindingNavigator3
            // 
            this.BindingNavigator3.AddNewItem = null;
            this.BindingNavigator3.CountItem = null;
            this.BindingNavigator3.DeleteItem = null;
            this.BindingNavigator3.Dock = System.Windows.Forms.DockStyle.None;
            this.BindingNavigator3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButton2});
            this.BindingNavigator3.Location = new System.Drawing.Point(638, 159);
            this.BindingNavigator3.MoveFirstItem = null;
            this.BindingNavigator3.MoveLastItem = null;
            this.BindingNavigator3.MoveNextItem = null;
            this.BindingNavigator3.MovePreviousItem = null;
            this.BindingNavigator3.Name = "BindingNavigator3";
            this.BindingNavigator3.PositionItem = null;
            this.BindingNavigator3.Size = new System.Drawing.Size(35, 25);
            this.BindingNavigator3.TabIndex = 19;
            this.BindingNavigator3.Text = "BindingNavigator3";
            // 
            // ToolStripButton2
            // 
            this.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripButton2.Image")));
            this.ToolStripButton2.Name = "ToolStripButton2";
            this.ToolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButton2.Text = "Сохранить данные";
            // 
            // CheckBox5
            // 
            this.CheckBox5.AutoSize = true;
            this.CheckBox5.Location = new System.Drawing.Point(170, 16);
            this.CheckBox5.Name = "CheckBox5";
            this.CheckBox5.Size = new System.Drawing.Size(18, 17);
            this.CheckBox5.TabIndex = 12;
            this.CheckBox5.ThreeState = true;
            this.CheckBox5.UseVisualStyleBackColor = true;
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Location = new System.Drawing.Point(30, 15);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(132, 17);
            this.Label18.TabIndex = 11;
            this.Label18.Text = "Группы отопления";
            // 
            // TabPage5
            // 
            this.TabPage5.Controls.Add(this.menuStrip5);
            this.TabPage5.Controls.Add(this.label10);
            this.TabPage5.Controls.Add(this.textBox10);
            this.TabPage5.Controls.Add(this.bindingNavigator4);
            this.TabPage5.Controls.Add(this.checkBox15);
            this.TabPage5.Controls.Add(this.label8);
            this.TabPage5.Controls.Add(this.MenuStrip6);
            this.TabPage5.Controls.Add(this.CheckBox6);
            this.TabPage5.Controls.Add(this.Label23);
            this.TabPage5.Location = new System.Drawing.Point(4, 25);
            this.TabPage5.Name = "TabPage5";
            this.TabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage5.Size = new System.Drawing.Size(706, 211);
            this.TabPage5.TabIndex = 2;
            this.TabPage5.Text = "Двигатели ";
            this.TabPage5.UseVisualStyleBackColor = true;
            // 
            // MenuStrip6
            // 
            this.MenuStrip6.Dock = System.Windows.Forms.DockStyle.None;
            this.MenuStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem12});
            this.MenuStrip6.Location = new System.Drawing.Point(493, 168);
            this.MenuStrip6.Name = "MenuStrip6";
            this.MenuStrip6.Size = new System.Drawing.Size(190, 31);
            this.MenuStrip6.TabIndex = 24;
            this.MenuStrip6.Text = "MenuStrip6";
            this.MenuStrip6.Visible = false;
            // 
            // ToolStripMenuItem12
            // 
            this.ToolStripMenuItem12.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem13});
            this.ToolStripMenuItem12.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ToolStripMenuItem12.Name = "ToolStripMenuItem12";
            this.ToolStripMenuItem12.Size = new System.Drawing.Size(182, 27);
            this.ToolStripMenuItem12.Text = "Отправка данных";
            // 
            // ToolStripMenuItem13
            // 
            this.ToolStripMenuItem13.Name = "ToolStripMenuItem13";
            this.ToolStripMenuItem13.Size = new System.Drawing.Size(172, 28);
            this.ToolStripMenuItem13.Text = "Слесарям";
            // 
            // CheckBox6
            // 
            this.CheckBox6.AutoSize = true;
            this.CheckBox6.Location = new System.Drawing.Point(244, 20);
            this.CheckBox6.Name = "CheckBox6";
            this.CheckBox6.Size = new System.Drawing.Size(18, 17);
            this.CheckBox6.TabIndex = 12;
            this.CheckBox6.ThreeState = true;
            this.CheckBox6.UseVisualStyleBackColor = true;
            // 
            // Label23
            // 
            this.Label23.AutoSize = true;
            this.Label23.Location = new System.Drawing.Point(30, 15);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(133, 17);
            this.Label23.TabIndex = 11;
            this.Label23.Text = "Тяговый двигатель";
            // 
            // TabPage7
            // 
            this.TabPage7.Controls.Add(this.menuStrip7);
            this.TabPage7.Controls.Add(this.label15);
            this.TabPage7.Controls.Add(this.textBox11);
            this.TabPage7.Controls.Add(this.bindingNavigator5);
            this.TabPage7.Controls.Add(this.CheckBox9);
            this.TabPage7.Controls.Add(this.CheckBox8);
            this.TabPage7.Controls.Add(this.CheckBox7);
            this.TabPage7.Location = new System.Drawing.Point(4, 25);
            this.TabPage7.Name = "TabPage7";
            this.TabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage7.Size = new System.Drawing.Size(706, 222);
            this.TabPage7.TabIndex = 4;
            this.TabPage7.Text = "Тормозная система";
            this.TabPage7.UseVisualStyleBackColor = true;
            // 
            // CheckBox9
            // 
            this.CheckBox9.AutoSize = true;
            this.CheckBox9.Location = new System.Drawing.Point(25, 58);
            this.CheckBox9.Name = "CheckBox9";
            this.CheckBox9.Size = new System.Drawing.Size(153, 21);
            this.CheckBox9.TabIndex = 2;
            this.CheckBox9.Text = "Рельсовый тормоз";
            this.CheckBox9.UseVisualStyleBackColor = true;
            // 
            // CheckBox8
            // 
            this.CheckBox8.AutoSize = true;
            this.CheckBox8.Location = new System.Drawing.Point(25, 32);
            this.CheckBox8.Name = "CheckBox8";
            this.CheckBox8.Size = new System.Drawing.Size(164, 21);
            this.CheckBox8.TabIndex = 1;
            this.CheckBox8.Text = "Колодочный тормоз";
            this.CheckBox8.UseVisualStyleBackColor = true;
            // 
            // CheckBox7
            // 
            this.CheckBox7.AutoSize = true;
            this.CheckBox7.Location = new System.Drawing.Point(25, 9);
            this.CheckBox7.Name = "CheckBox7";
            this.CheckBox7.Size = new System.Drawing.Size(231, 21);
            this.CheckBox7.TabIndex = 0;
            this.CheckBox7.Text = "Электродинамический тормоз";
            this.CheckBox7.UseVisualStyleBackColor = true;
            // 
            // TabPage2
            // 
            this.TabPage2.Controls.Add(this.menuStrip9);
            this.TabPage2.Controls.Add(this.label16);
            this.TabPage2.Controls.Add(this.textBox12);
            this.TabPage2.Controls.Add(this.bindingNavigator6);
            this.TabPage2.Controls.Add(this.CheckBox14);
            this.TabPage2.Controls.Add(this.CheckBox13);
            this.TabPage2.Controls.Add(this.CheckBox12);
            this.TabPage2.Controls.Add(this.CheckBox11);
            this.TabPage2.Location = new System.Drawing.Point(4, 25);
            this.TabPage2.Name = "TabPage2";
            this.TabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage2.Size = new System.Drawing.Size(706, 222);
            this.TabPage2.TabIndex = 5;
            this.TabPage2.Text = "Прочее";
            this.TabPage2.UseVisualStyleBackColor = true;
            // 
            // CheckBox14
            // 
            this.CheckBox14.AutoSize = true;
            this.CheckBox14.Location = new System.Drawing.Point(25, 90);
            this.CheckBox14.Name = "CheckBox14";
            this.CheckBox14.Size = new System.Drawing.Size(165, 21);
            this.CheckBox14.TabIndex = 7;
            this.CheckBox14.Text = "Целостность стёкол";
            this.CheckBox14.ThreeState = true;
            this.CheckBox14.UseVisualStyleBackColor = true;
            // 
            // CheckBox13
            // 
            this.CheckBox13.AutoSize = true;
            this.CheckBox13.Location = new System.Drawing.Point(25, 63);
            this.CheckBox13.Name = "CheckBox13";
            this.CheckBox13.Size = new System.Drawing.Size(242, 21);
            this.CheckBox13.TabIndex = 5;
            this.CheckBox13.Text = "Давление на контакный провод";
            this.CheckBox13.ThreeState = true;
            this.CheckBox13.UseVisualStyleBackColor = true;
            // 
            // CheckBox12
            // 
            this.CheckBox12.AutoSize = true;
            this.CheckBox12.Location = new System.Drawing.Point(25, 36);
            this.CheckBox12.Name = "CheckBox12";
            this.CheckBox12.Size = new System.Drawing.Size(205, 21);
            this.CheckBox12.TabIndex = 4;
            this.CheckBox12.Text = "Сигнальное оборудование";
            this.CheckBox12.ThreeState = true;
            this.CheckBox12.UseVisualStyleBackColor = true;
            // 
            // CheckBox11
            // 
            this.CheckBox11.AutoSize = true;
            this.CheckBox11.Location = new System.Drawing.Point(25, 9);
            this.CheckBox11.Name = "CheckBox11";
            this.CheckBox11.Size = new System.Drawing.Size(68, 21);
            this.CheckBox11.TabIndex = 3;
            this.CheckBox11.Text = "Связь";
            this.CheckBox11.ThreeState = true;
            this.CheckBox11.UseVisualStyleBackColor = true;
            // 
            // CheckBox2
            // 
            this.CheckBox2.AutoSize = true;
            this.CheckBox2.Checked = true;
            this.CheckBox2.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.CheckBox2.Enabled = false;
            this.CheckBox2.Location = new System.Drawing.Point(143, 13);
            this.CheckBox2.Name = "CheckBox2";
            this.CheckBox2.Size = new System.Drawing.Size(216, 21);
            this.CheckBox2.TabIndex = 1;
            this.CheckBox2.Text = "Проверено, есть замечания\r\n";
            this.CheckBox2.UseVisualStyleBackColor = true;
            // 
            // CheckBox1
            // 
            this.CheckBox1.AutoSize = true;
            this.CheckBox1.Checked = true;
            this.CheckBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox1.Enabled = false;
            this.CheckBox1.Location = new System.Drawing.Point(13, 13);
            this.CheckBox1.Name = "CheckBox1";
            this.CheckBox1.Size = new System.Drawing.Size(103, 21);
            this.CheckBox1.TabIndex = 0;
            this.CheckBox1.Text = "Проверено";
            this.CheckBox1.UseVisualStyleBackColor = true;
            // 
            // GroupBox1
            // 
            this.GroupBox1.BackColor = System.Drawing.SystemColors.Window;
            this.GroupBox1.Controls.Add(this.Panel6);
            this.GroupBox1.Controls.Add(this.TextBox4);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.Panel4);
            this.GroupBox1.Controls.Add(this.PictureBox1);
            this.GroupBox1.Controls.Add(this.TextBox1);
            this.GroupBox1.Controls.Add(this.TextBox8);
            this.GroupBox1.Controls.Add(this.TextBox9);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Controls.Add(this.Label12);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.TextBox2);
            this.GroupBox1.Controls.Add(this.TextBox3);
            this.GroupBox1.Controls.Add(this.Label9);
            this.GroupBox1.Controls.Add(this.Label6);
            this.GroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.GroupBox1.Location = new System.Drawing.Point(3, 3);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(306, 640);
            this.GroupBox1.TabIndex = 2;
            this.GroupBox1.TabStop = false;
            // 
            // Panel6
            // 
            this.Panel6.Controls.Add(NadbavkaLabel);
            this.Panel6.Controls.Add(this.NadbavkaTextBox);
            this.Panel6.Controls.Add(Indikator_2Label);
            this.Panel6.Controls.Add(this.Indikator_2TextBox);
            this.Panel6.Controls.Add(Indikator_1Label);
            this.Panel6.Controls.Add(this.Indikator_1TextBox);
            this.Panel6.Location = new System.Drawing.Point(21, 542);
            this.Panel6.Name = "Panel6";
            this.Panel6.Size = new System.Drawing.Size(242, 92);
            this.Panel6.TabIndex = 42;
            // 
            // NadbavkaLabel
            // 
            NadbavkaLabel.AutoSize = true;
            NadbavkaLabel.Location = new System.Drawing.Point(20, 67);
            NadbavkaLabel.Name = "NadbavkaLabel";
            NadbavkaLabel.Size = new System.Drawing.Size(76, 17);
            NadbavkaLabel.TabIndex = 4;
            NadbavkaLabel.Text = "Nadbavka:";
            // 
            // NadbavkaTextBox
            // 
            this.NadbavkaTextBox.Location = new System.Drawing.Point(109, 64);
            this.NadbavkaTextBox.Name = "NadbavkaTextBox";
            this.NadbavkaTextBox.Size = new System.Drawing.Size(100, 22);
            this.NadbavkaTextBox.TabIndex = 5;
            // 
            // Indikator_2Label
            // 
            Indikator_2Label.AutoSize = true;
            Indikator_2Label.Location = new System.Drawing.Point(19, 37);
            Indikator_2Label.Name = "Indikator_2Label";
            Indikator_2Label.Size = new System.Drawing.Size(78, 17);
            Indikator_2Label.TabIndex = 2;
            Indikator_2Label.Text = "Indikator 2:";
            // 
            // Indikator_2TextBox
            // 
            this.Indikator_2TextBox.Location = new System.Drawing.Point(109, 34);
            this.Indikator_2TextBox.Name = "Indikator_2TextBox";
            this.Indikator_2TextBox.Size = new System.Drawing.Size(100, 22);
            this.Indikator_2TextBox.TabIndex = 3;
            // 
            // Indikator_1Label
            // 
            Indikator_1Label.AutoSize = true;
            Indikator_1Label.Location = new System.Drawing.Point(19, 6);
            Indikator_1Label.Name = "Indikator_1Label";
            Indikator_1Label.Size = new System.Drawing.Size(78, 17);
            Indikator_1Label.TabIndex = 0;
            Indikator_1Label.Text = "Indikator 1:";
            // 
            // Indikator_1TextBox
            // 
            this.Indikator_1TextBox.Location = new System.Drawing.Point(109, 3);
            this.Indikator_1TextBox.Name = "Indikator_1TextBox";
            this.Indikator_1TextBox.Size = new System.Drawing.Size(100, 22);
            this.Indikator_1TextBox.TabIndex = 1;
            // 
            // TextBox4
            // 
            this.TextBox4.Location = new System.Drawing.Point(193, 457);
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.ReadOnly = true;
            this.TextBox4.Size = new System.Drawing.Size(100, 22);
            this.TextBox4.TabIndex = 40;
            this.TextBox4.Text = "Нет данных";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(18, 460);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(161, 17);
            this.Label3.TabIndex = 39;
            this.Label3.Text = "Специальное указание";
            // 
            // Panel4
            // 
            this.Panel4.Controls.Add(this.Label36);
            this.Panel4.Controls.Add(this.Label37);
            this.Panel4.Location = new System.Drawing.Point(21, 489);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(168, 41);
            this.Panel4.TabIndex = 38;
            // 
            // Label36
            // 
            this.Label36.AutoSize = true;
            this.Label36.Location = new System.Drawing.Point(94, 11);
            this.Label36.Name = "Label36";
            this.Label36.Size = new System.Drawing.Size(40, 17);
            this.Label36.TabIndex = 1;
            this.Label36.Text = "1270";
            // 
            // Label37
            // 
            this.Label37.AutoSize = true;
            this.Label37.Location = new System.Drawing.Point(16, 11);
            this.Label37.Name = "Label37";
            this.Label37.Size = new System.Drawing.Size(67, 17);
            this.Label37.TabIndex = 0;
            this.Label37.Text = "Табел №";
            // 
            // PictureBox1
            // 
            this.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureBox1.Location = new System.Drawing.Point(40, 63);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(253, 177);
            this.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox1.TabIndex = 20;
            this.PictureBox1.TabStop = false;
            // 
            // TextBox1
            // 
            this.TextBox1.Location = new System.Drawing.Point(193, 268);
            this.TextBox1.MaxLength = 4;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Size = new System.Drawing.Size(100, 22);
            this.TextBox1.TabIndex = 3;
            // 
            // TextBox8
            // 
            this.TextBox8.Location = new System.Drawing.Point(193, 425);
            this.TextBox8.Name = "TextBox8";
            this.TextBox8.ReadOnly = true;
            this.TextBox8.Size = new System.Drawing.Size(100, 22);
            this.TextBox8.TabIndex = 18;
            this.TextBox8.Text = "Нет данных";
            // 
            // TextBox9
            // 
            this.TextBox9.Location = new System.Drawing.Point(193, 332);
            this.TextBox9.Name = "TextBox9";
            this.TextBox9.ReadOnly = true;
            this.TextBox9.Size = new System.Drawing.Size(100, 22);
            this.TextBox9.TabIndex = 16;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(18, 276);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(64, 17);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Вагон №";
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(18, 405);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(186, 17);
            this.Label12.TabIndex = 16;
            this.Label12.Text = "Дата  последнего ремонта";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(18, 308);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(58, 17);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "Модель";
            // 
            // TextBox2
            // 
            this.TextBox2.Location = new System.Drawing.Point(193, 300);
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.ReadOnly = true;
            this.TextBox2.Size = new System.Drawing.Size(100, 22);
            this.TextBox2.TabIndex = 4;
            // 
            // TextBox3
            // 
            this.TextBox3.Location = new System.Drawing.Point(193, 364);
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.ReadOnly = true;
            this.TextBox3.Size = new System.Drawing.Size(100, 22);
            this.TextBox3.TabIndex = 10;
            this.TextBox3.Text = "Нет данных";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(18, 340);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(90, 17);
            this.Label9.TabIndex = 14;
            this.Label9.Text = "Год выпуска";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(18, 372);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(133, 17);
            this.Label6.TabIndex = 6;
            this.Label6.Text = "Последний ремонт";
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.SystemColors.Window;
            this.Panel3.Controls.Add(this.PictureBox2);
            this.Panel3.Controls.Add(this.GroupBox5);
            this.Panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel3.Location = new System.Drawing.Point(1093, 3);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(188, 640);
            this.Panel3.TabIndex = 22;
            // 
            // PictureBox2
            // 
            this.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureBox2.Location = new System.Drawing.Point(21, 69);
            this.PictureBox2.Name = "PictureBox2";
            this.PictureBox2.Size = new System.Drawing.Size(151, 119);
            this.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox2.TabIndex = 0;
            this.PictureBox2.TabStop = false;
            // 
            // GroupBox5
            // 
            this.GroupBox5.Controls.Add(this.Label35);
            this.GroupBox5.Controls.Add(this.Label34);
            this.GroupBox5.Controls.Add(this.Label11);
            this.GroupBox5.Controls.Add(this.Label26);
            this.GroupBox5.Controls.Add(this.Label25);
            this.GroupBox5.Controls.Add(this.Label24);
            this.GroupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.GroupBox5.Location = new System.Drawing.Point(3, 235);
            this.GroupBox5.Name = "GroupBox5";
            this.GroupBox5.Size = new System.Drawing.Size(182, 213);
            this.GroupBox5.TabIndex = 22;
            this.GroupBox5.TabStop = false;
            this.GroupBox5.Text = "Мастер ТО №1";
            // 
            // Label35
            // 
            this.Label35.AutoSize = true;
            this.Label35.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label35.Location = new System.Drawing.Point(48, 191);
            this.Label35.Name = "Label35";
            this.Label35.Size = new System.Drawing.Size(81, 24);
            this.Label35.TabIndex = 40;
            this.Label35.Text = "Label35";
            // 
            // Label34
            // 
            this.Label34.AutoSize = true;
            this.Label34.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label34.Location = new System.Drawing.Point(48, 126);
            this.Label34.Name = "Label34";
            this.Label34.Size = new System.Drawing.Size(81, 24);
            this.Label34.TabIndex = 39;
            this.Label34.Text = "Label34";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label11.Location = new System.Drawing.Point(48, 66);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(80, 24);
            this.Label11.TabIndex = 38;
            this.Label11.Text = "Label11";
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Location = new System.Drawing.Point(15, 159);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(71, 17);
            this.Label26.TabIndex = 2;
            this.Label26.Text = "Отчество";
            // 
            // Label25
            // 
            this.Label25.AutoSize = true;
            this.Label25.Location = new System.Drawing.Point(15, 100);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(35, 17);
            this.Label25.TabIndex = 1;
            this.Label25.Text = "Имя";
            // 
            // Label24
            // 
            this.Label24.AutoSize = true;
            this.Label24.Location = new System.Drawing.Point(15, 39);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(70, 17);
            this.Label24.TabIndex = 0;
            this.Label24.Text = "Фамилия";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(29, 71);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 17);
            this.label7.TabIndex = 23;
            this.label7.Text = "Замечание";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(118, 68);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(554, 22);
            this.textBox5.TabIndex = 22;
            // 
            // menuStrip4
            // 
            this.menuStrip4.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5});
            this.menuStrip4.Location = new System.Drawing.Point(498, 93);
            this.menuStrip4.Name = "menuStrip4";
            this.menuStrip4.Size = new System.Drawing.Size(190, 31);
            this.menuStrip4.TabIndex = 24;
            this.menuStrip4.Text = "menuStrip4";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem6});
            this.toolStripMenuItem5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(182, 27);
            this.toolStripMenuItem5.Text = "Отправка данных";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(172, 28);
            this.toolStripMenuItem6.Text = "Слесарям";
            // 
            // menuStrip8
            // 
            this.menuStrip8.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip8.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem7});
            this.menuStrip8.Location = new System.Drawing.Point(498, 93);
            this.menuStrip8.Name = "menuStrip8";
            this.menuStrip8.Size = new System.Drawing.Size(190, 31);
            this.menuStrip8.TabIndex = 27;
            this.menuStrip8.Text = "menuStrip8";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem8});
            this.toolStripMenuItem7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(182, 27);
            this.toolStripMenuItem7.Text = "Отправка данных";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(172, 28);
            this.toolStripMenuItem8.Text = "Слесарям";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 17);
            this.label4.TabIndex = 26;
            this.label4.Text = "Замечание";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(118, 68);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(554, 22);
            this.textBox7.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(30, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(198, 17);
            this.label8.TabIndex = 25;
            this.label8.Text = "Двигатель постоянного тока";
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(244, 44);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(18, 17);
            this.checkBox15.TabIndex = 26;
            this.checkBox15.ThreeState = true;
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // menuStrip5
            // 
            this.menuStrip5.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem9});
            this.menuStrip5.Location = new System.Drawing.Point(498, 93);
            this.menuStrip5.Name = "menuStrip5";
            this.menuStrip5.Size = new System.Drawing.Size(190, 31);
            this.menuStrip5.TabIndex = 31;
            this.menuStrip5.Text = "menuStrip5";
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem11});
            this.toolStripMenuItem9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(182, 27);
            this.toolStripMenuItem9.Text = "Отправка данных";
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(172, 28);
            this.toolStripMenuItem11.Text = "Слесарям";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(29, 71);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 17);
            this.label10.TabIndex = 30;
            this.label10.Text = "Замечание";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(118, 68);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(554, 22);
            this.textBox10.TabIndex = 29;
            // 
            // bindingNavigator4
            // 
            this.bindingNavigator4.AddNewItem = null;
            this.bindingNavigator4.CountItem = null;
            this.bindingNavigator4.DeleteItem = null;
            this.bindingNavigator4.Dock = System.Windows.Forms.DockStyle.None;
            this.bindingNavigator4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton3});
            this.bindingNavigator4.Location = new System.Drawing.Point(638, 159);
            this.bindingNavigator4.MoveFirstItem = null;
            this.bindingNavigator4.MoveLastItem = null;
            this.bindingNavigator4.MoveNextItem = null;
            this.bindingNavigator4.MovePreviousItem = null;
            this.bindingNavigator4.Name = "bindingNavigator4";
            this.bindingNavigator4.PositionItem = null;
            this.bindingNavigator4.Size = new System.Drawing.Size(35, 25);
            this.bindingNavigator4.TabIndex = 28;
            this.bindingNavigator4.Text = "bindingNavigator4";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "Сохранить данные";
            // 
            // menuStrip7
            // 
            this.menuStrip7.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem14});
            this.menuStrip7.Location = new System.Drawing.Point(485, 110);
            this.menuStrip7.Name = "menuStrip7";
            this.menuStrip7.Size = new System.Drawing.Size(190, 31);
            this.menuStrip7.TabIndex = 35;
            this.menuStrip7.Text = "menuStrip7";
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem15});
            this.toolStripMenuItem14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(182, 27);
            this.toolStripMenuItem14.Text = "Отправка данных";
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(172, 28);
            this.toolStripMenuItem15.Text = "Слесарям";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(24, 88);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 17);
            this.label15.TabIndex = 34;
            this.label15.Text = "Замечание";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(113, 85);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(554, 22);
            this.textBox11.TabIndex = 33;
            // 
            // bindingNavigator5
            // 
            this.bindingNavigator5.AddNewItem = null;
            this.bindingNavigator5.CountItem = null;
            this.bindingNavigator5.DeleteItem = null;
            this.bindingNavigator5.Dock = System.Windows.Forms.DockStyle.None;
            this.bindingNavigator5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton4});
            this.bindingNavigator5.Location = new System.Drawing.Point(633, 176);
            this.bindingNavigator5.MoveFirstItem = null;
            this.bindingNavigator5.MoveLastItem = null;
            this.bindingNavigator5.MoveNextItem = null;
            this.bindingNavigator5.MovePreviousItem = null;
            this.bindingNavigator5.Name = "bindingNavigator5";
            this.bindingNavigator5.PositionItem = null;
            this.bindingNavigator5.Size = new System.Drawing.Size(35, 25);
            this.bindingNavigator5.TabIndex = 32;
            this.bindingNavigator5.Text = "bindingNavigator5";
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton4.Text = "Сохранить данные";
            // 
            // menuStrip9
            // 
            this.menuStrip9.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip9.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem16});
            this.menuStrip9.Location = new System.Drawing.Point(486, 142);
            this.menuStrip9.Name = "menuStrip9";
            this.menuStrip9.Size = new System.Drawing.Size(190, 31);
            this.menuStrip9.TabIndex = 39;
            this.menuStrip9.Text = "menuStrip9";
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem17});
            this.toolStripMenuItem16.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(182, 27);
            this.toolStripMenuItem16.Text = "Отправка данных";
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            this.toolStripMenuItem17.Size = new System.Drawing.Size(172, 28);
            this.toolStripMenuItem17.Text = "Слесарям";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(25, 120);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(82, 17);
            this.label16.TabIndex = 38;
            this.label16.Text = "Замечание";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(114, 117);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(554, 22);
            this.textBox12.TabIndex = 37;
            // 
            // bindingNavigator6
            // 
            this.bindingNavigator6.AddNewItem = null;
            this.bindingNavigator6.CountItem = null;
            this.bindingNavigator6.DeleteItem = null;
            this.bindingNavigator6.Dock = System.Windows.Forms.DockStyle.None;
            this.bindingNavigator6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton5});
            this.bindingNavigator6.Location = new System.Drawing.Point(637, 180);
            this.bindingNavigator6.MoveFirstItem = null;
            this.bindingNavigator6.MoveLastItem = null;
            this.bindingNavigator6.MoveNextItem = null;
            this.bindingNavigator6.MovePreviousItem = null;
            this.bindingNavigator6.Name = "bindingNavigator6";
            this.bindingNavigator6.PositionItem = null;
            this.bindingNavigator6.Size = new System.Drawing.Size(35, 25);
            this.bindingNavigator6.TabIndex = 36;
            this.bindingNavigator6.Text = "bindingNavigator6";
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton5.Text = "Сохранить данные";
            // 
            // Sama_programma_tex_obs_1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1284, 778);
            this.Controls.Add(this.TableLayoutPanel1);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaximizeBox = false;
            this.Name = "Sama_programma_tex_obs_1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sama_programma_tex_obs_1";
            this.TableLayoutPanel1.ResumeLayout(false);
            this.Panel5.ResumeLayout(false);
            this.Panel5.PerformLayout();
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.MenuStrip1.ResumeLayout(false);
            this.MenuStrip1.PerformLayout();
            this.GroupBox4.ResumeLayout(false);
            this.GroupBox4.PerformLayout();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.TabControl2.ResumeLayout(false);
            this.TabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            this.TabPage10.ResumeLayout(false);
            this.TabPage10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView2)).EndInit();
            this.GroupBox3.ResumeLayout(false);
            this.GroupBox3.PerformLayout();
            this.MenuStrip2.ResumeLayout(false);
            this.MenuStrip2.PerformLayout();
            this.Panel2.ResumeLayout(false);
            this.Panel2.PerformLayout();
            this.TabControl3.ResumeLayout(false);
            this.TabPage3.ResumeLayout(false);
            this.TabPage3.PerformLayout();
            this.MenuStrip3.ResumeLayout(false);
            this.MenuStrip3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator1)).EndInit();
            this.BindingNavigator1.ResumeLayout(false);
            this.BindingNavigator1.PerformLayout();
            this.TabPage4.ResumeLayout(false);
            this.TabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator2)).EndInit();
            this.BindingNavigator2.ResumeLayout(false);
            this.BindingNavigator2.PerformLayout();
            this.TabPage6.ResumeLayout(false);
            this.TabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator3)).EndInit();
            this.BindingNavigator3.ResumeLayout(false);
            this.BindingNavigator3.PerformLayout();
            this.TabPage5.ResumeLayout(false);
            this.TabPage5.PerformLayout();
            this.MenuStrip6.ResumeLayout(false);
            this.MenuStrip6.PerformLayout();
            this.TabPage7.ResumeLayout(false);
            this.TabPage7.PerformLayout();
            this.TabPage2.ResumeLayout(false);
            this.TabPage2.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.Panel6.ResumeLayout(false);
            this.Panel6.PerformLayout();
            this.Panel4.ResumeLayout(false);
            this.Panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.Panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).EndInit();
            this.GroupBox5.ResumeLayout(false);
            this.GroupBox5.PerformLayout();
            this.menuStrip4.ResumeLayout(false);
            this.menuStrip4.PerformLayout();
            this.menuStrip8.ResumeLayout(false);
            this.menuStrip8.PerformLayout();
            this.menuStrip5.ResumeLayout(false);
            this.menuStrip5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator4)).EndInit();
            this.bindingNavigator4.ResumeLayout(false);
            this.bindingNavigator4.PerformLayout();
            this.menuStrip7.ResumeLayout(false);
            this.menuStrip7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator5)).EndInit();
            this.bindingNavigator5.ResumeLayout(false);
            this.bindingNavigator5.PerformLayout();
            this.menuStrip9.ResumeLayout(false);
            this.menuStrip9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator6)).EndInit();
            this.bindingNavigator6.ResumeLayout(false);
            this.bindingNavigator6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        internal System.Windows.Forms.Panel Panel5;
        internal System.Windows.Forms.Label Label31;
        internal System.Windows.Forms.TextBox TextBox25;
        internal System.Windows.Forms.TextBox TextBox26;
        internal System.Windows.Forms.Label Label32;
        internal System.Windows.Forms.Label Label33;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.MenuStrip MenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem ВыберитеПользователяToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ПриёмщикаВагоновToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox1;
        internal System.Windows.Forms.ToolStripMenuItem МастераЦТОToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox3;
        internal System.Windows.Forms.ToolStripMenuItem СлесарейToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem МастераТО1ToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox4;
        internal System.Windows.Forms.ToolStripMenuItem МастераТО2ToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox6;
        internal System.Windows.Forms.ToolStripMenuItem МастераСРToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox5;
        internal System.Windows.Forms.ToolStripMenuItem ОтделенияToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem МалярногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox7;
        internal System.Windows.Forms.ToolStripMenuItem СлесарноКузовногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox8;
        internal System.Windows.Forms.ToolStripMenuItem РедукторногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox9;
        internal System.Windows.Forms.ToolStripMenuItem МеханическогоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox11;
        internal System.Windows.Forms.ToolStripMenuItem КолесотокарногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox12;
        internal System.Windows.Forms.ToolStripMenuItem МастерскиеToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem РадиотехническойToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox13;
        internal System.Windows.Forms.ToolStripMenuItem АккумуляторнойToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox14;
        internal System.Windows.Forms.ToolStripMenuItem УчасткаToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem РемонтаТокоприемниковToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox15;
        internal System.Windows.Forms.ToolStripMenuItem ЭлектроаппаратногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox16;
        internal System.Windows.Forms.ToolStripMenuItem РемонтаДвигателейToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox17;
        internal System.Windows.Forms.ToolStripMenuItem ЦехаToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ЭлектросварочногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox18;
        internal System.Windows.Forms.ToolStripMenuItem ТележечногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox19;
        internal System.Windows.Forms.ToolStripMenuItem АдминистрацииToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox2;
        internal System.Windows.Forms.ToolStripMenuItem ОтчетыToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ПриёмщикаВагоновToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem ПрохождениеЕОToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ВыходToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem2;
        internal System.Windows.Forms.ToolStripMenuItem ВыходToolStripMenuItem2;
        internal System.Windows.Forms.ToolStripMenuItem ЗамечанияToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem СлесарямToolStripMenuItem1;
        internal System.Windows.Forms.GroupBox GroupBox4;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel1;
        internal System.Windows.Forms.ToolStripStatusLabel ssdate;
        internal System.Windows.Forms.ToolStripStatusLabel sstime;
        internal System.Windows.Forms.TabControl TabControl2;
        internal System.Windows.Forms.TabPage TabPage9;
        internal System.Windows.Forms.DataGridView DataGridView1;
        internal System.Windows.Forms.TabPage TabPage10;
        internal System.Windows.Forms.Label Predmet_ystLabel1;
        internal System.Windows.Forms.Label NameLabel1;
        internal System.Windows.Forms.Label Time_2Label1;
        internal System.Windows.Forms.Label Data_2Label1;
        internal System.Windows.Forms.Label Label30;
        internal System.Windows.Forms.Label Label29;
        internal System.Windows.Forms.Label Label28;
        internal System.Windows.Forms.Label Label27;
        internal System.Windows.Forms.DataGridView DataGridView2;
        internal System.Windows.Forms.GroupBox GroupBox3;
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.DateTimePicker DateTimePicker2;
        internal System.Windows.Forms.DateTimePicker DateTimePicker1;
        internal System.Windows.Forms.MenuStrip MenuStrip2;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem4;
        internal System.Windows.Forms.ToolStripMenuItem ЗапросОУстраненииНеисправностейToolStripMenuItem;
        internal System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.TabControl TabControl3;
        internal System.Windows.Forms.TabPage TabPage3;
        internal System.Windows.Forms.MenuStrip MenuStrip3;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem3;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem10;
        internal System.Windows.Forms.BindingNavigator BindingNavigator1;
        internal System.Windows.Forms.ToolStripButton Vagon_spisokBindingNavigatorSaveItem21;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.TextBox TextBox6;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.CheckBox CheckBox4;
        internal System.Windows.Forms.TabPage TabPage4;
        internal System.Windows.Forms.BindingNavigator BindingNavigator2;
        internal System.Windows.Forms.ToolStripButton ToolStripButton1;
        internal System.Windows.Forms.CheckBox CheckBox3;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.TabPage TabPage6;
        internal System.Windows.Forms.BindingNavigator BindingNavigator3;
        internal System.Windows.Forms.ToolStripButton ToolStripButton2;
        internal System.Windows.Forms.CheckBox CheckBox5;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.TabPage TabPage5;
        internal System.Windows.Forms.MenuStrip MenuStrip6;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem12;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem13;
        internal System.Windows.Forms.CheckBox CheckBox6;
        internal System.Windows.Forms.Label Label23;
        internal System.Windows.Forms.TabPage TabPage7;
        internal System.Windows.Forms.CheckBox CheckBox9;
        internal System.Windows.Forms.CheckBox CheckBox8;
        internal System.Windows.Forms.CheckBox CheckBox7;
        internal System.Windows.Forms.TabPage TabPage2;
        internal System.Windows.Forms.CheckBox CheckBox14;
        internal System.Windows.Forms.CheckBox CheckBox13;
        internal System.Windows.Forms.CheckBox CheckBox12;
        internal System.Windows.Forms.CheckBox CheckBox11;
        internal System.Windows.Forms.CheckBox CheckBox2;
        internal System.Windows.Forms.CheckBox CheckBox1;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Panel Panel6;
        internal System.Windows.Forms.TextBox NadbavkaTextBox;
        internal System.Windows.Forms.TextBox Indikator_2TextBox;
        internal System.Windows.Forms.TextBox Indikator_1TextBox;
        internal System.Windows.Forms.TextBox TextBox4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Panel Panel4;
        internal System.Windows.Forms.Label Label36;
        internal System.Windows.Forms.Label Label37;
        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.TextBox TextBox1;
        internal System.Windows.Forms.TextBox TextBox8;
        internal System.Windows.Forms.TextBox TextBox9;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox TextBox2;
        internal System.Windows.Forms.TextBox TextBox3;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.PictureBox PictureBox2;
        internal System.Windows.Forms.GroupBox GroupBox5;
        internal System.Windows.Forms.Label Label35;
        internal System.Windows.Forms.Label Label34;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.Label Label25;
        internal System.Windows.Forms.Label Label24;
        internal System.Windows.Forms.MenuStrip menuStrip4;
        internal System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        internal System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.TextBox textBox5;
        internal System.Windows.Forms.MenuStrip menuStrip8;
        internal System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        internal System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.TextBox textBox7;
        internal System.Windows.Forms.MenuStrip menuStrip5;
        internal System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        internal System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.TextBox textBox10;
        internal System.Windows.Forms.BindingNavigator bindingNavigator4;
        internal System.Windows.Forms.ToolStripButton toolStripButton3;
        internal System.Windows.Forms.CheckBox checkBox15;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.MenuStrip menuStrip7;
        internal System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        internal System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        internal System.Windows.Forms.Label label15;
        internal System.Windows.Forms.TextBox textBox11;
        internal System.Windows.Forms.BindingNavigator bindingNavigator5;
        internal System.Windows.Forms.ToolStripButton toolStripButton4;
        internal System.Windows.Forms.MenuStrip menuStrip9;
        internal System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        internal System.Windows.Forms.ToolStripMenuItem toolStripMenuItem17;
        internal System.Windows.Forms.Label label16;
        internal System.Windows.Forms.TextBox textBox12;
        internal System.Windows.Forms.BindingNavigator bindingNavigator6;
        internal System.Windows.Forms.ToolStripButton toolStripButton5;
    }
}