﻿namespace Terminal_per
{
    partial class Sama_programma_eged_osm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label NadbavkaLabel;
            System.Windows.Forms.Label Indikator_2Label;
            System.Windows.Forms.Label Indikator_1Label;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sama_programma_eged_osm));
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.MenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ВыберитеПользователяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ПриёмщикаВагоновToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.МастераЦТОToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox3 = new System.Windows.Forms.ToolStripTextBox();
            this.СлесарейToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.МастераТО1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox4 = new System.Windows.Forms.ToolStripTextBox();
            this.МастераТО2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox6 = new System.Windows.Forms.ToolStripTextBox();
            this.МастераСРToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox5 = new System.Windows.Forms.ToolStripTextBox();
            this.ОтделенияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.МалярногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox7 = new System.Windows.Forms.ToolStripTextBox();
            this.СлесарноКузовногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox8 = new System.Windows.Forms.ToolStripTextBox();
            this.РедукторногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox9 = new System.Windows.Forms.ToolStripTextBox();
            this.МеханическогоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox11 = new System.Windows.Forms.ToolStripTextBox();
            this.КолесотокарногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox12 = new System.Windows.Forms.ToolStripTextBox();
            this.МастерскиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.РадиотехническойToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox13 = new System.Windows.Forms.ToolStripTextBox();
            this.АккумуляторнойToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox14 = new System.Windows.Forms.ToolStripTextBox();
            this.УчасткаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.РемонтаТокоприемниковToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox15 = new System.Windows.Forms.ToolStripTextBox();
            this.ЭлектроаппаратногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox16 = new System.Windows.Forms.ToolStripTextBox();
            this.РемонтаДвигателейToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox17 = new System.Windows.Forms.ToolStripTextBox();
            this.ЦехаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ЭлектросварочногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox18 = new System.Windows.Forms.ToolStripTextBox();
            this.ТележечногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox19 = new System.Windows.Forms.ToolStripTextBox();
            this.АдминистрацииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.ОтчетыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ПриёмщикаВагоновToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ПрохождениеЕОToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ВыходToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ВыходToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ЗамечанияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.СлесарямToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.GroupBox4 = new System.Windows.Forms.GroupBox();
            this.TabControl2 = new System.Windows.Forms.TabControl();
            this.TabPage9 = new System.Windows.Forms.TabPage();
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            this.TabPage10 = new System.Windows.Forms.TabPage();
            this.Predmet_ystLabel1 = new System.Windows.Forms.Label();
            this.NameLabel1 = new System.Windows.Forms.Label();
            this.Time_2Label1 = new System.Windows.Forms.Label();
            this.Data_2Label1 = new System.Windows.Forms.Label();
            this.Label30 = new System.Windows.Forms.Label();
            this.Label29 = new System.Windows.Forms.Label();
            this.Label28 = new System.Windows.Forms.Label();
            this.Label27 = new System.Windows.Forms.Label();
            this.DataGridView2 = new System.Windows.Forms.DataGridView();
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.MenuStrip2 = new System.Windows.Forms.MenuStrip();
            this.ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.СлесарямToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.CheckBox3 = new System.Windows.Forms.CheckBox();
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.TabPage1 = new System.Windows.Forms.TabPage();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.BindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.Vagon_spisokBindingNavigatorSaveItem21 = new System.Windows.Forms.ToolStripButton();
            this.TabPage2 = new System.Windows.Forms.TabPage();
            this.checkedListBox2 = new System.Windows.Forms.CheckedListBox();
            this.BindingNavigator9 = new System.Windows.Forms.BindingNavigator(this.components);
            this.ToolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigator8 = new System.Windows.Forms.BindingNavigator(this.components);
            this.ToolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigator2 = new System.Windows.Forms.BindingNavigator(this.components);
            this.ToolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.TabPage3 = new System.Windows.Forms.TabPage();
            this.checkedListBox6 = new System.Windows.Forms.CheckedListBox();
            this.BindingNavigator3 = new System.Windows.Forms.BindingNavigator(this.components);
            this.ToolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.TabPage4 = new System.Windows.Forms.TabPage();
            this.checkedListBox3 = new System.Windows.Forms.CheckedListBox();
            this.BindingNavigator4 = new System.Windows.Forms.BindingNavigator(this.components);
            this.ToolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.TabPage5 = new System.Windows.Forms.TabPage();
            this.BindingNavigator5 = new System.Windows.Forms.BindingNavigator(this.components);
            this.ToolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.TextBox15 = new System.Windows.Forms.TextBox();
            this.TextBox16 = new System.Windows.Forms.TextBox();
            this.TextBox17 = new System.Windows.Forms.TextBox();
            this.TextBox18 = new System.Windows.Forms.TextBox();
            this.TextBox14 = new System.Windows.Forms.TextBox();
            this.TextBox13 = new System.Windows.Forms.TextBox();
            this.TextBox12 = new System.Windows.Forms.TextBox();
            this.TextBox11 = new System.Windows.Forms.TextBox();
            this.Label19 = new System.Windows.Forms.Label();
            this.Label20 = new System.Windows.Forms.Label();
            this.Label21 = new System.Windows.Forms.Label();
            this.Label22 = new System.Windows.Forms.Label();
            this.Label23 = new System.Windows.Forms.Label();
            this.Label18 = new System.Windows.Forms.Label();
            this.Label17 = new System.Windows.Forms.Label();
            this.Label16 = new System.Windows.Forms.Label();
            this.Label15 = new System.Windows.Forms.Label();
            this.Label14 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.TabPage6 = new System.Windows.Forms.TabPage();
            this.checkedListBox4 = new System.Windows.Forms.CheckedListBox();
            this.BindingNavigator6 = new System.Windows.Forms.BindingNavigator(this.components);
            this.ToolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.TabPage8 = new System.Windows.Forms.TabPage();
            this.checkedListBox5 = new System.Windows.Forms.CheckedListBox();
            this.BindingNavigator7 = new System.Windows.Forms.BindingNavigator(this.components);
            this.ToolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.CheckBox1 = new System.Windows.Forms.CheckBox();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.Panel6 = new System.Windows.Forms.Panel();
            this.NadbavkaTextBox = new System.Windows.Forms.TextBox();
            this.Indikator_2TextBox = new System.Windows.Forms.TextBox();
            this.Indikator_1TextBox = new System.Windows.Forms.TextBox();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.Label36 = new System.Windows.Forms.Label();
            this.Label37 = new System.Windows.Forms.Label();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.TextBox1 = new System.Windows.Forms.TextBox();
            this.TextBox7 = new System.Windows.Forms.TextBox();
            this.TextBox8 = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.TextBox9 = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.TextBox2 = new System.Windows.Forms.TextBox();
            this.TextBox3 = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.PictureBox2 = new System.Windows.Forms.PictureBox();
            this.GroupBox5 = new System.Windows.Forms.GroupBox();
            this.Label35 = new System.Windows.Forms.Label();
            this.Label34 = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.Label26 = new System.Windows.Forms.Label();
            this.Label25 = new System.Windows.Forms.Label();
            this.Label24 = new System.Windows.Forms.Label();
            this.DataLabel1 = new System.Windows.Forms.Label();
            this.TimeLabel1 = new System.Windows.Forms.Label();
            this.Panel5 = new System.Windows.Forms.Panel();
            this.Label31 = new System.Windows.Forms.Label();
            this.TextBox25 = new System.Windows.Forms.TextBox();
            this.TextBox26 = new System.Windows.Forms.TextBox();
            this.Label32 = new System.Windows.Forms.Label();
            this.Label33 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label40 = new System.Windows.Forms.Label();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ssdate = new System.Windows.Forms.ToolStripStatusLabel();
            this.sstime = new System.Windows.Forms.ToolStripStatusLabel();
            NadbavkaLabel = new System.Windows.Forms.Label();
            Indikator_2Label = new System.Windows.Forms.Label();
            Indikator_1Label = new System.Windows.Forms.Label();
            this.TableLayoutPanel1.SuspendLayout();
            this.Panel1.SuspendLayout();
            this.MenuStrip1.SuspendLayout();
            this.GroupBox4.SuspendLayout();
            this.TabControl2.SuspendLayout();
            this.TabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            this.TabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView2)).BeginInit();
            this.GroupBox3.SuspendLayout();
            this.MenuStrip2.SuspendLayout();
            this.Panel2.SuspendLayout();
            this.TabControl1.SuspendLayout();
            this.TabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator1)).BeginInit();
            this.BindingNavigator1.SuspendLayout();
            this.TabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator9)).BeginInit();
            this.BindingNavigator9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator8)).BeginInit();
            this.BindingNavigator8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator2)).BeginInit();
            this.BindingNavigator2.SuspendLayout();
            this.TabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator3)).BeginInit();
            this.BindingNavigator3.SuspendLayout();
            this.TabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator4)).BeginInit();
            this.BindingNavigator4.SuspendLayout();
            this.TabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator5)).BeginInit();
            this.BindingNavigator5.SuspendLayout();
            this.TabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator6)).BeginInit();
            this.BindingNavigator6.SuspendLayout();
            this.TabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator7)).BeginInit();
            this.BindingNavigator7.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.Panel6.SuspendLayout();
            this.Panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.Panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).BeginInit();
            this.GroupBox5.SuspendLayout();
            this.Panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // NadbavkaLabel
            // 
            NadbavkaLabel.AutoSize = true;
            NadbavkaLabel.Location = new System.Drawing.Point(20, 67);
            NadbavkaLabel.Name = "NadbavkaLabel";
            NadbavkaLabel.Size = new System.Drawing.Size(76, 17);
            NadbavkaLabel.TabIndex = 4;
            NadbavkaLabel.Text = "Nadbavka:";
            // 
            // Indikator_2Label
            // 
            Indikator_2Label.AutoSize = true;
            Indikator_2Label.Location = new System.Drawing.Point(19, 37);
            Indikator_2Label.Name = "Indikator_2Label";
            Indikator_2Label.Size = new System.Drawing.Size(78, 17);
            Indikator_2Label.TabIndex = 2;
            Indikator_2Label.Text = "Indikator 2:";
            // 
            // Indikator_1Label
            // 
            Indikator_1Label.AutoSize = true;
            Indikator_1Label.Location = new System.Drawing.Point(19, 6);
            Indikator_1Label.Name = "Indikator_1Label";
            Indikator_1Label.Size = new System.Drawing.Size(78, 17);
            Indikator_1Label.TabIndex = 0;
            Indikator_1Label.Text = "Indikator 1:";
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.BackColor = System.Drawing.SystemColors.Window;
            this.TableLayoutPanel1.ColumnCount = 3;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.32558F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.65804F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.01637F));
            this.TableLayoutPanel1.Controls.Add(this.Panel1, 1, 0);
            this.TableLayoutPanel1.Controls.Add(this.GroupBox1, 0, 0);
            this.TableLayoutPanel1.Controls.Add(this.Panel3, 2, 0);
            this.TableLayoutPanel1.Controls.Add(this.panel7, 1, 1);
            this.TableLayoutPanel1.Controls.Add(this.Panel5, 0, 1);
            this.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 2;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 83.10011F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.89989F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TableLayoutPanel1.Size = new System.Drawing.Size(1284, 778);
            this.TableLayoutPanel1.TabIndex = 3;
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.SystemColors.Window;
            this.Panel1.Controls.Add(this.groupBox2);
            this.Panel1.Controls.Add(this.MenuStrip1);
            this.Panel1.Controls.Add(this.GroupBox4);
            this.Panel1.Controls.Add(this.GroupBox3);
            this.Panel1.Controls.Add(this.Panel2);
            this.Panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel1.Location = new System.Drawing.Point(315, 3);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(772, 640);
            this.Panel1.TabIndex = 0;
            // 
            // MenuStrip1
            // 
            this.MenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ВыберитеПользователяToolStripMenuItem,
            this.ОтчетыToolStripMenuItem,
            this.ВыходToolStripMenuItem1,
            this.ЗамечанияToolStripMenuItem});
            this.MenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip1.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.MenuStrip1.Name = "MenuStrip1";
            this.MenuStrip1.Size = new System.Drawing.Size(772, 31);
            this.MenuStrip1.TabIndex = 10;
            this.MenuStrip1.Text = "MenuStrip1";
            // 
            // ВыберитеПользователяToolStripMenuItem
            // 
            this.ВыберитеПользователяToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ПриёмщикаВагоновToolStripMenuItem,
            this.МастераЦТОToolStripMenuItem,
            this.СлесарейToolStripMenuItem,
            this.МастераТО1ToolStripMenuItem,
            this.МастераТО2ToolStripMenuItem,
            this.МастераСРToolStripMenuItem,
            this.ОтделенияToolStripMenuItem,
            this.МастерскиеToolStripMenuItem,
            this.УчасткаToolStripMenuItem,
            this.ЦехаToolStripMenuItem,
            this.АдминистрацииToolStripMenuItem});
            this.ВыберитеПользователяToolStripMenuItem.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ВыберитеПользователяToolStripMenuItem.Name = "ВыберитеПользователяToolStripMenuItem";
            this.ВыберитеПользователяToolStripMenuItem.Size = new System.Drawing.Size(260, 27);
            this.ВыберитеПользователяToolStripMenuItem.Text = "В&ыберите учётную запись";
            // 
            // ПриёмщикаВагоновToolStripMenuItem
            // 
            this.ПриёмщикаВагоновToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox1});
            this.ПриёмщикаВагоновToolStripMenuItem.Name = "ПриёмщикаВагоновToolStripMenuItem";
            this.ПриёмщикаВагоновToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.ПриёмщикаВагоновToolStripMenuItem.Text = "Приёмщика вагонов";
            // 
            // ToolStripTextBox1
            // 
            this.ToolStripTextBox1.Name = "ToolStripTextBox1";
            this.ToolStripTextBox1.ReadOnly = true;
            this.ToolStripTextBox1.Size = new System.Drawing.Size(100, 27);
            this.ToolStripTextBox1.Text = "100";
            // 
            // МастераЦТОToolStripMenuItem
            // 
            this.МастераЦТОToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox3});
            this.МастераЦТОToolStripMenuItem.Name = "МастераЦТОToolStripMenuItem";
            this.МастераЦТОToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.МастераЦТОToolStripMenuItem.Text = "Мастера ЕО";
            // 
            // ToolStripTextBox3
            // 
            this.ToolStripTextBox3.Name = "ToolStripTextBox3";
            this.ToolStripTextBox3.Size = new System.Drawing.Size(100, 27);
            // 
            // СлесарейToolStripMenuItem
            // 
            this.СлесарейToolStripMenuItem.Name = "СлесарейToolStripMenuItem";
            this.СлесарейToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.СлесарейToolStripMenuItem.Text = "Слесарей";
            // 
            // МастераТО1ToolStripMenuItem
            // 
            this.МастераТО1ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox4});
            this.МастераТО1ToolStripMenuItem.Name = "МастераТО1ToolStripMenuItem";
            this.МастераТО1ToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.МастераТО1ToolStripMenuItem.Text = "Мастера ТО1";
            // 
            // ToolStripTextBox4
            // 
            this.ToolStripTextBox4.Name = "ToolStripTextBox4";
            this.ToolStripTextBox4.Size = new System.Drawing.Size(100, 27);
            // 
            // МастераТО2ToolStripMenuItem
            // 
            this.МастераТО2ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox6});
            this.МастераТО2ToolStripMenuItem.Name = "МастераТО2ToolStripMenuItem";
            this.МастераТО2ToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.МастераТО2ToolStripMenuItem.Text = "Мастера ТО2";
            // 
            // ToolStripTextBox6
            // 
            this.ToolStripTextBox6.Name = "ToolStripTextBox6";
            this.ToolStripTextBox6.Size = new System.Drawing.Size(100, 27);
            // 
            // МастераСРToolStripMenuItem
            // 
            this.МастераСРToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox5});
            this.МастераСРToolStripMenuItem.Name = "МастераСРToolStripMenuItem";
            this.МастераСРToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.МастераСРToolStripMenuItem.Text = "Мастера СР";
            // 
            // ToolStripTextBox5
            // 
            this.ToolStripTextBox5.Name = "ToolStripTextBox5";
            this.ToolStripTextBox5.Size = new System.Drawing.Size(100, 27);
            // 
            // ОтделенияToolStripMenuItem
            // 
            this.ОтделенияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.МалярногоToolStripMenuItem,
            this.СлесарноКузовногоToolStripMenuItem,
            this.РедукторногоToolStripMenuItem,
            this.МеханическогоToolStripMenuItem,
            this.КолесотокарногоToolStripMenuItem});
            this.ОтделенияToolStripMenuItem.Name = "ОтделенияToolStripMenuItem";
            this.ОтделенияToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.ОтделенияToolStripMenuItem.Text = "Отделения";
            // 
            // МалярногоToolStripMenuItem
            // 
            this.МалярногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox7});
            this.МалярногоToolStripMenuItem.Name = "МалярногоToolStripMenuItem";
            this.МалярногоToolStripMenuItem.Size = new System.Drawing.Size(268, 28);
            this.МалярногоToolStripMenuItem.Text = "Малярного";
            // 
            // ToolStripTextBox7
            // 
            this.ToolStripTextBox7.Name = "ToolStripTextBox7";
            this.ToolStripTextBox7.Size = new System.Drawing.Size(100, 27);
            // 
            // СлесарноКузовногоToolStripMenuItem
            // 
            this.СлесарноКузовногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox8});
            this.СлесарноКузовногоToolStripMenuItem.Name = "СлесарноКузовногоToolStripMenuItem";
            this.СлесарноКузовногоToolStripMenuItem.Size = new System.Drawing.Size(268, 28);
            this.СлесарноКузовногоToolStripMenuItem.Text = "Слесарно-Кузовного";
            // 
            // ToolStripTextBox8
            // 
            this.ToolStripTextBox8.Name = "ToolStripTextBox8";
            this.ToolStripTextBox8.Size = new System.Drawing.Size(100, 27);
            // 
            // РедукторногоToolStripMenuItem
            // 
            this.РедукторногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox9});
            this.РедукторногоToolStripMenuItem.Name = "РедукторногоToolStripMenuItem";
            this.РедукторногоToolStripMenuItem.Size = new System.Drawing.Size(268, 28);
            this.РедукторногоToolStripMenuItem.Text = "Редукторного";
            // 
            // ToolStripTextBox9
            // 
            this.ToolStripTextBox9.Name = "ToolStripTextBox9";
            this.ToolStripTextBox9.Size = new System.Drawing.Size(100, 27);
            // 
            // МеханическогоToolStripMenuItem
            // 
            this.МеханическогоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox11});
            this.МеханическогоToolStripMenuItem.Name = "МеханическогоToolStripMenuItem";
            this.МеханическогоToolStripMenuItem.Size = new System.Drawing.Size(268, 28);
            this.МеханическогоToolStripMenuItem.Text = "Механического";
            // 
            // ToolStripTextBox11
            // 
            this.ToolStripTextBox11.Name = "ToolStripTextBox11";
            this.ToolStripTextBox11.Size = new System.Drawing.Size(100, 27);
            // 
            // КолесотокарногоToolStripMenuItem
            // 
            this.КолесотокарногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox12});
            this.КолесотокарногоToolStripMenuItem.Name = "КолесотокарногоToolStripMenuItem";
            this.КолесотокарногоToolStripMenuItem.Size = new System.Drawing.Size(268, 28);
            this.КолесотокарногоToolStripMenuItem.Text = "Колесотокарного";
            // 
            // ToolStripTextBox12
            // 
            this.ToolStripTextBox12.Name = "ToolStripTextBox12";
            this.ToolStripTextBox12.Size = new System.Drawing.Size(100, 27);
            // 
            // МастерскиеToolStripMenuItem
            // 
            this.МастерскиеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.РадиотехническойToolStripMenuItem,
            this.АккумуляторнойToolStripMenuItem});
            this.МастерскиеToolStripMenuItem.Name = "МастерскиеToolStripMenuItem";
            this.МастерскиеToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.МастерскиеToolStripMenuItem.Text = "Мастерской";
            // 
            // РадиотехническойToolStripMenuItem
            // 
            this.РадиотехническойToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox13});
            this.РадиотехническойToolStripMenuItem.Name = "РадиотехническойToolStripMenuItem";
            this.РадиотехническойToolStripMenuItem.Size = new System.Drawing.Size(246, 28);
            this.РадиотехническойToolStripMenuItem.Text = "Радиотехнической";
            // 
            // ToolStripTextBox13
            // 
            this.ToolStripTextBox13.Name = "ToolStripTextBox13";
            this.ToolStripTextBox13.Size = new System.Drawing.Size(100, 27);
            // 
            // АккумуляторнойToolStripMenuItem
            // 
            this.АккумуляторнойToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox14});
            this.АккумуляторнойToolStripMenuItem.Name = "АккумуляторнойToolStripMenuItem";
            this.АккумуляторнойToolStripMenuItem.Size = new System.Drawing.Size(246, 28);
            this.АккумуляторнойToolStripMenuItem.Text = "Аккумуляторной";
            // 
            // ToolStripTextBox14
            // 
            this.ToolStripTextBox14.Name = "ToolStripTextBox14";
            this.ToolStripTextBox14.Size = new System.Drawing.Size(100, 27);
            // 
            // УчасткаToolStripMenuItem
            // 
            this.УчасткаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.РемонтаТокоприемниковToolStripMenuItem,
            this.ЭлектроаппаратногоToolStripMenuItem,
            this.РемонтаДвигателейToolStripMenuItem});
            this.УчасткаToolStripMenuItem.Name = "УчасткаToolStripMenuItem";
            this.УчасткаToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.УчасткаToolStripMenuItem.Text = "Участка";
            // 
            // РемонтаТокоприемниковToolStripMenuItem
            // 
            this.РемонтаТокоприемниковToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox15});
            this.РемонтаТокоприемниковToolStripMenuItem.Name = "РемонтаТокоприемниковToolStripMenuItem";
            this.РемонтаТокоприемниковToolStripMenuItem.Size = new System.Drawing.Size(309, 28);
            this.РемонтаТокоприемниковToolStripMenuItem.Text = "Ремонта токоприемников";
            // 
            // ToolStripTextBox15
            // 
            this.ToolStripTextBox15.Name = "ToolStripTextBox15";
            this.ToolStripTextBox15.Size = new System.Drawing.Size(100, 27);
            // 
            // ЭлектроаппаратногоToolStripMenuItem
            // 
            this.ЭлектроаппаратногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox16});
            this.ЭлектроаппаратногоToolStripMenuItem.Name = "ЭлектроаппаратногоToolStripMenuItem";
            this.ЭлектроаппаратногоToolStripMenuItem.Size = new System.Drawing.Size(309, 28);
            this.ЭлектроаппаратногоToolStripMenuItem.Text = "Электроаппаратного";
            // 
            // ToolStripTextBox16
            // 
            this.ToolStripTextBox16.Name = "ToolStripTextBox16";
            this.ToolStripTextBox16.Size = new System.Drawing.Size(100, 27);
            // 
            // РемонтаДвигателейToolStripMenuItem
            // 
            this.РемонтаДвигателейToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox17});
            this.РемонтаДвигателейToolStripMenuItem.Name = "РемонтаДвигателейToolStripMenuItem";
            this.РемонтаДвигателейToolStripMenuItem.Size = new System.Drawing.Size(309, 28);
            this.РемонтаДвигателейToolStripMenuItem.Text = "Ремонта двигателей";
            // 
            // ToolStripTextBox17
            // 
            this.ToolStripTextBox17.Name = "ToolStripTextBox17";
            this.ToolStripTextBox17.Size = new System.Drawing.Size(100, 27);
            // 
            // ЦехаToolStripMenuItem
            // 
            this.ЦехаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ЭлектросварочногоToolStripMenuItem,
            this.ТележечногоToolStripMenuItem});
            this.ЦехаToolStripMenuItem.Name = "ЦехаToolStripMenuItem";
            this.ЦехаToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.ЦехаToolStripMenuItem.Text = "Цеха";
            // 
            // ЭлектросварочногоToolStripMenuItem
            // 
            this.ЭлектросварочногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox18});
            this.ЭлектросварочногоToolStripMenuItem.Name = "ЭлектросварочногоToolStripMenuItem";
            this.ЭлектросварочногоToolStripMenuItem.Size = new System.Drawing.Size(258, 28);
            this.ЭлектросварочногоToolStripMenuItem.Text = "Электросварочного";
            // 
            // ToolStripTextBox18
            // 
            this.ToolStripTextBox18.Name = "ToolStripTextBox18";
            this.ToolStripTextBox18.Size = new System.Drawing.Size(100, 27);
            // 
            // ТележечногоToolStripMenuItem
            // 
            this.ТележечногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox19});
            this.ТележечногоToolStripMenuItem.Name = "ТележечногоToolStripMenuItem";
            this.ТележечногоToolStripMenuItem.Size = new System.Drawing.Size(258, 28);
            this.ТележечногоToolStripMenuItem.Text = "Тележечного";
            // 
            // ToolStripTextBox19
            // 
            this.ToolStripTextBox19.Name = "ToolStripTextBox19";
            this.ToolStripTextBox19.Size = new System.Drawing.Size(100, 27);
            // 
            // АдминистрацииToolStripMenuItem
            // 
            this.АдминистрацииToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox2});
            this.АдминистрацииToolStripMenuItem.Name = "АдминистрацииToolStripMenuItem";
            this.АдминистрацииToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.АдминистрацииToolStripMenuItem.Text = "Администрации";
            // 
            // ToolStripTextBox2
            // 
            this.ToolStripTextBox2.Name = "ToolStripTextBox2";
            this.ToolStripTextBox2.ReadOnly = true;
            this.ToolStripTextBox2.Size = new System.Drawing.Size(100, 27);
            this.ToolStripTextBox2.Text = "100";
            // 
            // ОтчетыToolStripMenuItem
            // 
            this.ОтчетыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ПриёмщикаВагоновToolStripMenuItem1,
            this.ПрохождениеЕОToolStripMenuItem});
            this.ОтчетыToolStripMenuItem.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ОтчетыToolStripMenuItem.Name = "ОтчетыToolStripMenuItem";
            this.ОтчетыToolStripMenuItem.Size = new System.Drawing.Size(90, 27);
            this.ОтчетыToolStripMenuItem.Text = "От&четы";
            this.ОтчетыToolStripMenuItem.ToolTipText = "Пока не все работает))Требуется для работы Excel 2003";
            // 
            // ПриёмщикаВагоновToolStripMenuItem1
            // 
            this.ПриёмщикаВагоновToolStripMenuItem1.Name = "ПриёмщикаВагоновToolStripMenuItem1";
            this.ПриёмщикаВагоновToolStripMenuItem1.Size = new System.Drawing.Size(262, 28);
            this.ПриёмщикаВагоновToolStripMenuItem1.Text = "Приёмщика вагонов";
            // 
            // ПрохождениеЕОToolStripMenuItem
            // 
            this.ПрохождениеЕОToolStripMenuItem.Name = "ПрохождениеЕОToolStripMenuItem";
            this.ПрохождениеЕОToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.ПрохождениеЕОToolStripMenuItem.Text = "Прохождение ЕО";
            // 
            // ВыходToolStripMenuItem1
            // 
            this.ВыходToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem2,
            this.ВыходToolStripMenuItem2});
            this.ВыходToolStripMenuItem1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ВыходToolStripMenuItem1.Name = "ВыходToolStripMenuItem1";
            this.ВыходToolStripMenuItem1.Size = new System.Drawing.Size(81, 27);
            this.ВыходToolStripMenuItem1.Text = "В&ыход";
            // 
            // ToolStripMenuItem2
            // 
            this.ToolStripMenuItem2.Name = "ToolStripMenuItem2";
            this.ToolStripMenuItem2.Size = new System.Drawing.Size(247, 28);
            this.ToolStripMenuItem2.Text = "Окончание сеанса";
            // 
            // ВыходToolStripMenuItem2
            // 
            this.ВыходToolStripMenuItem2.Name = "ВыходToolStripMenuItem2";
            this.ВыходToolStripMenuItem2.Size = new System.Drawing.Size(247, 28);
            this.ВыходToolStripMenuItem2.Text = "Выход";
            // 
            // ЗамечанияToolStripMenuItem
            // 
            this.ЗамечанияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.СлесарямToolStripMenuItem1});
            this.ЗамечанияToolStripMenuItem.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ЗамечанияToolStripMenuItem.Name = "ЗамечанияToolStripMenuItem";
            this.ЗамечанияToolStripMenuItem.Size = new System.Drawing.Size(120, 27);
            this.ЗамечанияToolStripMenuItem.Text = "Замечание";
            // 
            // СлесарямToolStripMenuItem1
            // 
            this.СлесарямToolStripMenuItem1.Name = "СлесарямToolStripMenuItem1";
            this.СлесарямToolStripMenuItem1.Size = new System.Drawing.Size(172, 28);
            this.СлесарямToolStripMenuItem1.Text = "Слесарям";
            // 
            // GroupBox4
            // 
            this.GroupBox4.Controls.Add(this.StatusStrip1);
            this.GroupBox4.Controls.Add(this.TabControl2);
            this.GroupBox4.Location = new System.Drawing.Point(21, 29);
            this.GroupBox4.Name = "GroupBox4";
            this.GroupBox4.Size = new System.Drawing.Size(728, 203);
            this.GroupBox4.TabIndex = 9;
            this.GroupBox4.TabStop = false;
            // 
            // TabControl2
            // 
            this.TabControl2.Controls.Add(this.TabPage9);
            this.TabControl2.Controls.Add(this.TabPage10);
            this.TabControl2.Location = new System.Drawing.Point(0, 9);
            this.TabControl2.Name = "TabControl2";
            this.TabControl2.SelectedIndex = 0;
            this.TabControl2.Size = new System.Drawing.Size(728, 194);
            this.TabControl2.TabIndex = 0;
            // 
            // TabPage9
            // 
            this.TabPage9.Controls.Add(this.DataGridView1);
            this.TabPage9.Location = new System.Drawing.Point(4, 25);
            this.TabPage9.Name = "TabPage9";
            this.TabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage9.Size = new System.Drawing.Size(720, 165);
            this.TabPage9.TabIndex = 0;
            this.TabPage9.Text = "Неисправность";
            this.TabPage9.UseVisualStyleBackColor = true;
            // 
            // DataGridView1
            // 
            this.DataGridView1.AllowUserToAddRows = false;
            this.DataGridView1.AllowUserToDeleteRows = false;
            this.DataGridView1.AllowUserToOrderColumns = true;
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Location = new System.Drawing.Point(6, 7);
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.ReadOnly = true;
            this.DataGridView1.Size = new System.Drawing.Size(708, 150);
            this.DataGridView1.TabIndex = 3;
            // 
            // TabPage10
            // 
            this.TabPage10.AutoScroll = true;
            this.TabPage10.Controls.Add(this.Predmet_ystLabel1);
            this.TabPage10.Controls.Add(this.NameLabel1);
            this.TabPage10.Controls.Add(this.Time_2Label1);
            this.TabPage10.Controls.Add(this.Data_2Label1);
            this.TabPage10.Controls.Add(this.Label30);
            this.TabPage10.Controls.Add(this.Label29);
            this.TabPage10.Controls.Add(this.Label28);
            this.TabPage10.Controls.Add(this.Label27);
            this.TabPage10.Controls.Add(this.DataGridView2);
            this.TabPage10.Location = new System.Drawing.Point(4, 25);
            this.TabPage10.Name = "TabPage10";
            this.TabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage10.Size = new System.Drawing.Size(720, 165);
            this.TabPage10.TabIndex = 1;
            this.TabPage10.Text = "Проверка исполнения";
            this.TabPage10.UseVisualStyleBackColor = true;
            // 
            // Predmet_ystLabel1
            // 
            this.Predmet_ystLabel1.Location = new System.Drawing.Point(544, 9);
            this.Predmet_ystLabel1.Name = "Predmet_ystLabel1";
            this.Predmet_ystLabel1.Size = new System.Drawing.Size(100, 23);
            this.Predmet_ystLabel1.TabIndex = 13;
            // 
            // NameLabel1
            // 
            this.NameLabel1.Location = new System.Drawing.Point(544, 40);
            this.NameLabel1.Name = "NameLabel1";
            this.NameLabel1.Size = new System.Drawing.Size(100, 23);
            this.NameLabel1.TabIndex = 15;
            // 
            // Time_2Label1
            // 
            this.Time_2Label1.Location = new System.Drawing.Point(544, 73);
            this.Time_2Label1.Name = "Time_2Label1";
            this.Time_2Label1.Size = new System.Drawing.Size(100, 23);
            this.Time_2Label1.TabIndex = 17;
            // 
            // Data_2Label1
            // 
            this.Data_2Label1.Location = new System.Drawing.Point(544, 103);
            this.Data_2Label1.Name = "Data_2Label1";
            this.Data_2Label1.Size = new System.Drawing.Size(100, 23);
            this.Data_2Label1.TabIndex = 19;
            // 
            // Label30
            // 
            this.Label30.AutoSize = true;
            this.Label30.Location = new System.Drawing.Point(380, 9);
            this.Label30.Name = "Label30";
            this.Label30.Size = new System.Drawing.Size(148, 17);
            this.Label30.TabIndex = 8;
            this.Label30.Text = "Что было  устранено";
            // 
            // Label29
            // 
            this.Label29.AutoSize = true;
            this.Label29.Location = new System.Drawing.Point(380, 78);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(135, 17);
            this.Label29.TabIndex = 7;
            this.Label29.Text = "Время  устранения";
            // 
            // Label28
            // 
            this.Label28.AutoSize = true;
            this.Label28.Location = new System.Drawing.Point(380, 108);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(123, 17);
            this.Label28.TabIndex = 6;
            this.Label28.Text = "Дата устранения";
            // 
            // Label27
            // 
            this.Label27.AutoSize = true;
            this.Label27.Location = new System.Drawing.Point(380, 45);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(107, 17);
            this.Label27.TabIndex = 5;
            this.Label27.Text = "Кем устранено";
            // 
            // DataGridView2
            // 
            this.DataGridView2.AllowUserToAddRows = false;
            this.DataGridView2.AllowUserToDeleteRows = false;
            this.DataGridView2.AllowUserToOrderColumns = true;
            this.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView2.Location = new System.Drawing.Point(9, 3);
            this.DataGridView2.Name = "DataGridView2";
            this.DataGridView2.ReadOnly = true;
            this.DataGridView2.Size = new System.Drawing.Size(356, 150);
            this.DataGridView2.TabIndex = 4;
            // 
            // GroupBox3
            // 
            this.GroupBox3.Controls.Add(this.Label10);
            this.GroupBox3.Controls.Add(this.Label7);
            this.GroupBox3.Controls.Add(this.MenuStrip2);
            this.GroupBox3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GroupBox3.Location = new System.Drawing.Point(21, 487);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(310, 73);
            this.GroupBox3.TabIndex = 8;
            this.GroupBox3.TabStop = false;
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(390, 33);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(0, 22);
            this.Label10.TabIndex = 10;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(279, 33);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(0, 22);
            this.Label7.TabIndex = 9;
            // 
            // MenuStrip2
            // 
            this.MenuStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.MenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem1});
            this.MenuStrip2.Location = new System.Drawing.Point(13, 25);
            this.MenuStrip2.Name = "MenuStrip2";
            this.MenuStrip2.Size = new System.Drawing.Size(276, 30);
            this.MenuStrip2.TabIndex = 3;
            this.MenuStrip2.Text = "MenuStrip2";
            // 
            // ToolStripMenuItem1
            // 
            this.ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.СлесарямToolStripMenuItem});
            this.ToolStripMenuItem1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ToolStripMenuItem1.Name = "ToolStripMenuItem1";
            this.ToolStripMenuItem1.Size = new System.Drawing.Size(268, 26);
            this.ToolStripMenuItem1.Text = "Отправка замечаний/данных";
            // 
            // СлесарямToolStripMenuItem
            // 
            this.СлесарямToolStripMenuItem.Name = "СлесарямToolStripMenuItem";
            this.СлесарямToolStripMenuItem.Size = new System.Drawing.Size(162, 26);
            this.СлесарямToolStripMenuItem.Text = "Слесарям";
            // 
            // Panel2
            // 
            this.Panel2.Controls.Add(this.CheckBox3);
            this.Panel2.Controls.Add(this.TabControl1);
            this.Panel2.Controls.Add(this.CheckBox1);
            this.Panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.Panel2.Location = new System.Drawing.Point(21, 238);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(731, 232);
            this.Panel2.TabIndex = 5;
            // 
            // CheckBox3
            // 
            this.CheckBox3.AutoSize = true;
            this.CheckBox3.Enabled = false;
            this.CheckBox3.Location = new System.Drawing.Point(129, 3);
            this.CheckBox3.Name = "CheckBox3";
            this.CheckBox3.Size = new System.Drawing.Size(215, 21);
            this.CheckBox3.TabIndex = 2;
            this.CheckBox3.Text = "Не проверено\\не требуется";
            this.CheckBox3.UseVisualStyleBackColor = true;
            // 
            // TabControl1
            // 
            this.TabControl1.Controls.Add(this.TabPage1);
            this.TabControl1.Controls.Add(this.TabPage2);
            this.TabControl1.Controls.Add(this.TabPage3);
            this.TabControl1.Controls.Add(this.TabPage4);
            this.TabControl1.Controls.Add(this.TabPage5);
            this.TabControl1.Controls.Add(this.TabPage6);
            this.TabControl1.Controls.Add(this.TabPage8);
            this.TabControl1.Location = new System.Drawing.Point(3, 29);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(725, 200);
            this.TabControl1.TabIndex = 0;
            // 
            // TabPage1
            // 
            this.TabPage1.Controls.Add(this.checkedListBox1);
            this.TabPage1.Controls.Add(this.Label5);
            this.TabPage1.Controls.Add(this.BindingNavigator1);
            this.TabPage1.Location = new System.Drawing.Point(4, 25);
            this.TabPage1.Name = "TabPage1";
            this.TabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage1.Size = new System.Drawing.Size(717, 171);
            this.TabPage1.TabIndex = 0;
            this.TabPage1.Text = "Экипировка вагона";
            this.TabPage1.UseVisualStyleBackColor = true;
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.BackColor = System.Drawing.SystemColors.Window;
            this.checkedListBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.checkedListBox1.CheckOnClick = true;
            this.checkedListBox1.ColumnWidth = 300;
            this.checkedListBox1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkedListBox1.Items.AddRange(new object[] {
            "Медицинская аптечка",
            "Знак аварийной остановки",
            "Противооткатные башмаки",
            "Огнетушитель 5 л  - 2 шт",
            "Ломик для перевода стрелок",
            "Дополнительная сцепка",
            "2 штыря",
            "Информация для пассажиров",
            "Турникет"});
            this.checkedListBox1.Location = new System.Drawing.Point(16, 10);
            this.checkedListBox1.MultiColumn = true;
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkedListBox1.Size = new System.Drawing.Size(683, 125);
            this.checkedListBox1.TabIndex = 14;
            this.checkedListBox1.UseTabStops = false;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(13, 143);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(51, 17);
            this.Label5.TabIndex = 13;
            this.Label5.Text = "Label5";
            // 
            // BindingNavigator1
            // 
            this.BindingNavigator1.AddNewItem = null;
            this.BindingNavigator1.CountItem = null;
            this.BindingNavigator1.DeleteItem = null;
            this.BindingNavigator1.Dock = System.Windows.Forms.DockStyle.None;
            this.BindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Vagon_spisokBindingNavigatorSaveItem21});
            this.BindingNavigator1.Location = new System.Drawing.Point(678, 141);
            this.BindingNavigator1.MoveFirstItem = null;
            this.BindingNavigator1.MoveLastItem = null;
            this.BindingNavigator1.MoveNextItem = null;
            this.BindingNavigator1.MovePreviousItem = null;
            this.BindingNavigator1.Name = "BindingNavigator1";
            this.BindingNavigator1.PositionItem = null;
            this.BindingNavigator1.Size = new System.Drawing.Size(35, 25);
            this.BindingNavigator1.TabIndex = 12;
            this.BindingNavigator1.Text = "BindingNavigator1";
            // 
            // Vagon_spisokBindingNavigatorSaveItem21
            // 
            this.Vagon_spisokBindingNavigatorSaveItem21.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Vagon_spisokBindingNavigatorSaveItem21.Image = ((System.Drawing.Image)(resources.GetObject("Vagon_spisokBindingNavigatorSaveItem21.Image")));
            this.Vagon_spisokBindingNavigatorSaveItem21.Name = "Vagon_spisokBindingNavigatorSaveItem21";
            this.Vagon_spisokBindingNavigatorSaveItem21.Size = new System.Drawing.Size(23, 22);
            this.Vagon_spisokBindingNavigatorSaveItem21.Text = "Сохранить данные";
            this.Vagon_spisokBindingNavigatorSaveItem21.Click += new System.EventHandler(this.Vagon_spisokBindingNavigatorSaveItem21_Click);
            // 
            // TabPage2
            // 
            this.TabPage2.Controls.Add(this.checkedListBox2);
            this.TabPage2.Controls.Add(this.BindingNavigator9);
            this.TabPage2.Controls.Add(this.BindingNavigator8);
            this.TabPage2.Controls.Add(this.BindingNavigator2);
            this.TabPage2.Location = new System.Drawing.Point(4, 25);
            this.TabPage2.Name = "TabPage2";
            this.TabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage2.Size = new System.Drawing.Size(717, 171);
            this.TabPage2.TabIndex = 1;
            this.TabPage2.Text = "СГО";
            this.TabPage2.UseVisualStyleBackColor = true;
            // 
            // checkedListBox2
            // 
            this.checkedListBox2.BackColor = System.Drawing.SystemColors.Window;
            this.checkedListBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.checkedListBox2.CheckOnClick = true;
            this.checkedListBox2.ColumnWidth = 300;
            this.checkedListBox2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkedListBox2.Items.AddRange(new object[] {
            "Уборка салона",
            "Мойка кузова",
            "Мойка салона"});
            this.checkedListBox2.Location = new System.Drawing.Point(16, 10);
            this.checkedListBox2.MultiColumn = true;
            this.checkedListBox2.Name = "checkedListBox2";
            this.checkedListBox2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkedListBox2.Size = new System.Drawing.Size(683, 125);
            this.checkedListBox2.TabIndex = 15;
            this.checkedListBox2.UseTabStops = false;
            // 
            // BindingNavigator9
            // 
            this.BindingNavigator9.AddNewItem = null;
            this.BindingNavigator9.CountItem = null;
            this.BindingNavigator9.DeleteItem = null;
            this.BindingNavigator9.Dock = System.Windows.Forms.DockStyle.None;
            this.BindingNavigator9.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButton8});
            this.BindingNavigator9.Location = new System.Drawing.Point(678, 141);
            this.BindingNavigator9.MoveFirstItem = null;
            this.BindingNavigator9.MoveLastItem = null;
            this.BindingNavigator9.MoveNextItem = null;
            this.BindingNavigator9.MovePreviousItem = null;
            this.BindingNavigator9.Name = "BindingNavigator9";
            this.BindingNavigator9.PositionItem = null;
            this.BindingNavigator9.Size = new System.Drawing.Size(35, 25);
            this.BindingNavigator9.TabIndex = 14;
            this.BindingNavigator9.Text = "BindingNavigator9";
            // 
            // ToolStripButton8
            // 
            this.ToolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripButton8.Image")));
            this.ToolStripButton8.Name = "ToolStripButton8";
            this.ToolStripButton8.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButton8.Text = "Сохранить данные";
            this.ToolStripButton8.Click += new System.EventHandler(this.ToolStripButton8_Click);
            // 
            // BindingNavigator8
            // 
            this.BindingNavigator8.AddNewItem = null;
            this.BindingNavigator8.CountItem = null;
            this.BindingNavigator8.DeleteItem = null;
            this.BindingNavigator8.Dock = System.Windows.Forms.DockStyle.None;
            this.BindingNavigator8.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButton7});
            this.BindingNavigator8.Location = new System.Drawing.Point(681, 141);
            this.BindingNavigator8.MoveFirstItem = null;
            this.BindingNavigator8.MoveLastItem = null;
            this.BindingNavigator8.MoveNextItem = null;
            this.BindingNavigator8.MovePreviousItem = null;
            this.BindingNavigator8.Name = "BindingNavigator8";
            this.BindingNavigator8.PositionItem = null;
            this.BindingNavigator8.Size = new System.Drawing.Size(35, 25);
            this.BindingNavigator8.TabIndex = 13;
            this.BindingNavigator8.Text = "BindingNavigator8";
            this.BindingNavigator8.Visible = false;
            // 
            // ToolStripButton7
            // 
            this.ToolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripButton7.Image")));
            this.ToolStripButton7.Name = "ToolStripButton7";
            this.ToolStripButton7.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButton7.Text = "Сохранить данные";
            // 
            // BindingNavigator2
            // 
            this.BindingNavigator2.AddNewItem = null;
            this.BindingNavigator2.CountItem = null;
            this.BindingNavigator2.DeleteItem = null;
            this.BindingNavigator2.Dock = System.Windows.Forms.DockStyle.None;
            this.BindingNavigator2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButton1});
            this.BindingNavigator2.Location = new System.Drawing.Point(678, 141);
            this.BindingNavigator2.MoveFirstItem = null;
            this.BindingNavigator2.MoveLastItem = null;
            this.BindingNavigator2.MoveNextItem = null;
            this.BindingNavigator2.MovePreviousItem = null;
            this.BindingNavigator2.Name = "BindingNavigator2";
            this.BindingNavigator2.PositionItem = null;
            this.BindingNavigator2.Size = new System.Drawing.Size(35, 25);
            this.BindingNavigator2.TabIndex = 12;
            this.BindingNavigator2.Text = "BindingNavigator2";
            this.BindingNavigator2.Visible = false;
            // 
            // ToolStripButton1
            // 
            this.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripButton1.Image")));
            this.ToolStripButton1.Name = "ToolStripButton1";
            this.ToolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButton1.Text = "Сохранить данные";
            // 
            // TabPage3
            // 
            this.TabPage3.Controls.Add(this.checkedListBox6);
            this.TabPage3.Controls.Add(this.BindingNavigator3);
            this.TabPage3.Location = new System.Drawing.Point(4, 25);
            this.TabPage3.Name = "TabPage3";
            this.TabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage3.Size = new System.Drawing.Size(717, 171);
            this.TabPage3.TabIndex = 2;
            this.TabPage3.Text = "Оборудование на крыше";
            this.TabPage3.UseVisualStyleBackColor = true;
            // 
            // checkedListBox6
            // 
            this.checkedListBox6.BackColor = System.Drawing.SystemColors.Window;
            this.checkedListBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.checkedListBox6.CheckOnClick = true;
            this.checkedListBox6.ColumnWidth = 300;
            this.checkedListBox6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkedListBox6.Items.AddRange(new object[] {
            "Состояние пантографа"});
            this.checkedListBox6.Location = new System.Drawing.Point(16, 13);
            this.checkedListBox6.MultiColumn = true;
            this.checkedListBox6.Name = "checkedListBox6";
            this.checkedListBox6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkedListBox6.Size = new System.Drawing.Size(683, 125);
            this.checkedListBox6.TabIndex = 17;
            this.checkedListBox6.UseTabStops = false;
            // 
            // BindingNavigator3
            // 
            this.BindingNavigator3.AddNewItem = null;
            this.BindingNavigator3.CountItem = null;
            this.BindingNavigator3.DeleteItem = null;
            this.BindingNavigator3.Dock = System.Windows.Forms.DockStyle.None;
            this.BindingNavigator3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButton2});
            this.BindingNavigator3.Location = new System.Drawing.Point(678, 141);
            this.BindingNavigator3.MoveFirstItem = null;
            this.BindingNavigator3.MoveLastItem = null;
            this.BindingNavigator3.MoveNextItem = null;
            this.BindingNavigator3.MovePreviousItem = null;
            this.BindingNavigator3.Name = "BindingNavigator3";
            this.BindingNavigator3.PositionItem = null;
            this.BindingNavigator3.Size = new System.Drawing.Size(35, 25);
            this.BindingNavigator3.TabIndex = 12;
            this.BindingNavigator3.Text = "BindingNavigator3";
            // 
            // ToolStripButton2
            // 
            this.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripButton2.Image")));
            this.ToolStripButton2.Name = "ToolStripButton2";
            this.ToolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButton2.Text = "Сохранить данные";
            this.ToolStripButton2.Click += new System.EventHandler(this.ToolStripButton2_Click);
            // 
            // TabPage4
            // 
            this.TabPage4.Controls.Add(this.checkedListBox3);
            this.TabPage4.Controls.Add(this.BindingNavigator4);
            this.TabPage4.Location = new System.Drawing.Point(4, 25);
            this.TabPage4.Name = "TabPage4";
            this.TabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage4.Size = new System.Drawing.Size(717, 171);
            this.TabPage4.TabIndex = 3;
            this.TabPage4.Text = "ВСС";
            this.TabPage4.UseVisualStyleBackColor = true;
            // 
            // checkedListBox3
            // 
            this.checkedListBox3.BackColor = System.Drawing.SystemColors.Window;
            this.checkedListBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.checkedListBox3.CheckOnClick = true;
            this.checkedListBox3.ColumnWidth = 300;
            this.checkedListBox3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkedListBox3.Items.AddRange(new object[] {
            "Габаритные огни",
            "Огни стоп-сигнала",
            "Фары",
            "Указатели поворота"});
            this.checkedListBox3.Location = new System.Drawing.Point(16, 10);
            this.checkedListBox3.MultiColumn = true;
            this.checkedListBox3.Name = "checkedListBox3";
            this.checkedListBox3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkedListBox3.Size = new System.Drawing.Size(683, 125);
            this.checkedListBox3.TabIndex = 15;
            this.checkedListBox3.UseTabStops = false;
            // 
            // BindingNavigator4
            // 
            this.BindingNavigator4.AddNewItem = null;
            this.BindingNavigator4.CountItem = null;
            this.BindingNavigator4.DeleteItem = null;
            this.BindingNavigator4.Dock = System.Windows.Forms.DockStyle.None;
            this.BindingNavigator4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButton3});
            this.BindingNavigator4.Location = new System.Drawing.Point(678, 144);
            this.BindingNavigator4.MoveFirstItem = null;
            this.BindingNavigator4.MoveLastItem = null;
            this.BindingNavigator4.MoveNextItem = null;
            this.BindingNavigator4.MovePreviousItem = null;
            this.BindingNavigator4.Name = "BindingNavigator4";
            this.BindingNavigator4.PositionItem = null;
            this.BindingNavigator4.Size = new System.Drawing.Size(35, 25);
            this.BindingNavigator4.TabIndex = 12;
            this.BindingNavigator4.Text = "BindingNavigator4";
            // 
            // ToolStripButton3
            // 
            this.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripButton3.Image")));
            this.ToolStripButton3.Name = "ToolStripButton3";
            this.ToolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButton3.Text = "Сохранить данные";
            this.ToolStripButton3.Click += new System.EventHandler(this.ToolStripButton3_Click);
            // 
            // TabPage5
            // 
            this.TabPage5.Controls.Add(this.BindingNavigator5);
            this.TabPage5.Controls.Add(this.TextBox15);
            this.TabPage5.Controls.Add(this.TextBox16);
            this.TabPage5.Controls.Add(this.TextBox17);
            this.TabPage5.Controls.Add(this.TextBox18);
            this.TabPage5.Controls.Add(this.TextBox14);
            this.TabPage5.Controls.Add(this.TextBox13);
            this.TabPage5.Controls.Add(this.TextBox12);
            this.TabPage5.Controls.Add(this.TextBox11);
            this.TabPage5.Controls.Add(this.Label19);
            this.TabPage5.Controls.Add(this.Label20);
            this.TabPage5.Controls.Add(this.Label21);
            this.TabPage5.Controls.Add(this.Label22);
            this.TabPage5.Controls.Add(this.Label23);
            this.TabPage5.Controls.Add(this.Label18);
            this.TabPage5.Controls.Add(this.Label17);
            this.TabPage5.Controls.Add(this.Label16);
            this.TabPage5.Controls.Add(this.Label15);
            this.TabPage5.Controls.Add(this.Label14);
            this.TabPage5.Controls.Add(this.Label13);
            this.TabPage5.Controls.Add(this.Label4);
            this.TabPage5.Controls.Add(this.Label3);
            this.TabPage5.Location = new System.Drawing.Point(4, 25);
            this.TabPage5.Name = "TabPage5";
            this.TabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage5.Size = new System.Drawing.Size(717, 171);
            this.TabPage5.TabIndex = 4;
            this.TabPage5.Text = "ХЧТ";
            this.TabPage5.UseVisualStyleBackColor = true;
            // 
            // BindingNavigator5
            // 
            this.BindingNavigator5.AddNewItem = null;
            this.BindingNavigator5.CountItem = null;
            this.BindingNavigator5.DeleteItem = null;
            this.BindingNavigator5.Dock = System.Windows.Forms.DockStyle.None;
            this.BindingNavigator5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButton4});
            this.BindingNavigator5.Location = new System.Drawing.Point(678, 141);
            this.BindingNavigator5.MoveFirstItem = null;
            this.BindingNavigator5.MoveLastItem = null;
            this.BindingNavigator5.MoveNextItem = null;
            this.BindingNavigator5.MovePreviousItem = null;
            this.BindingNavigator5.Name = "BindingNavigator5";
            this.BindingNavigator5.PositionItem = null;
            this.BindingNavigator5.Size = new System.Drawing.Size(35, 25);
            this.BindingNavigator5.TabIndex = 21;
            this.BindingNavigator5.Text = "BindingNavigator5";
            this.BindingNavigator5.Visible = false;
            // 
            // ToolStripButton4
            // 
            this.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripButton4.Image")));
            this.ToolStripButton4.Name = "ToolStripButton4";
            this.ToolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButton4.Text = "Сохранить данные";
            // 
            // TextBox15
            // 
            this.TextBox15.Location = new System.Drawing.Point(634, 110);
            this.TextBox15.Name = "TextBox15";
            this.TextBox15.ReadOnly = true;
            this.TextBox15.Size = new System.Drawing.Size(45, 22);
            this.TextBox15.TabIndex = 20;
            // 
            // TextBox16
            // 
            this.TextBox16.Location = new System.Drawing.Point(634, 78);
            this.TextBox16.Name = "TextBox16";
            this.TextBox16.ReadOnly = true;
            this.TextBox16.Size = new System.Drawing.Size(45, 22);
            this.TextBox16.TabIndex = 19;
            // 
            // TextBox17
            // 
            this.TextBox17.Location = new System.Drawing.Point(634, 49);
            this.TextBox17.Name = "TextBox17";
            this.TextBox17.ReadOnly = true;
            this.TextBox17.Size = new System.Drawing.Size(45, 22);
            this.TextBox17.TabIndex = 18;
            // 
            // TextBox18
            // 
            this.TextBox18.Location = new System.Drawing.Point(634, 20);
            this.TextBox18.Name = "TextBox18";
            this.TextBox18.ReadOnly = true;
            this.TextBox18.Size = new System.Drawing.Size(45, 22);
            this.TextBox18.TabIndex = 17;
            // 
            // TextBox14
            // 
            this.TextBox14.Location = new System.Drawing.Point(314, 105);
            this.TextBox14.Name = "TextBox14";
            this.TextBox14.Size = new System.Drawing.Size(45, 22);
            this.TextBox14.TabIndex = 10;
            // 
            // TextBox13
            // 
            this.TextBox13.Location = new System.Drawing.Point(314, 73);
            this.TextBox13.Name = "TextBox13";
            this.TextBox13.Size = new System.Drawing.Size(45, 22);
            this.TextBox13.TabIndex = 9;
            // 
            // TextBox12
            // 
            this.TextBox12.Location = new System.Drawing.Point(314, 44);
            this.TextBox12.Name = "TextBox12";
            this.TextBox12.Size = new System.Drawing.Size(45, 22);
            this.TextBox12.TabIndex = 8;
            // 
            // TextBox11
            // 
            this.TextBox11.Location = new System.Drawing.Point(314, 15);
            this.TextBox11.Name = "TextBox11";
            this.TextBox11.Size = new System.Drawing.Size(45, 22);
            this.TextBox11.TabIndex = 7;
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Location = new System.Drawing.Point(418, 118);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(183, 17);
            this.Label19.TabIndex = 16;
            this.Label19.Text = "зазор рельсового тормоза";
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Location = new System.Drawing.Point(486, 86);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(127, 17);
            this.Label20.TabIndex = 15;
            this.Label20.Text = "толщина бондажа";
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.Location = new System.Drawing.Point(486, 57);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(55, 17);
            this.Label21.TabIndex = 14;
            this.Label21.Text = "высота";
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.Location = new System.Drawing.Point(486, 28);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(59, 17);
            this.Label22.TabIndex = 13;
            this.Label22.Text = "ширина";
            // 
            // Label23
            // 
            this.Label23.AutoSize = true;
            this.Label23.Location = new System.Drawing.Point(402, 23);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(64, 17);
            this.Label23.TabIndex = 12;
            this.Label23.Text = "реборда";
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Location = new System.Drawing.Point(400, 3);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(73, 17);
            this.Label18.TabIndex = 11;
            this.Label18.Text = "Норматив";
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Location = new System.Drawing.Point(75, 113);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(183, 17);
            this.Label17.TabIndex = 6;
            this.Label17.Text = "зазор рельсового тормоза";
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Location = new System.Drawing.Point(80, 81);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(127, 17);
            this.Label16.TabIndex = 5;
            this.Label16.Text = "толщина бондажа";
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Location = new System.Drawing.Point(106, 3);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(88, 17);
            this.Label15.TabIndex = 4;
            this.Label15.Text = "измеренная";
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(82, 52);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(55, 17);
            this.Label14.TabIndex = 3;
            this.Label14.Text = "высота";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(166, 25);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(59, 17);
            this.Label13.TabIndex = 2;
            this.Label13.Text = "ширина";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(80, 23);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(64, 17);
            this.Label4.TabIndex = 1;
            this.Label4.Text = "реборда";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(6, 3);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(73, 17);
            this.Label3.TabIndex = 0;
            this.Label3.Text = "Величина";
            // 
            // TabPage6
            // 
            this.TabPage6.Controls.Add(this.checkedListBox4);
            this.TabPage6.Controls.Add(this.BindingNavigator6);
            this.TabPage6.Location = new System.Drawing.Point(4, 25);
            this.TabPage6.Name = "TabPage6";
            this.TabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage6.Size = new System.Drawing.Size(717, 171);
            this.TabPage6.TabIndex = 5;
            this.TabPage6.Text = "МУ";
            this.TabPage6.UseVisualStyleBackColor = true;
            // 
            // checkedListBox4
            // 
            this.checkedListBox4.BackColor = System.Drawing.SystemColors.Window;
            this.checkedListBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.checkedListBox4.CheckOnClick = true;
            this.checkedListBox4.ColumnWidth = 300;
            this.checkedListBox4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkedListBox4.Items.AddRange(new object[] {
            "Лобовые",
            "Боковые",
            "Автоинформатор"});
            this.checkedListBox4.Location = new System.Drawing.Point(16, 10);
            this.checkedListBox4.MultiColumn = true;
            this.checkedListBox4.Name = "checkedListBox4";
            this.checkedListBox4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkedListBox4.Size = new System.Drawing.Size(683, 125);
            this.checkedListBox4.TabIndex = 16;
            this.checkedListBox4.UseTabStops = false;
            // 
            // BindingNavigator6
            // 
            this.BindingNavigator6.AddNewItem = null;
            this.BindingNavigator6.CountItem = null;
            this.BindingNavigator6.DeleteItem = null;
            this.BindingNavigator6.Dock = System.Windows.Forms.DockStyle.None;
            this.BindingNavigator6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButton5});
            this.BindingNavigator6.Location = new System.Drawing.Point(676, 141);
            this.BindingNavigator6.MoveFirstItem = null;
            this.BindingNavigator6.MoveLastItem = null;
            this.BindingNavigator6.MoveNextItem = null;
            this.BindingNavigator6.MovePreviousItem = null;
            this.BindingNavigator6.Name = "BindingNavigator6";
            this.BindingNavigator6.PositionItem = null;
            this.BindingNavigator6.Size = new System.Drawing.Size(35, 25);
            this.BindingNavigator6.TabIndex = 12;
            this.BindingNavigator6.Text = "BindingNavigator6";
            // 
            // ToolStripButton5
            // 
            this.ToolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripButton5.Image")));
            this.ToolStripButton5.Name = "ToolStripButton5";
            this.ToolStripButton5.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButton5.Text = "Сохранить данные";
            this.ToolStripButton5.Click += new System.EventHandler(this.ToolStripButton5_Click);
            // 
            // TabPage8
            // 
            this.TabPage8.Controls.Add(this.checkedListBox5);
            this.TabPage8.Controls.Add(this.BindingNavigator7);
            this.TabPage8.Location = new System.Drawing.Point(4, 25);
            this.TabPage8.Name = "TabPage8";
            this.TabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage8.Size = new System.Drawing.Size(717, 171);
            this.TabPage8.TabIndex = 7;
            this.TabPage8.Text = "Прочее";
            this.TabPage8.UseVisualStyleBackColor = true;
            // 
            // checkedListBox5
            // 
            this.checkedListBox5.BackColor = System.Drawing.SystemColors.Window;
            this.checkedListBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.checkedListBox5.CheckOnClick = true;
            this.checkedListBox5.ColumnWidth = 300;
            this.checkedListBox5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkedListBox5.Items.AddRange(new object[] {
            "Отбойный брус",
            "Сцепной прибор",
            "Песочница",
            "Сигнал водителю",
            "Стоп-краны"});
            this.checkedListBox5.Location = new System.Drawing.Point(16, 13);
            this.checkedListBox5.MultiColumn = true;
            this.checkedListBox5.Name = "checkedListBox5";
            this.checkedListBox5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkedListBox5.Size = new System.Drawing.Size(683, 125);
            this.checkedListBox5.TabIndex = 16;
            this.checkedListBox5.UseTabStops = false;
            // 
            // BindingNavigator7
            // 
            this.BindingNavigator7.AddNewItem = null;
            this.BindingNavigator7.CountItem = null;
            this.BindingNavigator7.DeleteItem = null;
            this.BindingNavigator7.Dock = System.Windows.Forms.DockStyle.None;
            this.BindingNavigator7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButton6});
            this.BindingNavigator7.Location = new System.Drawing.Point(678, 141);
            this.BindingNavigator7.MoveFirstItem = null;
            this.BindingNavigator7.MoveLastItem = null;
            this.BindingNavigator7.MoveNextItem = null;
            this.BindingNavigator7.MovePreviousItem = null;
            this.BindingNavigator7.Name = "BindingNavigator7";
            this.BindingNavigator7.PositionItem = null;
            this.BindingNavigator7.Size = new System.Drawing.Size(35, 25);
            this.BindingNavigator7.TabIndex = 12;
            this.BindingNavigator7.Text = "BindingNavigator7";
            // 
            // ToolStripButton6
            // 
            this.ToolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripButton6.Image")));
            this.ToolStripButton6.Name = "ToolStripButton6";
            this.ToolStripButton6.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButton6.Text = "Сохранить данные";
            this.ToolStripButton6.Click += new System.EventHandler(this.ToolStripButton6_Click);
            // 
            // CheckBox1
            // 
            this.CheckBox1.AutoSize = true;
            this.CheckBox1.Checked = true;
            this.CheckBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox1.Enabled = false;
            this.CheckBox1.Location = new System.Drawing.Point(7, 3);
            this.CheckBox1.Name = "CheckBox1";
            this.CheckBox1.Size = new System.Drawing.Size(103, 21);
            this.CheckBox1.TabIndex = 0;
            this.CheckBox1.Text = "Проверено";
            this.CheckBox1.UseVisualStyleBackColor = true;
            // 
            // GroupBox1
            // 
            this.GroupBox1.BackColor = System.Drawing.SystemColors.Window;
            this.GroupBox1.Controls.Add(this.Panel6);
            this.GroupBox1.Controls.Add(this.Panel4);
            this.GroupBox1.Controls.Add(this.PictureBox1);
            this.GroupBox1.Controls.Add(this.TextBox1);
            this.GroupBox1.Controls.Add(this.TextBox7);
            this.GroupBox1.Controls.Add(this.TextBox8);
            this.GroupBox1.Controls.Add(this.Label8);
            this.GroupBox1.Controls.Add(this.TextBox9);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Controls.Add(this.Label12);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.TextBox2);
            this.GroupBox1.Controls.Add(this.TextBox3);
            this.GroupBox1.Controls.Add(this.Label9);
            this.GroupBox1.Controls.Add(this.Label6);
            this.GroupBox1.Location = new System.Drawing.Point(3, 3);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(306, 640);
            this.GroupBox1.TabIndex = 2;
            this.GroupBox1.TabStop = false;
            // 
            // Panel6
            // 
            this.Panel6.Controls.Add(NadbavkaLabel);
            this.Panel6.Controls.Add(this.NadbavkaTextBox);
            this.Panel6.Controls.Add(Indikator_2Label);
            this.Panel6.Controls.Add(this.Indikator_2TextBox);
            this.Panel6.Controls.Add(Indikator_1Label);
            this.Panel6.Controls.Add(this.Indikator_1TextBox);
            this.Panel6.Location = new System.Drawing.Point(21, 533);
            this.Panel6.Name = "Panel6";
            this.Panel6.Size = new System.Drawing.Size(242, 101);
            this.Panel6.TabIndex = 39;
            // 
            // NadbavkaTextBox
            // 
            this.NadbavkaTextBox.Location = new System.Drawing.Point(109, 64);
            this.NadbavkaTextBox.Name = "NadbavkaTextBox";
            this.NadbavkaTextBox.Size = new System.Drawing.Size(100, 22);
            this.NadbavkaTextBox.TabIndex = 5;
            // 
            // Indikator_2TextBox
            // 
            this.Indikator_2TextBox.Location = new System.Drawing.Point(109, 34);
            this.Indikator_2TextBox.Name = "Indikator_2TextBox";
            this.Indikator_2TextBox.Size = new System.Drawing.Size(100, 22);
            this.Indikator_2TextBox.TabIndex = 3;
            // 
            // Indikator_1TextBox
            // 
            this.Indikator_1TextBox.Location = new System.Drawing.Point(109, 3);
            this.Indikator_1TextBox.Name = "Indikator_1TextBox";
            this.Indikator_1TextBox.Size = new System.Drawing.Size(100, 22);
            this.Indikator_1TextBox.TabIndex = 1;
            // 
            // Panel4
            // 
            this.Panel4.Controls.Add(this.Label36);
            this.Panel4.Controls.Add(this.Label37);
            this.Panel4.Location = new System.Drawing.Point(20, 486);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(168, 41);
            this.Panel4.TabIndex = 38;
            // 
            // Label36
            // 
            this.Label36.AutoSize = true;
            this.Label36.Location = new System.Drawing.Point(94, 11);
            this.Label36.Name = "Label36";
            this.Label36.Size = new System.Drawing.Size(40, 17);
            this.Label36.TabIndex = 1;
            this.Label36.Text = "1270";
            // 
            // Label37
            // 
            this.Label37.AutoSize = true;
            this.Label37.Location = new System.Drawing.Point(16, 11);
            this.Label37.Name = "Label37";
            this.Label37.Size = new System.Drawing.Size(67, 17);
            this.Label37.TabIndex = 0;
            this.Label37.Text = "Табел №";
            // 
            // PictureBox1
            // 
            this.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureBox1.Location = new System.Drawing.Point(40, 63);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(253, 177);
            this.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox1.TabIndex = 20;
            this.PictureBox1.TabStop = false;
            // 
            // TextBox1
            // 
            this.TextBox1.Location = new System.Drawing.Point(193, 268);
            this.TextBox1.MaxLength = 4;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Size = new System.Drawing.Size(100, 22);
            this.TextBox1.TabIndex = 3;
            // 
            // TextBox7
            // 
            this.TextBox7.Location = new System.Drawing.Point(194, 426);
            this.TextBox7.Name = "TextBox7";
            this.TextBox7.ReadOnly = true;
            this.TextBox7.Size = new System.Drawing.Size(100, 22);
            this.TextBox7.TabIndex = 19;
            this.TextBox7.Text = "Нет данных";
            // 
            // TextBox8
            // 
            this.TextBox8.Location = new System.Drawing.Point(194, 396);
            this.TextBox8.Name = "TextBox8";
            this.TextBox8.ReadOnly = true;
            this.TextBox8.Size = new System.Drawing.Size(100, 22);
            this.TextBox8.TabIndex = 18;
            this.TextBox8.Text = "Нет данных";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(18, 434);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(148, 17);
            this.Label8.TabIndex = 8;
            this.Label8.Text = "Дата  последнего КР";
            // 
            // TextBox9
            // 
            this.TextBox9.Location = new System.Drawing.Point(193, 332);
            this.TextBox9.Name = "TextBox9";
            this.TextBox9.ReadOnly = true;
            this.TextBox9.Size = new System.Drawing.Size(100, 22);
            this.TextBox9.TabIndex = 16;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(18, 276);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(64, 17);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Вагон №";
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(18, 404);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(158, 17);
            this.Label12.TabIndex = 16;
            this.Label12.Text = "Дата  последнего ТО2\r\n";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(18, 308);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(58, 17);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "Модель";
            // 
            // TextBox2
            // 
            this.TextBox2.Location = new System.Drawing.Point(193, 300);
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.ReadOnly = true;
            this.TextBox2.Size = new System.Drawing.Size(100, 22);
            this.TextBox2.TabIndex = 4;
            // 
            // TextBox3
            // 
            this.TextBox3.Location = new System.Drawing.Point(193, 364);
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.ReadOnly = true;
            this.TextBox3.Size = new System.Drawing.Size(100, 22);
            this.TextBox3.TabIndex = 10;
            this.TextBox3.Text = "Нет данных";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(18, 340);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(90, 17);
            this.Label9.TabIndex = 14;
            this.Label9.Text = "Год выпуска";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(18, 372);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(158, 17);
            this.Label6.TabIndex = 6;
            this.Label6.Text = "Дата  последнего ТО1";
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.SystemColors.Window;
            this.Panel3.Controls.Add(this.PictureBox2);
            this.Panel3.Controls.Add(this.GroupBox5);
            this.Panel3.Controls.Add(this.DataLabel1);
            this.Panel3.Controls.Add(this.TimeLabel1);
            this.Panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel3.Location = new System.Drawing.Point(1093, 3);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(188, 640);
            this.Panel3.TabIndex = 22;
            // 
            // PictureBox2
            // 
            this.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureBox2.Location = new System.Drawing.Point(21, 69);
            this.PictureBox2.Name = "PictureBox2";
            this.PictureBox2.Size = new System.Drawing.Size(151, 130);
            this.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox2.TabIndex = 0;
            this.PictureBox2.TabStop = false;
            // 
            // GroupBox5
            // 
            this.GroupBox5.Controls.Add(this.Label35);
            this.GroupBox5.Controls.Add(this.Label34);
            this.GroupBox5.Controls.Add(this.Label11);
            this.GroupBox5.Controls.Add(this.Label26);
            this.GroupBox5.Controls.Add(this.Label25);
            this.GroupBox5.Controls.Add(this.Label24);
            this.GroupBox5.Location = new System.Drawing.Point(3, 235);
            this.GroupBox5.Name = "GroupBox5";
            this.GroupBox5.Size = new System.Drawing.Size(182, 213);
            this.GroupBox5.TabIndex = 22;
            this.GroupBox5.TabStop = false;
            this.GroupBox5.Text = "Мастер ЕО";
            // 
            // Label35
            // 
            this.Label35.AutoSize = true;
            this.Label35.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label35.Location = new System.Drawing.Point(48, 191);
            this.Label35.Name = "Label35";
            this.Label35.Size = new System.Drawing.Size(81, 24);
            this.Label35.TabIndex = 40;
            this.Label35.Text = "Label35";
            // 
            // Label34
            // 
            this.Label34.AutoSize = true;
            this.Label34.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label34.Location = new System.Drawing.Point(48, 126);
            this.Label34.Name = "Label34";
            this.Label34.Size = new System.Drawing.Size(81, 24);
            this.Label34.TabIndex = 39;
            this.Label34.Text = "Label34";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label11.Location = new System.Drawing.Point(48, 66);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(80, 24);
            this.Label11.TabIndex = 38;
            this.Label11.Text = "Label11";
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Location = new System.Drawing.Point(15, 159);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(71, 17);
            this.Label26.TabIndex = 2;
            this.Label26.Text = "Отчество";
            // 
            // Label25
            // 
            this.Label25.AutoSize = true;
            this.Label25.Location = new System.Drawing.Point(15, 100);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(35, 17);
            this.Label25.TabIndex = 1;
            this.Label25.Text = "Имя";
            // 
            // Label24
            // 
            this.Label24.AutoSize = true;
            this.Label24.Location = new System.Drawing.Point(15, 39);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(70, 17);
            this.Label24.TabIndex = 0;
            this.Label24.Text = "Фамилия";
            // 
            // DataLabel1
            // 
            this.DataLabel1.Location = new System.Drawing.Point(28, 572);
            this.DataLabel1.Name = "DataLabel1";
            this.DataLabel1.Size = new System.Drawing.Size(100, 23);
            this.DataLabel1.TabIndex = 37;
            this.DataLabel1.Text = "Label10";
            // 
            // TimeLabel1
            // 
            this.TimeLabel1.Location = new System.Drawing.Point(18, 595);
            this.TimeLabel1.Name = "TimeLabel1";
            this.TimeLabel1.Size = new System.Drawing.Size(100, 23);
            this.TimeLabel1.TabIndex = 36;
            this.TimeLabel1.Text = "Label11";
            // 
            // Panel5
            // 
            this.Panel5.Controls.Add(this.Label31);
            this.Panel5.Controls.Add(this.TextBox25);
            this.Panel5.Controls.Add(this.TextBox26);
            this.Panel5.Controls.Add(this.Label32);
            this.Panel5.Controls.Add(this.Label33);
            this.Panel5.Location = new System.Drawing.Point(3, 649);
            this.Panel5.Name = "Panel5";
            this.Panel5.Size = new System.Drawing.Size(216, 95);
            this.Panel5.TabIndex = 21;
            // 
            // Label31
            // 
            this.Label31.AutoSize = true;
            this.Label31.Location = new System.Drawing.Point(17, 14);
            this.Label31.Name = "Label31";
            this.Label31.Size = new System.Drawing.Size(164, 17);
            this.Label31.TabIndex = 19;
            this.Label31.Text = "Скрытая подпрограмма";
            // 
            // TextBox25
            // 
            this.TextBox25.Location = new System.Drawing.Point(20, 37);
            this.TextBox25.Name = "TextBox25";
            this.TextBox25.ReadOnly = true;
            this.TextBox25.Size = new System.Drawing.Size(100, 22);
            this.TextBox25.TabIndex = 10;
            this.TextBox25.Text = ".jpg";
            // 
            // TextBox26
            // 
            this.TextBox26.Location = new System.Drawing.Point(19, 63);
            this.TextBox26.Name = "TextBox26";
            this.TextBox26.ReadOnly = true;
            this.TextBox26.Size = new System.Drawing.Size(100, 22);
            this.TextBox26.TabIndex = 4;
            // 
            // Label32
            // 
            this.Label32.AutoSize = true;
            this.Label32.Location = new System.Drawing.Point(145, 66);
            this.Label32.Name = "Label32";
            this.Label32.Size = new System.Drawing.Size(59, 17);
            this.Label32.TabIndex = 17;
            this.Label32.Text = "Label32";
            // 
            // Label33
            // 
            this.Label33.AutoSize = true;
            this.Label33.Location = new System.Drawing.Point(136, 37);
            this.Label33.Name = "Label33";
            this.Label33.Size = new System.Drawing.Size(59, 17);
            this.Label33.TabIndex = 18;
            this.Label33.Text = "Label33";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(6, 31);
            this.progressBar1.Maximum = 70;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(325, 26);
            this.progressBar1.TabIndex = 11;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.groupBox6);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(315, 649);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(772, 126);
            this.panel7.TabIndex = 23;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(342, 31);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(70, 22);
            this.label39.TabIndex = 13;
            this.label39.Text = "label39";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.checkBox2);
            this.groupBox2.Controls.Add(this.progressBar1);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(337, 487);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(421, 130);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ход проверки-готовности вагона";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(148, 93);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(18, 17);
            this.checkBox2.TabIndex = 14;
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(183, 83);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(229, 35);
            this.button2.TabIndex = 15;
            this.button2.Text = "Отправка вагона в наряд";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(13, 37);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(325, 17);
            this.label38.TabIndex = 0;
            this.label38.Text = "Программа работает на операционной системе";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label40);
            this.groupBox6.Controls.Add(this.label38);
            this.groupBox6.Location = new System.Drawing.Point(21, 14);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(736, 100);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(344, 37);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(54, 17);
            this.label40.TabIndex = 1;
            this.label40.Text = "label40";
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.BackColor = System.Drawing.SystemColors.Window;
            this.StatusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.StatusStrip1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel1,
            this.ssdate,
            this.sstime});
            this.StatusStrip1.Location = new System.Drawing.Point(310, 9);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StatusStrip1.Size = new System.Drawing.Size(507, 28);
            this.StatusStrip1.TabIndex = 40;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // ToolStripStatusLabel1
            // 
            this.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1";
            this.ToolStripStatusLabel1.Size = new System.Drawing.Size(86, 23);
            this.ToolStripStatusLabel1.Text = "Сегодня";
            // 
            // ssdate
            // 
            this.ssdate.Name = "ssdate";
            this.ssdate.Size = new System.Drawing.Size(202, 23);
            this.ssdate.Text = "ToolStripStatusLabel2";
            // 
            // sstime
            // 
            this.sstime.Name = "sstime";
            this.sstime.Size = new System.Drawing.Size(202, 23);
            this.sstime.Text = "ToolStripStatusLabel3";
            // 
            // Sama_programma_eged_osm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 778);
            this.Controls.Add(this.TableLayoutPanel1);
            this.Name = "Sama_programma_eged_osm";
            this.Text = "Sama_programma_eged_osm";
            this.TableLayoutPanel1.ResumeLayout(false);
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.MenuStrip1.ResumeLayout(false);
            this.MenuStrip1.PerformLayout();
            this.GroupBox4.ResumeLayout(false);
            this.GroupBox4.PerformLayout();
            this.TabControl2.ResumeLayout(false);
            this.TabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            this.TabPage10.ResumeLayout(false);
            this.TabPage10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView2)).EndInit();
            this.GroupBox3.ResumeLayout(false);
            this.GroupBox3.PerformLayout();
            this.MenuStrip2.ResumeLayout(false);
            this.MenuStrip2.PerformLayout();
            this.Panel2.ResumeLayout(false);
            this.Panel2.PerformLayout();
            this.TabControl1.ResumeLayout(false);
            this.TabPage1.ResumeLayout(false);
            this.TabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator1)).EndInit();
            this.BindingNavigator1.ResumeLayout(false);
            this.BindingNavigator1.PerformLayout();
            this.TabPage2.ResumeLayout(false);
            this.TabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator9)).EndInit();
            this.BindingNavigator9.ResumeLayout(false);
            this.BindingNavigator9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator8)).EndInit();
            this.BindingNavigator8.ResumeLayout(false);
            this.BindingNavigator8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator2)).EndInit();
            this.BindingNavigator2.ResumeLayout(false);
            this.BindingNavigator2.PerformLayout();
            this.TabPage3.ResumeLayout(false);
            this.TabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator3)).EndInit();
            this.BindingNavigator3.ResumeLayout(false);
            this.BindingNavigator3.PerformLayout();
            this.TabPage4.ResumeLayout(false);
            this.TabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator4)).EndInit();
            this.BindingNavigator4.ResumeLayout(false);
            this.BindingNavigator4.PerformLayout();
            this.TabPage5.ResumeLayout(false);
            this.TabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator5)).EndInit();
            this.BindingNavigator5.ResumeLayout(false);
            this.BindingNavigator5.PerformLayout();
            this.TabPage6.ResumeLayout(false);
            this.TabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator6)).EndInit();
            this.BindingNavigator6.ResumeLayout(false);
            this.BindingNavigator6.PerformLayout();
            this.TabPage8.ResumeLayout(false);
            this.TabPage8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator7)).EndInit();
            this.BindingNavigator7.ResumeLayout(false);
            this.BindingNavigator7.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.Panel6.ResumeLayout(false);
            this.Panel6.PerformLayout();
            this.Panel4.ResumeLayout(false);
            this.Panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.Panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).EndInit();
            this.GroupBox5.ResumeLayout(false);
            this.GroupBox5.PerformLayout();
            this.Panel5.ResumeLayout(false);
            this.Panel5.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        internal System.Windows.Forms.Panel Panel5;
        internal System.Windows.Forms.Label Label31;
        internal System.Windows.Forms.TextBox TextBox25;
        internal System.Windows.Forms.TextBox TextBox26;
        internal System.Windows.Forms.Label Label32;
        internal System.Windows.Forms.Label Label33;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.MenuStrip MenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem ВыберитеПользователяToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ПриёмщикаВагоновToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox1;
        internal System.Windows.Forms.ToolStripMenuItem МастераЦТОToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox3;
        internal System.Windows.Forms.ToolStripMenuItem СлесарейToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem МастераТО1ToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox4;
        internal System.Windows.Forms.ToolStripMenuItem МастераТО2ToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox6;
        internal System.Windows.Forms.ToolStripMenuItem МастераСРToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox5;
        internal System.Windows.Forms.ToolStripMenuItem ОтделенияToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem МалярногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox7;
        internal System.Windows.Forms.ToolStripMenuItem СлесарноКузовногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox8;
        internal System.Windows.Forms.ToolStripMenuItem РедукторногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox9;
        internal System.Windows.Forms.ToolStripMenuItem МеханическогоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox11;
        internal System.Windows.Forms.ToolStripMenuItem КолесотокарногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox12;
        internal System.Windows.Forms.ToolStripMenuItem МастерскиеToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem РадиотехническойToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox13;
        internal System.Windows.Forms.ToolStripMenuItem АккумуляторнойToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox14;
        internal System.Windows.Forms.ToolStripMenuItem УчасткаToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem РемонтаТокоприемниковToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox15;
        internal System.Windows.Forms.ToolStripMenuItem ЭлектроаппаратногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox16;
        internal System.Windows.Forms.ToolStripMenuItem РемонтаДвигателейToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox17;
        internal System.Windows.Forms.ToolStripMenuItem ЦехаToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ЭлектросварочногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox18;
        internal System.Windows.Forms.ToolStripMenuItem ТележечногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox19;
        internal System.Windows.Forms.ToolStripMenuItem АдминистрацииToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox2;
        internal System.Windows.Forms.ToolStripMenuItem ОтчетыToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ПриёмщикаВагоновToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem ПрохождениеЕОToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ВыходToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem2;
        internal System.Windows.Forms.ToolStripMenuItem ВыходToolStripMenuItem2;
        internal System.Windows.Forms.ToolStripMenuItem ЗамечанияToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem СлесарямToolStripMenuItem1;
        internal System.Windows.Forms.GroupBox GroupBox4;
        internal System.Windows.Forms.TabControl TabControl2;
        internal System.Windows.Forms.TabPage TabPage9;
        internal System.Windows.Forms.DataGridView DataGridView1;
        internal System.Windows.Forms.TabPage TabPage10;
        internal System.Windows.Forms.Label Predmet_ystLabel1;
        internal System.Windows.Forms.Label NameLabel1;
        internal System.Windows.Forms.Label Time_2Label1;
        internal System.Windows.Forms.Label Data_2Label1;
        internal System.Windows.Forms.Label Label30;
        internal System.Windows.Forms.Label Label29;
        internal System.Windows.Forms.Label Label28;
        internal System.Windows.Forms.Label Label27;
        internal System.Windows.Forms.DataGridView DataGridView2;
        internal System.Windows.Forms.GroupBox GroupBox3;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.MenuStrip MenuStrip2;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem СлесарямToolStripMenuItem;
        internal System.Windows.Forms.CheckBox CheckBox3;
        internal System.Windows.Forms.TabPage TabPage1;
        internal System.Windows.Forms.BindingNavigator BindingNavigator1;
        internal System.Windows.Forms.ToolStripButton Vagon_spisokBindingNavigatorSaveItem21;
        internal System.Windows.Forms.TabPage TabPage2;
        internal System.Windows.Forms.BindingNavigator BindingNavigator9;
        internal System.Windows.Forms.ToolStripButton ToolStripButton8;
        internal System.Windows.Forms.BindingNavigator BindingNavigator8;
        internal System.Windows.Forms.ToolStripButton ToolStripButton7;
        internal System.Windows.Forms.BindingNavigator BindingNavigator2;
        internal System.Windows.Forms.ToolStripButton ToolStripButton1;
        internal System.Windows.Forms.TabPage TabPage3;
        internal System.Windows.Forms.BindingNavigator BindingNavigator3;
        internal System.Windows.Forms.ToolStripButton ToolStripButton2;
        internal System.Windows.Forms.TabPage TabPage4;
        internal System.Windows.Forms.BindingNavigator BindingNavigator4;
        internal System.Windows.Forms.ToolStripButton ToolStripButton3;
        internal System.Windows.Forms.TabPage TabPage5;
        internal System.Windows.Forms.BindingNavigator BindingNavigator5;
        internal System.Windows.Forms.ToolStripButton ToolStripButton4;
        internal System.Windows.Forms.TextBox TextBox15;
        internal System.Windows.Forms.TextBox TextBox16;
        internal System.Windows.Forms.TextBox TextBox17;
        internal System.Windows.Forms.TextBox TextBox18;
        internal System.Windows.Forms.TextBox TextBox14;
        internal System.Windows.Forms.TextBox TextBox13;
        internal System.Windows.Forms.TextBox TextBox12;
        internal System.Windows.Forms.TextBox TextBox11;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.Label Label21;
        internal System.Windows.Forms.Label Label22;
        internal System.Windows.Forms.Label Label23;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.Label Label17;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TabPage TabPage6;
        internal System.Windows.Forms.BindingNavigator BindingNavigator6;
        internal System.Windows.Forms.ToolStripButton ToolStripButton5;
        internal System.Windows.Forms.TabPage TabPage8;
        internal System.Windows.Forms.BindingNavigator BindingNavigator7;
        internal System.Windows.Forms.ToolStripButton ToolStripButton6;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Panel Panel6;
        internal System.Windows.Forms.TextBox NadbavkaTextBox;
        internal System.Windows.Forms.TextBox Indikator_2TextBox;
        internal System.Windows.Forms.TextBox Indikator_1TextBox;
        internal System.Windows.Forms.Panel Panel4;
        internal System.Windows.Forms.Label Label36;
        internal System.Windows.Forms.Label Label37;
        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.TextBox TextBox1;
        internal System.Windows.Forms.TextBox TextBox7;
        internal System.Windows.Forms.TextBox TextBox8;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox TextBox9;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox TextBox2;
        internal System.Windows.Forms.TextBox TextBox3;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.PictureBox PictureBox2;
        internal System.Windows.Forms.GroupBox GroupBox5;
        internal System.Windows.Forms.Label Label35;
        internal System.Windows.Forms.Label Label34;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.Label Label25;
        internal System.Windows.Forms.Label Label24;
        internal System.Windows.Forms.Label DataLabel1;
        internal System.Windows.Forms.Label TimeLabel1;
        public System.Windows.Forms.Label Label5;
        public System.Windows.Forms.CheckBox CheckBox1;
        public System.Windows.Forms.Panel Panel2;
        public System.Windows.Forms.TabControl TabControl1;
        public System.Windows.Forms.CheckedListBox checkedListBox1;
        public System.Windows.Forms.CheckedListBox checkedListBox2;
        public System.Windows.Forms.CheckedListBox checkedListBox3;
        public System.Windows.Forms.CheckedListBox checkedListBox4;
        public System.Windows.Forms.CheckedListBox checkedListBox5;
        public System.Windows.Forms.CheckedListBox checkedListBox6;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label38;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel1;
        internal System.Windows.Forms.ToolStripStatusLabel ssdate;
        internal System.Windows.Forms.ToolStripStatusLabel sstime;
    }
}