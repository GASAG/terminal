﻿namespace Terminal_per
{
    partial class Sama_programma
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.PictureBox19 = new System.Windows.Forms.PictureBox();
            this.PictureBox18 = new System.Windows.Forms.PictureBox();
            this.Panel6 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.PictureBox11 = new System.Windows.Forms.PictureBox();
            this.PictureBox12 = new System.Windows.Forms.PictureBox();
            this.MenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ВыберитеПользователяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ПриёмщикаВагоновToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.МастераЦТОToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox3 = new System.Windows.Forms.ToolStripTextBox();
            this.СлесарейToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.МастераТО1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox4 = new System.Windows.Forms.ToolStripTextBox();
            this.МастераТО2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox6 = new System.Windows.Forms.ToolStripTextBox();
            this.МастераСРToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox5 = new System.Windows.Forms.ToolStripTextBox();
            this.ОтделенияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.МалярногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox7 = new System.Windows.Forms.ToolStripTextBox();
            this.СлесарноКузовногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox8 = new System.Windows.Forms.ToolStripTextBox();
            this.РедукторногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox9 = new System.Windows.Forms.ToolStripTextBox();
            this.МеханическогоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox11 = new System.Windows.Forms.ToolStripTextBox();
            this.КолесотокарногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox12 = new System.Windows.Forms.ToolStripTextBox();
            this.МастерскиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.РадиотехническойToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox13 = new System.Windows.Forms.ToolStripTextBox();
            this.АккумуляторнойToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox14 = new System.Windows.Forms.ToolStripTextBox();
            this.УчасткаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.РемонтаТокоприемниковToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox15 = new System.Windows.Forms.ToolStripTextBox();
            this.ЭлектроаппаратногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox16 = new System.Windows.Forms.ToolStripTextBox();
            this.РемонтаДвигателейToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox17 = new System.Windows.Forms.ToolStripTextBox();
            this.ЦехаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ЭлектросварочногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox18 = new System.Windows.Forms.ToolStripTextBox();
            this.ТележечногоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox19 = new System.Windows.Forms.ToolStripTextBox();
            this.АдминистрацииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.ОтчетыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ПриёмщикаВагоновToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ПрохождениеЕОToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ВыходToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ВыходToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.PictureBox17 = new System.Windows.Forms.PictureBox();
            this.PictureBox9 = new System.Windows.Forms.PictureBox();
            this.PictureBox10 = new System.Windows.Forms.PictureBox();
            this.PictureBox16 = new System.Windows.Forms.PictureBox();
            this.PictureBox15 = new System.Windows.Forms.PictureBox();
            this.PictureBox14 = new System.Windows.Forms.PictureBox();
            this.PictureBox13 = new System.Windows.Forms.PictureBox();
            this.PictureBox8 = new System.Windows.Forms.PictureBox();
            this.PictureBox7 = new System.Windows.Forms.PictureBox();
            this.PictureBox6 = new System.Windows.Forms.PictureBox();
            this.PictureBox5 = new System.Windows.Forms.PictureBox();
            this.PictureBox4 = new System.Windows.Forms.PictureBox();
            this.PictureBox3 = new System.Windows.Forms.PictureBox();
            this.PictureBox2 = new System.Windows.Forms.PictureBox();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ssdate = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusStrip2 = new System.Windows.Forms.StatusStrip();
            this.sstime = new System.Windows.Forms.ToolStripStatusLabel();
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.TableLayoutPanel1.SuspendLayout();
            this.Panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox18)).BeginInit();
            this.Panel6.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox12)).BeginInit();
            this.MenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.GroupBox2.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            this.StatusStrip2.SuspendLayout();
            this.GroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.AutoSize = true;
            this.TableLayoutPanel1.ColumnCount = 3;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.15576F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.28349F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.56075F));
            this.TableLayoutPanel1.Controls.Add(this.Panel2, 2, 1);
            this.TableLayoutPanel1.Controls.Add(this.Panel6, 0, 1);
            this.TableLayoutPanel1.Controls.Add(this.GroupBox1, 1, 1);
            this.TableLayoutPanel1.Controls.Add(this.GroupBox2, 1, 0);
            this.TableLayoutPanel1.Controls.Add(this.GroupBox3, 2, 0);
            this.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 3;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.TableLayoutPanel1.Size = new System.Drawing.Size(1284, 778);
            this.TableLayoutPanel1.TabIndex = 8;
            // 
            // Panel2
            // 
            this.Panel2.Controls.Add(this.PictureBox19);
            this.Panel2.Controls.Add(this.PictureBox18);
            this.Panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel2.Location = new System.Drawing.Point(1022, 119);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(259, 499);
            this.Panel2.TabIndex = 28;
            // 
            // PictureBox19
            // 
            this.PictureBox19.Location = new System.Drawing.Point(21, 163);
            this.PictureBox19.Name = "PictureBox19";
            this.PictureBox19.Size = new System.Drawing.Size(151, 119);
            this.PictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox19.TabIndex = 1;
            this.PictureBox19.TabStop = false;
            // 
            // PictureBox18
            // 
            this.PictureBox18.Location = new System.Drawing.Point(21, 29);
            this.PictureBox18.Name = "PictureBox18";
            this.PictureBox18.Size = new System.Drawing.Size(151, 119);
            this.PictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox18.TabIndex = 0;
            this.PictureBox18.TabStop = false;
            // 
            // Panel6
            // 
            this.Panel6.Controls.Add(this.button3);
            this.Panel6.Controls.Add(this.button2);
            this.Panel6.Controls.Add(this.button1);
            this.Panel6.Location = new System.Drawing.Point(3, 119);
            this.Panel6.Name = "Panel6";
            this.Panel6.Size = new System.Drawing.Size(316, 471);
            this.Panel6.TabIndex = 30;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(51, 85);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 34);
            this.button2.TabIndex = 1;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(51, 51);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 36);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.PictureBox11);
            this.GroupBox1.Controls.Add(this.PictureBox12);
            this.GroupBox1.Controls.Add(this.MenuStrip1);
            this.GroupBox1.Controls.Add(this.PictureBox17);
            this.GroupBox1.Controls.Add(this.PictureBox9);
            this.GroupBox1.Controls.Add(this.PictureBox10);
            this.GroupBox1.Controls.Add(this.PictureBox16);
            this.GroupBox1.Controls.Add(this.PictureBox15);
            this.GroupBox1.Controls.Add(this.PictureBox14);
            this.GroupBox1.Controls.Add(this.PictureBox13);
            this.GroupBox1.Controls.Add(this.PictureBox8);
            this.GroupBox1.Controls.Add(this.PictureBox7);
            this.GroupBox1.Controls.Add(this.PictureBox6);
            this.GroupBox1.Controls.Add(this.PictureBox5);
            this.GroupBox1.Controls.Add(this.PictureBox4);
            this.GroupBox1.Controls.Add(this.PictureBox3);
            this.GroupBox1.Controls.Add(this.PictureBox2);
            this.GroupBox1.Controls.Add(this.PictureBox1);
            this.GroupBox1.Location = new System.Drawing.Point(325, 119);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(670, 452);
            this.GroupBox1.TabIndex = 32;
            this.GroupBox1.TabStop = false;
            // 
            // PictureBox11
            // 
            this.PictureBox11.Location = new System.Drawing.Point(90, 85);
            this.PictureBox11.Name = "PictureBox11";
            this.PictureBox11.Size = new System.Drawing.Size(522, 352);
            this.PictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox11.TabIndex = 41;
            this.PictureBox11.TabStop = false;
            // 
            // PictureBox12
            // 
            this.PictureBox12.Location = new System.Drawing.Point(90, 85);
            this.PictureBox12.Name = "PictureBox12";
            this.PictureBox12.Size = new System.Drawing.Size(522, 352);
            this.PictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox12.TabIndex = 42;
            this.PictureBox12.TabStop = false;
            // 
            // MenuStrip1
            // 
            this.MenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ВыберитеПользователяToolStripMenuItem,
            this.ОтчетыToolStripMenuItem,
            this.ВыходToolStripMenuItem1});
            this.MenuStrip1.Location = new System.Drawing.Point(3, 26);
            this.MenuStrip1.Name = "MenuStrip1";
            this.MenuStrip1.Size = new System.Drawing.Size(664, 31);
            this.MenuStrip1.TabIndex = 8;
            this.MenuStrip1.Text = "MenuStrip1";
            // 
            // ВыберитеПользователяToolStripMenuItem
            // 
            this.ВыберитеПользователяToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ПриёмщикаВагоновToolStripMenuItem,
            this.МастераЦТОToolStripMenuItem,
            this.СлесарейToolStripMenuItem,
            this.МастераТО1ToolStripMenuItem,
            this.МастераТО2ToolStripMenuItem,
            this.МастераСРToolStripMenuItem,
            this.ОтделенияToolStripMenuItem,
            this.МастерскиеToolStripMenuItem,
            this.УчасткаToolStripMenuItem,
            this.ЦехаToolStripMenuItem,
            this.АдминистрацииToolStripMenuItem});
            this.ВыберитеПользователяToolStripMenuItem.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ВыберитеПользователяToolStripMenuItem.Name = "ВыберитеПользователяToolStripMenuItem";
            this.ВыберитеПользователяToolStripMenuItem.Size = new System.Drawing.Size(260, 27);
            this.ВыберитеПользователяToolStripMenuItem.Text = "В&ыберите учётную запись";
            // 
            // ПриёмщикаВагоновToolStripMenuItem
            // 
            this.ПриёмщикаВагоновToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox1});
            this.ПриёмщикаВагоновToolStripMenuItem.Name = "ПриёмщикаВагоновToolStripMenuItem";
            this.ПриёмщикаВагоновToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.ПриёмщикаВагоновToolStripMenuItem.Text = "Приёмщика вагонов";
            // 
            // ToolStripTextBox1
            // 
            this.ToolStripTextBox1.Name = "ToolStripTextBox1";
            this.ToolStripTextBox1.ReadOnly = true;
            this.ToolStripTextBox1.Size = new System.Drawing.Size(100, 27);
            this.ToolStripTextBox1.Text = "100";
            // 
            // МастераЦТОToolStripMenuItem
            // 
            this.МастераЦТОToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox3});
            this.МастераЦТОToolStripMenuItem.Name = "МастераЦТОToolStripMenuItem";
            this.МастераЦТОToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.МастераЦТОToolStripMenuItem.Text = "Мастера ЕО";
            // 
            // ToolStripTextBox3
            // 
            this.ToolStripTextBox3.Name = "ToolStripTextBox3";
            this.ToolStripTextBox3.Size = new System.Drawing.Size(100, 27);
            // 
            // СлесарейToolStripMenuItem
            // 
            this.СлесарейToolStripMenuItem.Name = "СлесарейToolStripMenuItem";
            this.СлесарейToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.СлесарейToolStripMenuItem.Text = "Слесарей";
            // 
            // МастераТО1ToolStripMenuItem
            // 
            this.МастераТО1ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox4});
            this.МастераТО1ToolStripMenuItem.Name = "МастераТО1ToolStripMenuItem";
            this.МастераТО1ToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.МастераТО1ToolStripMenuItem.Text = "Мастера ТО1";
            // 
            // ToolStripTextBox4
            // 
            this.ToolStripTextBox4.Name = "ToolStripTextBox4";
            this.ToolStripTextBox4.Size = new System.Drawing.Size(100, 27);
            // 
            // МастераТО2ToolStripMenuItem
            // 
            this.МастераТО2ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox6});
            this.МастераТО2ToolStripMenuItem.Name = "МастераТО2ToolStripMenuItem";
            this.МастераТО2ToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.МастераТО2ToolStripMenuItem.Text = "Мастера ТО2";
            // 
            // ToolStripTextBox6
            // 
            this.ToolStripTextBox6.Name = "ToolStripTextBox6";
            this.ToolStripTextBox6.Size = new System.Drawing.Size(100, 27);
            // 
            // МастераСРToolStripMenuItem
            // 
            this.МастераСРToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox5});
            this.МастераСРToolStripMenuItem.Name = "МастераСРToolStripMenuItem";
            this.МастераСРToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.МастераСРToolStripMenuItem.Text = "Мастера СР";
            // 
            // ToolStripTextBox5
            // 
            this.ToolStripTextBox5.Name = "ToolStripTextBox5";
            this.ToolStripTextBox5.Size = new System.Drawing.Size(100, 27);
            // 
            // ОтделенияToolStripMenuItem
            // 
            this.ОтделенияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.МалярногоToolStripMenuItem,
            this.СлесарноКузовногоToolStripMenuItem,
            this.РедукторногоToolStripMenuItem,
            this.МеханическогоToolStripMenuItem,
            this.КолесотокарногоToolStripMenuItem});
            this.ОтделенияToolStripMenuItem.Name = "ОтделенияToolStripMenuItem";
            this.ОтделенияToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.ОтделенияToolStripMenuItem.Text = "Отделения";
            // 
            // МалярногоToolStripMenuItem
            // 
            this.МалярногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox7});
            this.МалярногоToolStripMenuItem.Name = "МалярногоToolStripMenuItem";
            this.МалярногоToolStripMenuItem.Size = new System.Drawing.Size(268, 28);
            this.МалярногоToolStripMenuItem.Text = "Малярного";
            // 
            // ToolStripTextBox7
            // 
            this.ToolStripTextBox7.Name = "ToolStripTextBox7";
            this.ToolStripTextBox7.Size = new System.Drawing.Size(100, 27);
            // 
            // СлесарноКузовногоToolStripMenuItem
            // 
            this.СлесарноКузовногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox8});
            this.СлесарноКузовногоToolStripMenuItem.Name = "СлесарноКузовногоToolStripMenuItem";
            this.СлесарноКузовногоToolStripMenuItem.Size = new System.Drawing.Size(268, 28);
            this.СлесарноКузовногоToolStripMenuItem.Text = "Слесарно-Кузовного";
            // 
            // ToolStripTextBox8
            // 
            this.ToolStripTextBox8.Name = "ToolStripTextBox8";
            this.ToolStripTextBox8.Size = new System.Drawing.Size(100, 27);
            // 
            // РедукторногоToolStripMenuItem
            // 
            this.РедукторногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox9});
            this.РедукторногоToolStripMenuItem.Name = "РедукторногоToolStripMenuItem";
            this.РедукторногоToolStripMenuItem.Size = new System.Drawing.Size(268, 28);
            this.РедукторногоToolStripMenuItem.Text = "Редукторного";
            // 
            // ToolStripTextBox9
            // 
            this.ToolStripTextBox9.Name = "ToolStripTextBox9";
            this.ToolStripTextBox9.Size = new System.Drawing.Size(100, 27);
            // 
            // МеханическогоToolStripMenuItem
            // 
            this.МеханическогоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox11});
            this.МеханическогоToolStripMenuItem.Name = "МеханическогоToolStripMenuItem";
            this.МеханическогоToolStripMenuItem.Size = new System.Drawing.Size(268, 28);
            this.МеханическогоToolStripMenuItem.Text = "Механического";
            // 
            // ToolStripTextBox11
            // 
            this.ToolStripTextBox11.Name = "ToolStripTextBox11";
            this.ToolStripTextBox11.Size = new System.Drawing.Size(100, 27);
            // 
            // КолесотокарногоToolStripMenuItem
            // 
            this.КолесотокарногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox12});
            this.КолесотокарногоToolStripMenuItem.Name = "КолесотокарногоToolStripMenuItem";
            this.КолесотокарногоToolStripMenuItem.Size = new System.Drawing.Size(268, 28);
            this.КолесотокарногоToolStripMenuItem.Text = "Колесотокарного";
            // 
            // ToolStripTextBox12
            // 
            this.ToolStripTextBox12.Name = "ToolStripTextBox12";
            this.ToolStripTextBox12.Size = new System.Drawing.Size(100, 27);
            // 
            // МастерскиеToolStripMenuItem
            // 
            this.МастерскиеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.РадиотехническойToolStripMenuItem,
            this.АккумуляторнойToolStripMenuItem});
            this.МастерскиеToolStripMenuItem.Name = "МастерскиеToolStripMenuItem";
            this.МастерскиеToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.МастерскиеToolStripMenuItem.Text = "Мастерской";
            // 
            // РадиотехническойToolStripMenuItem
            // 
            this.РадиотехническойToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox13});
            this.РадиотехническойToolStripMenuItem.Name = "РадиотехническойToolStripMenuItem";
            this.РадиотехническойToolStripMenuItem.Size = new System.Drawing.Size(246, 28);
            this.РадиотехническойToolStripMenuItem.Text = "Радиотехнической";
            // 
            // ToolStripTextBox13
            // 
            this.ToolStripTextBox13.Name = "ToolStripTextBox13";
            this.ToolStripTextBox13.Size = new System.Drawing.Size(100, 27);
            // 
            // АккумуляторнойToolStripMenuItem
            // 
            this.АккумуляторнойToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox14});
            this.АккумуляторнойToolStripMenuItem.Name = "АккумуляторнойToolStripMenuItem";
            this.АккумуляторнойToolStripMenuItem.Size = new System.Drawing.Size(246, 28);
            this.АккумуляторнойToolStripMenuItem.Text = "Аккумуляторной";
            // 
            // ToolStripTextBox14
            // 
            this.ToolStripTextBox14.Name = "ToolStripTextBox14";
            this.ToolStripTextBox14.Size = new System.Drawing.Size(100, 27);
            // 
            // УчасткаToolStripMenuItem
            // 
            this.УчасткаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.РемонтаТокоприемниковToolStripMenuItem,
            this.ЭлектроаппаратногоToolStripMenuItem,
            this.РемонтаДвигателейToolStripMenuItem});
            this.УчасткаToolStripMenuItem.Name = "УчасткаToolStripMenuItem";
            this.УчасткаToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.УчасткаToolStripMenuItem.Text = "Участка";
            // 
            // РемонтаТокоприемниковToolStripMenuItem
            // 
            this.РемонтаТокоприемниковToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox15});
            this.РемонтаТокоприемниковToolStripMenuItem.Name = "РемонтаТокоприемниковToolStripMenuItem";
            this.РемонтаТокоприемниковToolStripMenuItem.Size = new System.Drawing.Size(309, 28);
            this.РемонтаТокоприемниковToolStripMenuItem.Text = "Ремонта токоприемников";
            // 
            // ToolStripTextBox15
            // 
            this.ToolStripTextBox15.Name = "ToolStripTextBox15";
            this.ToolStripTextBox15.Size = new System.Drawing.Size(100, 27);
            // 
            // ЭлектроаппаратногоToolStripMenuItem
            // 
            this.ЭлектроаппаратногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox16});
            this.ЭлектроаппаратногоToolStripMenuItem.Name = "ЭлектроаппаратногоToolStripMenuItem";
            this.ЭлектроаппаратногоToolStripMenuItem.Size = new System.Drawing.Size(309, 28);
            this.ЭлектроаппаратногоToolStripMenuItem.Text = "Электроаппаратного";
            // 
            // ToolStripTextBox16
            // 
            this.ToolStripTextBox16.Name = "ToolStripTextBox16";
            this.ToolStripTextBox16.Size = new System.Drawing.Size(100, 27);
            // 
            // РемонтаДвигателейToolStripMenuItem
            // 
            this.РемонтаДвигателейToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox17});
            this.РемонтаДвигателейToolStripMenuItem.Name = "РемонтаДвигателейToolStripMenuItem";
            this.РемонтаДвигателейToolStripMenuItem.Size = new System.Drawing.Size(309, 28);
            this.РемонтаДвигателейToolStripMenuItem.Text = "Ремонта двигателей";
            // 
            // ToolStripTextBox17
            // 
            this.ToolStripTextBox17.Name = "ToolStripTextBox17";
            this.ToolStripTextBox17.Size = new System.Drawing.Size(100, 27);
            // 
            // ЦехаToolStripMenuItem
            // 
            this.ЦехаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ЭлектросварочногоToolStripMenuItem,
            this.ТележечногоToolStripMenuItem});
            this.ЦехаToolStripMenuItem.Name = "ЦехаToolStripMenuItem";
            this.ЦехаToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.ЦехаToolStripMenuItem.Text = "Цеха";
            // 
            // ЭлектросварочногоToolStripMenuItem
            // 
            this.ЭлектросварочногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox18});
            this.ЭлектросварочногоToolStripMenuItem.Name = "ЭлектросварочногоToolStripMenuItem";
            this.ЭлектросварочногоToolStripMenuItem.Size = new System.Drawing.Size(258, 28);
            this.ЭлектросварочногоToolStripMenuItem.Text = "Электросварочного";
            // 
            // ToolStripTextBox18
            // 
            this.ToolStripTextBox18.Name = "ToolStripTextBox18";
            this.ToolStripTextBox18.Size = new System.Drawing.Size(100, 27);
            // 
            // ТележечногоToolStripMenuItem
            // 
            this.ТележечногоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox19});
            this.ТележечногоToolStripMenuItem.Name = "ТележечногоToolStripMenuItem";
            this.ТележечногоToolStripMenuItem.Size = new System.Drawing.Size(258, 28);
            this.ТележечногоToolStripMenuItem.Text = "Тележечного";
            // 
            // ToolStripTextBox19
            // 
            this.ToolStripTextBox19.Name = "ToolStripTextBox19";
            this.ToolStripTextBox19.Size = new System.Drawing.Size(100, 27);
            // 
            // АдминистрацииToolStripMenuItem
            // 
            this.АдминистрацииToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripTextBox2});
            this.АдминистрацииToolStripMenuItem.Name = "АдминистрацииToolStripMenuItem";
            this.АдминистрацииToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.АдминистрацииToolStripMenuItem.Text = "Администрации";
            // 
            // ToolStripTextBox2
            // 
            this.ToolStripTextBox2.Name = "ToolStripTextBox2";
            this.ToolStripTextBox2.ReadOnly = true;
            this.ToolStripTextBox2.Size = new System.Drawing.Size(100, 27);
            this.ToolStripTextBox2.Text = "100";
            // 
            // ОтчетыToolStripMenuItem
            // 
            this.ОтчетыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ПриёмщикаВагоновToolStripMenuItem1,
            this.ПрохождениеЕОToolStripMenuItem});
            this.ОтчетыToolStripMenuItem.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ОтчетыToolStripMenuItem.Name = "ОтчетыToolStripMenuItem";
            this.ОтчетыToolStripMenuItem.Size = new System.Drawing.Size(90, 27);
            this.ОтчетыToolStripMenuItem.Text = "От&четы";
            this.ОтчетыToolStripMenuItem.ToolTipText = "Пока не все работает))Требуется для работы Excel 2003";
            // 
            // ПриёмщикаВагоновToolStripMenuItem1
            // 
            this.ПриёмщикаВагоновToolStripMenuItem1.Name = "ПриёмщикаВагоновToolStripMenuItem1";
            this.ПриёмщикаВагоновToolStripMenuItem1.Size = new System.Drawing.Size(262, 28);
            this.ПриёмщикаВагоновToolStripMenuItem1.Text = "Приёмщика вагонов";
            // 
            // ПрохождениеЕОToolStripMenuItem
            // 
            this.ПрохождениеЕОToolStripMenuItem.Name = "ПрохождениеЕОToolStripMenuItem";
            this.ПрохождениеЕОToolStripMenuItem.Size = new System.Drawing.Size(262, 28);
            this.ПрохождениеЕОToolStripMenuItem.Text = "Прохождение ЕО";
            // 
            // ВыходToolStripMenuItem1
            // 
            this.ВыходToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ВыходToolStripMenuItem2});
            this.ВыходToolStripMenuItem1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ВыходToolStripMenuItem1.Name = "ВыходToolStripMenuItem1";
            this.ВыходToolStripMenuItem1.Size = new System.Drawing.Size(81, 27);
            this.ВыходToolStripMenuItem1.Text = "В&ыход";
            // 
            // ВыходToolStripMenuItem2
            // 
            this.ВыходToolStripMenuItem2.Name = "ВыходToolStripMenuItem2";
            this.ВыходToolStripMenuItem2.Size = new System.Drawing.Size(139, 28);
            this.ВыходToolStripMenuItem2.Text = "Выход";
            // 
            // PictureBox17
            // 
            this.PictureBox17.Location = new System.Drawing.Point(288, 274);
            this.PictureBox17.Name = "PictureBox17";
            this.PictureBox17.Size = new System.Drawing.Size(261, 176);
            this.PictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox17.TabIndex = 13;
            this.PictureBox17.TabStop = false;
            // 
            // PictureBox9
            // 
            this.PictureBox9.Location = new System.Drawing.Point(90, 85);
            this.PictureBox9.Name = "PictureBox9";
            this.PictureBox9.Size = new System.Drawing.Size(522, 352);
            this.PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox9.TabIndex = 39;
            this.PictureBox9.TabStop = false;
            // 
            // PictureBox10
            // 
            this.PictureBox10.Location = new System.Drawing.Point(90, 85);
            this.PictureBox10.Name = "PictureBox10";
            this.PictureBox10.Size = new System.Drawing.Size(522, 352);
            this.PictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox10.TabIndex = 40;
            this.PictureBox10.TabStop = false;
            // 
            // PictureBox16
            // 
            this.PictureBox16.Location = new System.Drawing.Point(258, 250);
            this.PictureBox16.Name = "PictureBox16";
            this.PictureBox16.Size = new System.Drawing.Size(261, 176);
            this.PictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox16.TabIndex = 12;
            this.PictureBox16.TabStop = false;
            // 
            // PictureBox15
            // 
            this.PictureBox15.Location = new System.Drawing.Point(236, 236);
            this.PictureBox15.Name = "PictureBox15";
            this.PictureBox15.Size = new System.Drawing.Size(261, 176);
            this.PictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox15.TabIndex = 11;
            this.PictureBox15.TabStop = false;
            // 
            // PictureBox14
            // 
            this.PictureBox14.Location = new System.Drawing.Point(90, 85);
            this.PictureBox14.Name = "PictureBox14";
            this.PictureBox14.Size = new System.Drawing.Size(522, 352);
            this.PictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox14.TabIndex = 10;
            this.PictureBox14.TabStop = false;
            // 
            // PictureBox13
            // 
            this.PictureBox13.Location = new System.Drawing.Point(90, 85);
            this.PictureBox13.Name = "PictureBox13";
            this.PictureBox13.Size = new System.Drawing.Size(522, 352);
            this.PictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox13.TabIndex = 9;
            this.PictureBox13.TabStop = false;
            // 
            // PictureBox8
            // 
            this.PictureBox8.Location = new System.Drawing.Point(90, 85);
            this.PictureBox8.Name = "PictureBox8";
            this.PictureBox8.Size = new System.Drawing.Size(522, 352);
            this.PictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox8.TabIndex = 8;
            this.PictureBox8.TabStop = false;
            // 
            // PictureBox7
            // 
            this.PictureBox7.Location = new System.Drawing.Point(156, 172);
            this.PictureBox7.Name = "PictureBox7";
            this.PictureBox7.Size = new System.Drawing.Size(261, 176);
            this.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox7.TabIndex = 7;
            this.PictureBox7.TabStop = false;
            // 
            // PictureBox6
            // 
            this.PictureBox6.Location = new System.Drawing.Point(90, 85);
            this.PictureBox6.Name = "PictureBox6";
            this.PictureBox6.Size = new System.Drawing.Size(522, 352);
            this.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox6.TabIndex = 6;
            this.PictureBox6.TabStop = false;
            // 
            // PictureBox5
            // 
            this.PictureBox5.Location = new System.Drawing.Point(90, 85);
            this.PictureBox5.Name = "PictureBox5";
            this.PictureBox5.Size = new System.Drawing.Size(522, 352);
            this.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox5.TabIndex = 5;
            this.PictureBox5.TabStop = false;
            // 
            // PictureBox4
            // 
            this.PictureBox4.Location = new System.Drawing.Point(90, 85);
            this.PictureBox4.Name = "PictureBox4";
            this.PictureBox4.Size = new System.Drawing.Size(522, 352);
            this.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox4.TabIndex = 4;
            this.PictureBox4.TabStop = false;
            // 
            // PictureBox3
            // 
            this.PictureBox3.Location = new System.Drawing.Point(90, 85);
            this.PictureBox3.Name = "PictureBox3";
            this.PictureBox3.Size = new System.Drawing.Size(522, 352);
            this.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox3.TabIndex = 3;
            this.PictureBox3.TabStop = false;
            // 
            // PictureBox2
            // 
            this.PictureBox2.Location = new System.Drawing.Point(36, 70);
            this.PictureBox2.Name = "PictureBox2";
            this.PictureBox2.Size = new System.Drawing.Size(261, 176);
            this.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox2.TabIndex = 2;
            this.PictureBox2.TabStop = false;
            // 
            // PictureBox1
            // 
            this.PictureBox1.Location = new System.Drawing.Point(90, 85);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(522, 352);
            this.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox1.TabIndex = 0;
            this.PictureBox1.TabStop = false;
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.Label2);
            this.GroupBox2.Controls.Add(this.Label1);
            this.GroupBox2.Controls.Add(this.StatusStrip1);
            this.GroupBox2.Controls.Add(this.StatusStrip2);
            this.GroupBox2.Location = new System.Drawing.Point(325, 3);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(690, 104);
            this.GroupBox2.TabIndex = 33;
            this.GroupBox2.TabStop = false;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(386, 40);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(324, 22);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "Для начала работы выберите режим!";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label1.Location = new System.Drawing.Point(16, 22);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(580, 23);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Программа  \"Терминал\" модуль АСУ \"Депо\" приветствует вас!";
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.StatusStrip1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel1,
            this.ssdate});
            this.StatusStrip1.Location = new System.Drawing.Point(16, 71);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(305, 28);
            this.StatusStrip1.TabIndex = 36;
            this.StatusStrip1.Text = "StatusStrip3";
            // 
            // ToolStripStatusLabel1
            // 
            this.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1";
            this.ToolStripStatusLabel1.Size = new System.Drawing.Size(86, 23);
            this.ToolStripStatusLabel1.Text = "Сегодня";
            // 
            // ssdate
            // 
            this.ssdate.Name = "ssdate";
            this.ssdate.Size = new System.Drawing.Size(202, 23);
            this.ssdate.Text = "ToolStripStatusLabel1";
            // 
            // StatusStrip2
            // 
            this.StatusStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.StatusStrip2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StatusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sstime});
            this.StatusStrip2.Location = new System.Drawing.Point(286, 71);
            this.StatusStrip2.Name = "StatusStrip2";
            this.StatusStrip2.Size = new System.Drawing.Size(219, 28);
            this.StatusStrip2.TabIndex = 37;
            this.StatusStrip2.Text = "StatusStrip4";
            // 
            // sstime
            // 
            this.sstime.Name = "sstime";
            this.sstime.Size = new System.Drawing.Size(202, 23);
            this.sstime.Text = "ToolStripStatusLabel1";
            // 
            // GroupBox3
            // 
            this.GroupBox3.Controls.Add(this.Label4);
            this.GroupBox3.Controls.Add(this.Label3);
            this.GroupBox3.Location = new System.Drawing.Point(1022, 3);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(259, 104);
            this.GroupBox3.TabIndex = 34;
            this.GroupBox3.TabStop = false;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(163, 58);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(51, 22);
            this.Label4.TabIndex = 37;
            this.Label4.Text = "RTM";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(6, 22);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(178, 22);
            this.Label3.TabIndex = 36;
            this.Label3.Text = "Версия программы ";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(51, 115);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(110, 33);
            this.button3.TabIndex = 2;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Sama_programma
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1284, 778);
            this.Controls.Add(this.TableLayoutPanel1);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Sama_programma";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Программа Терминал модуль АСУ ДЕПО";
            this.TableLayoutPanel1.ResumeLayout(false);
            this.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox18)).EndInit();
            this.Panel6.ResumeLayout(false);
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox12)).EndInit();
            this.MenuStrip1.ResumeLayout(false);
            this.MenuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.StatusStrip2.ResumeLayout(false);
            this.StatusStrip2.PerformLayout();
            this.GroupBox3.ResumeLayout(false);
            this.GroupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        internal System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.PictureBox PictureBox19;
        internal System.Windows.Forms.PictureBox PictureBox18;
        internal System.Windows.Forms.Panel Panel6;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.PictureBox PictureBox11;
        internal System.Windows.Forms.PictureBox PictureBox12;
        internal System.Windows.Forms.MenuStrip MenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem ВыберитеПользователяToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ПриёмщикаВагоновToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox1;
        internal System.Windows.Forms.ToolStripMenuItem МастераЦТОToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox3;
        internal System.Windows.Forms.ToolStripMenuItem СлесарейToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem МастераТО1ToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox4;
        internal System.Windows.Forms.ToolStripMenuItem МастераТО2ToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox6;
        internal System.Windows.Forms.ToolStripMenuItem МастераСРToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox5;
        internal System.Windows.Forms.ToolStripMenuItem ОтделенияToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem МалярногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox7;
        internal System.Windows.Forms.ToolStripMenuItem СлесарноКузовногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox8;
        internal System.Windows.Forms.ToolStripMenuItem РедукторногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox9;
        internal System.Windows.Forms.ToolStripMenuItem МеханическогоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox11;
        internal System.Windows.Forms.ToolStripMenuItem КолесотокарногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox12;
        internal System.Windows.Forms.ToolStripMenuItem МастерскиеToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem РадиотехническойToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox13;
        internal System.Windows.Forms.ToolStripMenuItem АккумуляторнойToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox14;
        internal System.Windows.Forms.ToolStripMenuItem УчасткаToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem РемонтаТокоприемниковToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox15;
        internal System.Windows.Forms.ToolStripMenuItem ЭлектроаппаратногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox16;
        internal System.Windows.Forms.ToolStripMenuItem РемонтаДвигателейToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox17;
        internal System.Windows.Forms.ToolStripMenuItem ЦехаToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ЭлектросварочногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox18;
        internal System.Windows.Forms.ToolStripMenuItem ТележечногоToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox19;
        internal System.Windows.Forms.ToolStripMenuItem АдминистрацииToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox2;
        internal System.Windows.Forms.ToolStripMenuItem ОтчетыToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ПриёмщикаВагоновToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem ПрохождениеЕОToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ВыходToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem ВыходToolStripMenuItem2;
        internal System.Windows.Forms.PictureBox PictureBox17;
        internal System.Windows.Forms.PictureBox PictureBox9;
        internal System.Windows.Forms.PictureBox PictureBox10;
        internal System.Windows.Forms.PictureBox PictureBox16;
        internal System.Windows.Forms.PictureBox PictureBox15;
        internal System.Windows.Forms.PictureBox PictureBox14;
        internal System.Windows.Forms.PictureBox PictureBox13;
        internal System.Windows.Forms.PictureBox PictureBox8;
        internal System.Windows.Forms.PictureBox PictureBox7;
        internal System.Windows.Forms.PictureBox PictureBox6;
        internal System.Windows.Forms.PictureBox PictureBox5;
        internal System.Windows.Forms.PictureBox PictureBox4;
        internal System.Windows.Forms.PictureBox PictureBox3;
        internal System.Windows.Forms.PictureBox PictureBox2;
        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel1;
        internal System.Windows.Forms.ToolStripStatusLabel ssdate;
        internal System.Windows.Forms.StatusStrip StatusStrip2;
        internal System.Windows.Forms.ToolStripStatusLabel sstime;
        internal System.Windows.Forms.GroupBox GroupBox3;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}

