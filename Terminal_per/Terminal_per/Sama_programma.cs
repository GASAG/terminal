﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Terminal_per
{
    public partial class Sama_programma : Form
    {
        public Sama_programma()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           Form Sama_programma_eged_osm = new Sama_programma_eged_osm();
           Sama_programma_eged_osm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form Form1 = new Form1();
            Form1.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form Sama_programma_tex_obs_1 = new Sama_programma_tex_obs_1();
            Sama_programma_tex_obs_1.Show();
        }
    }
}
